package com.garlane.pancake.plugin.ui;

import com.alibaba.fastjson.JSONArray;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.project.DumbService;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.util.List;
import java.util.Map;

/** 显示任务进度条，需要将总体步骤放到这里
 * @author lixinfa
 * @date 2022/2/6 0:31
 */
public class GeneratorTask extends Task.Modal {
    private final Project project;
    private final JFrame windows;
    //json列表，jsonPath-jsonName-JSONArray
    private final Map<String, Map<String, JSONArray>> pageJsonArraysMap;
    //json数组的注释，位于数组上面，说明哪些驼峰key是属于主体的，比如creatTime
    private final Map<String, List<String>> jsonArraysOwnKeyMap;

    public GeneratorTask(@NotNull Project project, JFrame windows
            , Map<String, Map<String, JSONArray>> pageJsonArraysMap
            , Map<String, List<String>> jsonArraysOwnKeyMap) {
        super(project, "生成代码", false);
        this.project = project;
        this.windows = windows;
        this.pageJsonArraysMap = pageJsonArraysMap;
        this.jsonArraysOwnKeyMap = jsonArraysOwnKeyMap;
    }

    @Override
    public void run(@NotNull ProgressIndicator indicator) {
        indicator.setIndeterminate(false);
        //页面解析
        generator(indicator);
        //从json中拆分数据实体

        //
    }

    public void generator(ProgressIndicator indicator) {
        indicator.setText("正在解析json内在逻辑 ......");
        indicator.setFraction(0.0);
        int pageJsonArraysSize = pageJsonArraysMap.size();
        int countInt = pageJsonArraysSize * 1000;
        double count = (countInt) * 1.0;
//        for (String jsonPath : pageJsonArraysMap.keySet()) {
//            int index = start + integer + 1;
//            indicator.setText2(String.format("[%s/%s] [%s] --> %s",
//                    index, countInt, jsonPath, filename));
//            indicator.setFraction(index / count);
//        }
//
//        for (int i = 0; i < pageJsonArraysSize; i++) {
//            RootModel rootModel = rootModels.get(i);
//            int start = i * templateSize;
//            generator.generator(rootModel, templates, (integer, filename) -> {
//                int index = start + integer + 1;
//                indicator.setText2(String.format("[%s/%s] [%s] --> %s",
//                        index, countInt, pageJsonArraysMap..getName(), filename));
//                indicator.setFraction(index / count);
//            });
//        }
        indicator.setText("json内在逻辑解析完毕！");
        indicator.setText2("正在执行收尾工作，请稍候 ......");

        indicator.setFraction(1.0);
        indicator.setIndeterminate(true);
//        PluginUtils.refreshProject();
    }

    @Override
    public void onSuccess() {
        super.onSuccess();
        windows.dispose();
        DumbService dumbService = DumbService.getInstance(project);
        if (dumbService.isDumb()) {
            dumbService.showDumbModeNotification("正在等待其他任务完成后才能进行格式化代码操作 ...");
        }
//        dumbService.smartInvokeLater(() -> {
//            List<PsiFile> saveFiles = generator.getSaveFiles();
//            if (!saveFiles.isEmpty()) {
//                FileUtils.getInstance().reformatCode(project, saveFiles.toArray(new PsiFile[0]));
//            }
//        });
//        Messages.showInfoMessage(String.format("代码构建完毕，涉及 %s 个数据库表和 %s 个模板文件，总共生成 %s 个文件。", rootModels.size(), templates.size(), generator.getSaveFiles().size()), "完成");
    }

    @Override
    public void onThrowable(@NotNull Throwable error) {
        super.onThrowable(error);
        Messages.showErrorDialog("代码生成失败，当前插件 2.x 版本不兼容旧版的代码模板，请升级代码模板，代码模板升级指南请查看插件介绍。\n\n" + error.getMessage(), "生成代码失败");
        windows.setVisible(true);
    }
}
