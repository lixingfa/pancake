package com.garlane.pancake.plugin.ui;

import com.alibaba.fastjson.JSONArray;
import com.garlane.pancake.common.model.View;
import com.garlane.pancake.common.util.DataBaseUtil;
import com.garlane.pancake.common.util.HtmlUtil;
import com.intellij.openapi.fileChooser.FileChooserDescriptor;
import com.intellij.openapi.fileChooser.FileChooserDescriptorFactory;
import com.intellij.openapi.fileChooser.PathChooserDialog;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.ui.TextBrowseFolderListener;
import com.intellij.openapi.ui.TextFieldWithBrowseButton;
import org.apache.commons.lang3.StringUtils;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainFrame extends JFrame {

    public MainFrame(Project project){
        initWindows(project);
    }
    /**
     * 初始化窗口配置
     */
    private void initWindows(Project project) {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        //内容设置
        JPanel content = initContent(project);
        setContentPane(content);
        int frameWidth = content.getPreferredSize().width;
        int frameHeight = content.getPreferredSize().height;
        setLocation((screenSize.width - frameWidth) / 2, (screenSize.height - frameHeight) / 2);
        setResizable(false);
        pack();
        setTitle("选择页面路径");
        setVisible(true);
    }

    /**
     * 内容面板初始化
     */
    private JPanel initContent(Project project){
        //这里比较简单，如果分成多个类设置内容，则可以采用全局变量
        JPanel content = new JPanel();
        //文件路径选择框
        TextFieldWithBrowseButton pagesPathField = new TextFieldWithBrowseButton();
        FileChooserDescriptor chooserDescriptor = FileChooserDescriptorFactory.createSingleFileOrFolderDescriptor();
        chooserDescriptor.putUserData(PathChooserDialog.PREFER_LAST_OVER_EXPLICIT, false);
        chooserDescriptor.setTitle("选择页面路径");
        pagesPathField.addBrowseFolderListener(new TextBrowseFolderListener(chooserDescriptor, project));
        content.add(pagesPathField);
        //数据库类型选择
        String[] dataBaseType = {"MYSQL"};
        JComboBox<String> dataBaseJComboBox = new JComboBox(dataBaseType);
        content.add(dataBaseJComboBox);
        //按钮：完成按钮
        JButton finishButton = new JButton("执行");
        addFinishButtonEvent(finishButton, pagesPathField, String.valueOf(dataBaseJComboBox.getSelectedItem()), project);
        content.add(finishButton);
        return content;
    }

    /**
     * 给完成按钮添加事件
     * @param finishButton 完成按钮
     * @param pagesPathField 文件选择路径
     * @param dataBase 数据库类型
     * @param project 项目对象
     */
    private void addFinishButtonEvent(JButton finishButton, TextFieldWithBrowseButton pagesPathField
            , String dataBase, Project project){
        finishButton.addActionListener(event -> {
            try {
                String pathFieldText = pagesPathField.getText();
                if (StringUtils.isBlank(pathFieldText)) {
                    Messages.showWarningDialog("请选择静态页面所在的文件夹。", "警告");
                    return;
                }
                File htmlsDir = new File(pathFieldText);
                if (htmlsDir.isFile()){
                    Messages.showWarningDialog("请选择静态页面所在的文件夹，现在选的是文件。", "警告");
                    return;
                }
                setVisible(false);
                //json列表，jsonPath-jsonName-JSONArray
                Map<String, Map<String, JSONArray>> pageJsonArraysMap = new HashMap<>();
                //json数组的注释，位于数组上面，说明哪些驼峰key是属于主体的，比如creatTime
                Map<String, List<String>> jsonArraysOwnKeyMap = new HashMap<>();

                //解析页面，得到pageJsonArraysMap和jsonArraysOwnKeyMap的内容
                List<View> views = HtmlUtil.htmlAnalysis(htmlsDir, pageJsonArraysMap, jsonArraysOwnKeyMap, false);


                //从json中拆分数据实体
                Map<String, Object> sqlMap = DataBaseUtil.getDateEntityFromJson(pageJsonArraysMap, jsonArraysOwnKeyMap, dataBase);
                //计算要处理的页面，估算处理时间

                //构建测试数据，供最后验证

                //验证

//                GeneratorTask generatorTask = new GeneratorTask(project, this, generator, allSelectFile, tableSetting.getRootModels());
//                ProgressManager.getInstance().run(generatorTask);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
                setVisible(true);
                Messages.showErrorDialog(throwable.getMessage() + "如有疑问，请联系开发者。\n\n", "生成代码失败");
            }
        });
    }
}
