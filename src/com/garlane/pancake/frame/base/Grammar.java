package com.garlane.pancake.frame.base;

import com.garlane.pancake.common.constant.Constant;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 解析编程语言语法的类
 */
public class Grammar {
    /**
     * 单行注释
     */
    private String singleAnnotation;
    /**
     * 多行注释开头
     */
    private String multipleAnnotaBegin;
    /**
     * 多行注释开头
     */
    private String multipleAnnotaEnd;
    /**
     * 句子结束的符号
     */
    private String sentenceEnd;
    /**变量的命名规则*/
    public static final String VAR_REX = "[_a-zA-Z]+[_0-9a-zA-Z]*";

    /**
     * 默认构造函数
     */
    public Grammar() {
        singleAnnotation = "//";
        multipleAnnotaBegin = "/*";
        multipleAnnotaEnd = "*/";
        sentenceEnd = Constant.SEMICOLON_EN;
    }

    public void init(String singleAnnotation, String multipleAnnotaBegin, String multipleAnnotaEnd, String sentenceEnd) {
        this.singleAnnotation = singleAnnotation;
        this.multipleAnnotaBegin = multipleAnnotaBegin;
        this.multipleAnnotaEnd = multipleAnnotaEnd;
        this.sentenceEnd = sentenceEnd;
    }

    /**
     * 语法解析
     *
     * @param list 要解析的行
     */
    public void analysis(List<String> list) {
        //1、先去掉注释和空白行，并去掉前后空格，这些内容对我们分析逻辑没有帮助
        standardCode(list);
        //2、得到代码块和句子
//        for (String line : list) {
//            //判断句式，双等号、大于、小于、大于等于、小于等于
//            if (line.contains(DOUBLE_EQ)) {
//
//            }
//        }
    }

    /**
     * 去除无用代码，并将代码规范化,{之间的代码换行}，;后换行
     *
     * @param list 代码
     * @return List<String> 处理后的代码
     */
    public List<String> standardCode(List<String> list) {
        List<String> lines = new ArrayList<>(list.size());
        //代码规范化
        for (String line : list) {
            lines.addAll(standardCode(line));
        }
        //去除注释和无用代码
        Iterator<String> iterator = lines.listIterator();
        boolean multipleAnnota = false;
        int i = 0;
        while (iterator.hasNext()) {
            //统一去掉前后空格
            String line = iterator.next().trim();
            if (line.contains(multipleAnnotaEnd)) {
                //多行注解结尾，能到这里的，就是不同一行的
                multipleAnnota = false;
                //位于结尾
                if (line.endsWith(multipleAnnotaEnd)) {
                    //包含开头但不是以多行注释开头
                    if (line.contains(multipleAnnotaBegin) && !line.startsWith(multipleAnnotaBegin)) {
                        line = line.substring(0, line.indexOf(multipleAnnotaBegin)).trim();
                        list.set(i, line);
                    } else {
                        //没包含开头
                        iterator.remove();
                        i--;
                    }
                } else {
                    //不位于结尾
                    line = line.substring(line.indexOf(multipleAnnotaEnd) + 2, line.length()).trim();
                    list.set(i, line);
                }
            } else
                //身处多行注释中
                if (multipleAnnota
                        //无内容
                        || StringUtils.isBlank(line)
                        //单行注释
                        || line.startsWith(singleAnnotation)
                        //一个结束符号
                        || Constant.SEMICOLON_EN.equals(line)) {
                    iterator.remove();
                    i--;
                } else if (line.contains(singleAnnotation)) {
                    //单行注释在句子后面
                    line = line.substring(0, line.indexOf(singleAnnotation)).trim();
                    list.set(i, line);
                } else if (line.contains(multipleAnnotaBegin)) {
                    multipleAnnota = true;
                    if (line.startsWith(multipleAnnotaBegin)) {
                        //位于开头，直接移除
                        iterator.remove();
                        i--;
                    } else {
                        //不位于开头
                        line = line.substring(0, line.indexOf(multipleAnnotaBegin)).trim();
                        list.set(i, line);
                    }
                }
            i++;
        }
        return lines;
    }

    /**
     * 对字符串按编码格式进行换行
     *
     * @param s 字符串
     * @return List<String> 换行后的代码
     */
    public List<String> standardCode(String s) {
        List<String> list = new ArrayList<>();
        StringBuilder builder = new StringBuilder();
        char[] cs = s.toCharArray();
        for (char c : cs) {
            //{;
            if ('{' == c || ';' == c) {
                builder.append(c);
                list.add(builder.toString().trim());
                builder = new StringBuilder();
            } else if ('}' == c) {
                //}
                if (builder.length() > 0) {
                    list.add(builder.toString());
                    builder = new StringBuilder();
                }
                list.add(String.valueOf(c));
            } else {
                builder.append(c);
            }
        }
        if (builder.length() > 0) {
            list.add(builder.toString());
        }
        return list;
    }
}
