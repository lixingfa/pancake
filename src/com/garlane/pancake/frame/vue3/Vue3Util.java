package com.garlane.pancake.frame.vue3;
import com.garlane.pancake.common.constant.Constant;
import com.garlane.pancake.common.model.view.GetDataAction;
import com.garlane.pancake.common.util.StringUtil;
import com.garlane.pancake.frame.base.Grammar;

import java.util.*;

/**
 * vue3的解析类，关注对外包引入数据的筛选和排序，这些动作后续要放到服务器端完成
 */
public class Vue3Util {
    //使用Vue.createApp({ /* 选项 */ })的方式配置vue对象，真没必要把{}单独声明
//    private static final String VUE_CREATE_REX = "[ =\\w]*Vue.createApp\\([ ]*\\{";
    /**
     * data部分
     */
    private static final String DATA_REX = "data[ ]*\\([ ]*\\)[ ]*\\{";
    /**
     * created
     */
    private static final String CREATED_REX = "created[ ]*\\([ ]*\\)[ ]*\\{";
    /**
     * methods
     */
    private static final String METHODS_REX = "methods[ ]*:[ ]*\\{";
    /**
     * 筛选
     */
    private static final String FILTER_REX = "[this.|let |var |const ]+" + Grammar.VAR_REX + "[ ]*=[ ]*"
            + Grammar.VAR_REX + ".filter\\([ ]*\\([ ]*" + Grammar.VAR_REX + "[ ]*\\)[ ]*=>[ ]*\\{";
    /**
     * 排序
     */
    private static final String ORDER_REX = ".sort\\([ ]*sortByKeys\\([ \":a-zA-Z,]+\\)[ ]*\\)[ ]*";
    /**
     * 直接排序
     */
    private static final String THIS_ORDER_REX = "[this.|let |var |const ]+" + Grammar.VAR_REX + "[ ]*=[ ]*"
            + Grammar.VAR_REX + ".sort\\([ ]*sortByKeys\\(";
    private static final String RETURN = "return";
    /**
     * 获取vue3对象
     * @param lines         html页面的内容
     * @return Map<String, GetDataAction>
     */
    public static List<GetDataAction> getJsonDataActionMap(List<String> lines) {
        //用到的json数据，json数据的名称-对应的动作
        List<GetDataAction> getJsonDataActionList = new ArrayList<>();
        //在data里就直接用了变量的数据
        Map<String, String> varNameMap = new HashMap<>();
        //1、去除无用代码，View是把整个页面当成一个String处理的，Vue需要逐行处理
        Grammar grammar = new Grammar();
        List<String> list = grammar.standardCode(lines);
        //2、解析vue对象
        boolean inData = false;
        boolean inCreated = false;
        boolean inFilter = false;
        int braceTotal = 0;
        GetDataAction getDataAction = new GetDataAction();
        StringBuilder conditionBuilder = new StringBuilder();
        for (String line : list) {
            if (line.contains(Constant.BRACE_LEFT)) {
                //左花括号{++
                braceTotal++;
            } else if (line.contains(Constant.BRACE_RIGHT)) {
                //左花括号{--
                braceTotal--;
            }
            if (line.length() == 1) {
                if (braceTotal == 1) {
                    if (inData) {
                        inData = false;
                    } else if (inCreated) {
                        inCreated = false;
                        //退出了create，如果还有没被单独处理的数据，就需要在create里新增
                        for (String varName : varNameMap.keySet()) {
                            if (varNameMap.get(varName) == null){
                                continue;
                            }
                            //属于变量，一般是来自json，在data里直接用上就是无条件也不排序的
                            getDataAction = new GetDataAction();
                            getDataAction.setVueDataName(varName);
                            getDataAction.setJsonName(varNameMap.get(varName));
                            getJsonDataActionList.add(getDataAction);
                        }
                    }
                }
                if (getDataAction.getReplaceCodeList().size() > 0){
                    getDataAction.getReplaceCodeList().add(line);
                }
                continue;
            }
            if (StringUtil.isMatchRex(line, DATA_REX)) {
                inData = true;
            } else if (inData) {
                if (line.contains(RETURN)) {
                    continue;
                }
                //切割名称和值，冒号之前的key部分加上引号
                String[] kv = line.split(Constant.COLON_EN);
                //变量的值
                kv[1] = kv[1].replace(Constant.COMMA_EN, Constant.EMPTY).trim();
                if (!"null".equals(kv[1]) && StringUtil.isMatchRex(kv[1], Grammar.VAR_REX)) {
                    //先记下来，因为有时候又在created里面重新赋值了，单对data里忘了修改成null
                    kv[0] = kv[0].trim();
                    varNameMap.put(kv[0], kv[1]);
                }
            } else if (StringUtil.isMatchRex(line, CREATED_REX)) {
                //此时可以通过vue访问data中的数据和方法，还没创建虚拟dom，一般用于对数据提前处理
                inCreated = true;
            } else if (inCreated) {
                //排序
                String orderStr = StringUtil.getRex(line, ORDER_REX);
                if (orderStr != null) {
                    //直接排序
                    String thisOrderStr = StringUtil.getRex(line, THIS_ORDER_REX);
                    if (thisOrderStr != null) {
                        orderStr = line.replaceAll(THIS_ORDER_REX, Constant.EMPTY)
                                .replaceAll("\\)[ ]*", Constant.EMPTY)
                                .replaceAll("\"", Constant.EMPTY)
                                .replaceAll(Constant.SEMICOLON_EN, Constant.EMPTY);
                        getDataAction = new GetDataAction();
                        getDataAction.getReplaceCodeList().add(line);
                        getDataAction.setOrder(orderStr);
                        //赋值短语
                        getTheSetedPhrase(thisOrderStr, varNameMap, getDataAction);
                        String jsonName = thisOrderStr.substring(thisOrderStr.indexOf(Constant.EQ) + 1, thisOrderStr.indexOf(".sort")).trim();
                        getDataAction.setJsonName(jsonName);
                        getJsonDataActionList.add(getDataAction);
                    } else {
                        getDataAction.getReplaceCodeList().add(line);
                        line = line.replace(orderStr, Constant.EMPTY);
                        //筛选之后的排序
                        orderStr = orderStr.replaceAll(".sort\\([ ]*sortByKeys\\(", Constant.EMPTY)
                                .replaceAll("\\)[ ]*", Constant.EMPTY)
                                .replaceAll("\"", Constant.EMPTY)
                                .replaceAll(Constant.SEMICOLON_EN, Constant.EMPTY);
                        //排序条件
                        getDataAction.setOrder(orderStr);
                    }
                }
                if (inFilter) {
                    //有排序的情况，
                    if (line.equals(";") || line.equals(");")) {
                        if (getDataAction.getOrder() == null){
                            //没有排序的，需要增加
                            getDataAction.getReplaceCodeList().add(line);
                        }
                        inFilter = false;
                        getDataAction.setCondition(conditionBuilder.toString().replace(RETURN, Constant.EMPTY).trim());
                        getJsonDataActionList.add(getDataAction);
                        //数据初始化
                        getDataAction = new GetDataAction();
                        conditionBuilder.delete(0, conditionBuilder.length());
                    }else {
                        conditionBuilder.append(line);
                    }
                    getDataAction.getReplaceCodeList().add(line);
                } else {
                    //寻找过滤赋值
                    String filterStr = StringUtil.getRex(line, FILTER_REX);
                    if (filterStr != null) {
                        inFilter = true;
                        getDataAction = new GetDataAction();
                        getDataAction.getReplaceCodeList().add(line);
                        //赋值短语
                        getTheSetedPhrase(filterStr, varNameMap, getDataAction);
                        //拿到对应的jsonName
                        String jsonName = filterStr.substring(filterStr.indexOf(Constant.EQ) + 1
                                , filterStr.indexOf(".filter")).trim();
                        getDataAction.setJsonName(jsonName);
                    }
                }
            }
        }
        return getJsonDataActionList;
    }

    /**
     * 拿到赋值的短语，并从data移除
     * @param s 赋值语句
     * @param varNameMap data直接赋值的映射
     * @param getDataAction 赋值的实体
     */
    private static void getTheSetedPhrase(String s
            , Map<String, String> varNameMap
            , GetDataAction getDataAction){
        String theSetedPhrase = s.substring(0, s.indexOf(Constant.EQ) + 1);
        if (theSetedPhrase.contains("this.")) {
            String vueVarName = theSetedPhrase.substring(5, theSetedPhrase.indexOf(Constant.EQ)).trim();
            //这里单独处理了，data里用的
            varNameMap.put(vueVarName, null);
            //data中的变量
            getDataAction.setVueDataName(vueVarName);
        }
        getDataAction.setTheSetedPhrase(theSetedPhrase);
    }
}
