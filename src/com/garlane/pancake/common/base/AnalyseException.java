package com.garlane.pancake.common.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *	系统异常处理基类类
 *	@author lixingfa
 *	@date 2022年1月17日 上午9:24:44
 *
 */
public class AnalyseException extends RuntimeException{

	private static final long serialVersionUID = 1L;
	
	protected Logger log = LoggerFactory.getLogger(this.getClass());
	
	public AnalyseException(){
	}  
	
	public AnalyseException(String message) {
        super(message);
    }  
	
	public AnalyseException(Throwable t) {
        super(t);
    }
	
	public AnalyseException(String message, Throwable t) {
        super(message,t);
    }
	
	public AnalyseException(String message, Exception e){
		super(message,e);
		log.error("系统类异常，异常如下：");
	}  

}

