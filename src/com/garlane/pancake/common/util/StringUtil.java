/**
 * 
 */
package com.garlane.pancake.common.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.alibaba.fastjson.JSONArray;
import com.garlane.pancake.common.constant.Constant;

/**
 * @author lixingfa
 * @date 2019年4月16日下午11:24:33
 * 
 */
public class StringUtil {
	
	/**字母字符串*/
	public static final String CHAR_REX = "[a-zA-Z]+";
	/**yyyy年MM月 年月在excel里只有这种格式，其他形式会被认为是小数或表达式*/
	public static final String TIME_REX = "[0-9]{1,4}年[0-9]{1,2}月";
	/**yyyy-MM-dd年月日常见格式*///-号也是特殊字符，所以要转义，但单独出现时不需要，与其他特殊字符一起时需要
	public static final String TIME_REX1 = "[0-9]{1,4}[年\\-/.]{1}[0-9]{1,2}[月\\-/.]{1}[0-9]{1,2}[日]{0,1}";
	/**yyyy-MM-dd HH年月日时常见格式*/
	public static final String TIME_REX2 = "[0-9]{1,4}[年\\-/.]{1}[0-9]{1,2}[月\\-/.]{1}[0-9]{1,2}[日]{0,1}[ \n]{1}[0-9]{1,2}[时]{0,1}";
	/**yyyy-MM-dd HH:mm年月日时分常见格式*/
	public static final String TIME_REX3 = "[0-9]{1,4}[年\\-/.]{1}[0-9]{1,2}[月\\-/.]{1}[0-9]{1,2}[日]{0,1}[ \n]{1}[0-9]{1,2}[时:]{1}[0-9]{1,2}[分]{0,1}";
	/**yyyy-MM-dd HH:mm:ss年月日时分秒常见格式*/
	public static final String TIME_REX4 = "[0-9]{1,4}[年\\-/.]{1}[0-9]{1,2}[月\\-/.]{1}[0-9]{1,2}[日]{0,1}[ \n]{1}[0-9]{1,2}[时:]{1}[0-9]{1,2}[分:]{1}[0-9]{1,2}[秒]{0,1}";
	/**HH:mm:ss时分秒常见格式*/
	public static final String TIME_REX5 = "{1}[0-9]{1,2}[时:]{1}[0-9]{1,2}[分:]{1}[0-9]{1,2}[秒]{0,1}";
	/**标准时间字符串*/
	public static final String TIME_STRING = "yyyy-MM-dd HH:mm:ss";
	/**连续的时间*/
	public static final String TIME_STRING2 = "yyyyMMddHHmmss";
	/**连续的时间和四位毫秒微秒纳秒*/
	public static final String TIME_STRING3 = "yyyyMMddHHmmssSSSS";
	/**年月*/
	public static final String TIME_STRING4 = "yyyy-MM";
	/**年月日*/
	public static final String TIME_STRING5 = "yyyy-MM-dd";
	/**年月日时*/
	public static final String TIME_STRING6 = "yyyy-MM-dd HH";
	/**年月日时分*/
	public static final String TIME_STRING7 = "yyyy-MM-dd HH:mm";
	/**时分秒*/
	public static final String TIME_STRING8 = "HH:mm:ss";
	
	/**得到一堆字符串的正则表达式*/
	public static String getStringRex(List<String> list) {
		List<List<Integer>> ranges = getSubStrRanges(list);
		StringBuffer rex = new StringBuffer();
		if (ranges != null) {
			String s = list.get(0);
			//处理第一个
			List<Integer> range = ranges.get(0);
			int start = range.get(0);
			if (start != 0) {//子串不是从0开始的
				String sub =s.substring(0, start + 1);
				sub = getStringRex(sub);
				rex.append(sub);
			}
			rex.append(s.substring(range.get(0), range.get(1) + 1));//子串
			//循环处理当前子串的上限和下一个的下限
			int end = ranges.size() - 1;
			for (int i = 0; i < end; i++) {
				range = ranges.get(i);
				String sub = s.substring(range.get(1), ranges.get(i + 1).get(0) + 1);
				rex.append(getStringRex(sub));
				rex.append(s.substring(ranges.get(i + 1).get(0), ranges.get(i + 1).get(1)));
			}
			//最后一个子串之后的
			if (end > 0) {
				end = ranges.get(end).get(1) + 1;
				if (end < s.length() - 1) {
					String sub = s.substring(end, s.length());
					rex.append(getStringRex(sub));
				}				
			}
			//得到共同部分后，看后面接的字符
			if (rex.length() < s.length()) {
				int min = Integer.MAX_VALUE;
				int max = 0;
				List<String> subList = new ArrayList<>();
				for (String str : list) {
					String sub = str.substring(rex.length(), str.length());
					if (sub.length() > max) {
						max = sub.length();
					}
					if (sub.length() < min) {
						min = sub.length();
					}
					subList.add(sub);
				}
				//收集所有后接字符
				String rexStr = getSubListRex(subList);
				if (rexStr == null) {
					rex.append("[\\w]");//任意字符					
				}else {
					rex.append(rexStr.substring(0, rexStr.length() - 1));//总结出来的字符
				}
				rex.append("{").append(min).append(",").append(max).append("}");
			}
		}else {
			//没有共同的子串，则看看是否都是数字
			int min = Integer.MAX_VALUE;
			int max = 0;
			for (String str : list) {
				if (str.length() > max) {
					max = str.length();
				}
				if (str.length() < min) {
					min = str.length();
				}
			}
			//收集所有后接字符
			String rexStr = getSubListRex(list);
			if (rexStr != null) {
				rex.append(rexStr.substring(0, rexStr.length() - 1));//总结出来的字符
				rex.append("{").append(min).append(",").append(max).append("}");
			}
		}
		return rex.toString();
	}
	
	/**
	 * 获取些字符串子串的正则表达式
	 * @param s 字符串
	 * @return String 正则表达式
	 */
	public static String getSubListRex(List<String> s) {
		Set<String> rexSet = new HashSet<>();
		for (String str : s) {
			rexSet.add(getStringRex(str));
		}
		if (rexSet.size() != 1) {
			return null;
		}else {
			return new ArrayList<String>(rexSet).get(0);
		}
	}
	
	/**
	 * 获取某个字符串的正则表达式
	 * @param s
	 * @return String
	 */
	public static String getStringRex(String s) {
//		if (NumberUtil.isNum(s)) {
//			return NumberUtil.getNumRex(s);
//		}else if (isMatchRex(s, CHAR_REX)) {
//			return CHAR_REX;
//		}
		//各种数字的、字母的、字母数字混合的表达式返回
		StringBuffer sBuffer = new StringBuffer();
		String rex = null;
		int total = 0;
		char[] cs = s.toCharArray();
		for (char c : cs){
			if('A' <= c && c <= 'z'){
				if (!"[A-Za-z]".equals(rex)){
					if(rex != null){
						sBuffer.append(rex).append("{").append(total).append("}");
					}
					rex = "[A-Za-z]";
					total = 0;
				}
			}else if('0' <= c && c <= '9'){
				if (!"[0-9]".equals(rex)){
					if(rex != null){
						sBuffer.append(rex).append("{").append(total).append("}");
					}
					rex = "[0-9]";
					total = 0;
				}
			}
			total++;
		}
		if (rex != null){
			sBuffer.append(rex).append("{").append(total).append("}");
		}
		return sBuffer.toString();
	}
	
	/**
	 * 
	 * 获取字符串共同的子串
	 * @param list
	 * @return String
	 */
	public static List<String> getSubStrings(List<String> list) {
		List<List<Integer>> ranges = getSubStrRanges(list);
		List<String> subList = new ArrayList<>();
		if (ranges != null) {
			String s = list.get(0);
			for (List<Integer> range : ranges) {
				subList.add(s.substring(range.get(0), range.get(1) + 1));
			}
			return subList;
		}
		return subList;
	}
	
	/**
	 * 获取一组字符串子串的起止索引
	 * @param list 字符串列表
	 * @return List<List<Integer>> 子串的起止位置
	 */
	public static List<List<Integer>> getSubStrRanges(List<String> list){		
		Map<Character, Integer> charNum = new HashMap<>();//char在每个String中出现的次数
		List<Map<Character, List<Integer>>> cIndexsList = new ArrayList<>();//char在每个String中的索引
		for (String s : list) {
			char[] cs = s.toCharArray();
			Map<Character, List<Integer>> cIndexs = new HashMap<>();//字符在字符串中的索引
			//记录索引位置
			for (int i = 0; i < cs.length; i++) {
				char c = cs[i];
				if (!cIndexs.containsKey(c)) {
					cIndexs.put(c, new ArrayList<>());
				}
				cIndexs.get(c).add(i);//字符的索引
			}
			cIndexsList.add(cIndexs);
			//记录在所有字符串中的出现次数（不是本字符串的出现次数）
			for (char c : cIndexs.keySet()) {
				charNum.put(c, charNum.getOrDefault(c, 0) + 1);
			}
		}
		//寻找在每个String中都出现的
		List<Integer> nums = new ArrayList<>();
		for (char c : charNum.keySet()) {
			if (charNum.get(c) == list.size()) {//在每个字符串中都有出现
				List<Integer> indexs = cIndexsList.get(0).get(c);
				for (int i = 1; i < cIndexsList.size(); i++) {
					indexs.retainAll(cIndexsList.get(i).get(c));
				}
				if (indexs.size() > 0) {//在所有字符串中有共同索引
					nums.addAll(indexs);						
				}
			}
		}
		if (nums.size() > 0) {
			//得到取值范围，即各子串的起止位置
			return NumberUtil.getNumRanges(nums);
		}
		return null;
	}
	
	/***
	 * getUUID:(获得uuid编号)
	 * @author lixingfa
	 * @date 2018年7月4日下午7:18:21
	 * @return
	 */
	public static String getUUID(){
		return UUID.randomUUID().toString().replaceAll(Constant.SHORT_BAR, "");
	}
	
	/**是否匹配正则表达式
	 * @param s 字符串
	 * @return 
	 */
	public static boolean isMatchRex(String s,String rex){
		if (Pattern.matches(rex, s)) {
			return true;
		}else {
			return false;
		}
	}
	
	/**
	 * 获取正则表达式匹配的内容
	 * @param s 字符串
	 * @param rex 正则表达式
	 * @return Set<String>
	 */
	public static Set<String> getRexs(String s,String rex){
		Set<String> list = new HashSet<>();
		Pattern pattern = Pattern.compile(rex);
		// 现在创建 matcher 对象
		Matcher m = pattern.matcher(s);
		while (m.find()) {
			list.add(m.group());
		}
		return list;
	}

	/**
	 * 确定只有一个时，获取匹配的内容
	 * @param s 字符串
	 * @param rex 正则表达式
	 * @return String 匹配的内容
	 */
	public static String getRex(String s,String rex){
		String content = null;
		Pattern pattern = Pattern.compile(rex);
		// 现在创建 matcher 对象
		Matcher m = pattern.matcher(s);
		while (m.find()) {
			content = m.group();
		}
		return content;
	}
	/**
	 * 提供yyyy-MM-dd HH:mm:ss型的时间戳
	 * @return
	 */
	public static String getTime() {
		SimpleDateFormat df = new SimpleDateFormat(TIME_STRING);//设置日期格式
		return df.format(new Date());// new Date()为获取当前系统时间，也可使用当前时间戳
	}
	/**
	 * 提供yyyyMMddHHmmss型的时间戳
	 * @return
	 */
	public static String getTimeStr() {
		SimpleDateFormat df = new SimpleDateFormat(TIME_STRING2);//设置日期格式
		return df.format(new Date());// new Date()为获取当前系统时间，也可使用当前时间戳
	}
	
	/**
	 * 提供yyyyMMddHHmmssSSSS型的时间戳
	 * @return
	 */
	public static String getTimeStrSSSS() {
		SimpleDateFormat df = new SimpleDateFormat(TIME_STRING3);//设置日期格式
		return df.format(new Date());// new Date()为获取当前系统时间，也可使用当前时间戳
	}
	
	/**
	 * 获取子串在某个字符串里的所有位置
	 * @param s 字符串
	 * @param ch 子串
	 * @return List<Integer>
	 */
	public static List<Integer> indexOfs(String s,String ch){
		List<Integer> indexs = new ArrayList<>();
		indexOfs(s, ch,0, indexs);
		return indexs;
	}
	private static void indexOfs(String s,String ch,int fromIndex,List<Integer> indexs){
		int index = s.indexOf(ch,fromIndex);//从fromIndex开始找
		if (index != -1) {
			indexs.add(index);
			fromIndex = index + 1;//在找到的位置往后移一位
			indexOfs(s, ch,fromIndex, indexs);//继续找下去
		}else if(indexs.size() == 0){//字符串不包含要找的内容，返回-1
			indexs.add(index);			
		}
	}
	
	/**
	 * 某个对象在列表里的索引
	 * @param list
	 * @param obj
	 * @return List<Integer>
	 */
	public static List<Integer> indexOfs(List list,Object obj){
		return indexOfs(new JSONArray(list), obj);
	}
	/**
	 * 某个对象在列表里的索引
	 * @param s
	 * @param ch
	 * @return List<Integer>
	 */
	public static List<Integer> indexOfs(JSONArray s,Object ch){
		List<Integer> indexs = new ArrayList<>();
		indexOfs(JSONArray.parseArray(s.toJSONString()), ch,indexs);//不改变参数
		return indexs;
	}
	private static void indexOfs(JSONArray s,Object ch,List<Integer> indexs){
		int index = s.indexOf(ch);//从fromIndex开始找
		if (index != -1) {
			indexs.add(index);
			s.set(index, null);//置空
			indexOfs(s, ch, indexs);//继续找下去
		}else if(indexs.size() == 0){//字符串不包含要找的内容，返回-1
			indexs.add(index);			
		}
	}
	
	/**
	 * 保留字符串中特定内容，其余替换掉并简化
	 * @param s 字符串
	 * @param need 需要保留的内容
	 * @param reStr 替换的字符串
	 * @return String
	 *
	public static String replacWithNeed(String s,String need,String reStr) {
		//replaceAll 用到正则表达式
		//把保留字以外替换成A，会出现多个A
		s = s.replaceAll(Constant.BRACKET_LEFT + JSONConstant.REX_BEGIN + need + Constant.BRACKET_RIGHT, reStr);
		//把多个A替换成一个
		s = s.replaceAll(Constant.BRACKET_LEFT + JSONConstant.A + Constant.BRACKET_RIGHT + Constant.PLUS, reStr);
		return s;
	}*/
	
	/**
	 * 获得正则表达式
	 * @param s 字符串
	 * @param varValue 变量
	 * @return 正则表达式
	 *
	public static String getRexStringByVar(String s,String varValue) {
		boolean star = s.startsWith(varValue);
		boolean end = s.endsWith(varValue);		
		if (star && end) {
			s = s.replace(varValue, "");
		}else if (star) {
			s = s.replace(varValue, "") + JSONConstant.REX_END;
		}else if(end){
			s = JSONConstant.REX_BEGIN + s.replace(varValue, "");
		}
		return s;
	}*/

}
