package com.garlane.pancake.common.util;

import com.alibaba.fastjson.JSONArray;
import com.garlane.pancake.common.base.AnalyseException;
import com.garlane.pancake.common.constant.Constant;
import com.garlane.pancake.common.model.View;

import java.io.File;
import java.util.*;

public class HtmlUtil {
    /**html文件*/
    private static final String HTML = ".html";
    /**不扫描的目录*/
    private static final String NOT_SCAN_DIR = "assets,js";

    /**
     * 页面解析、替换和重写
     * @param dir page所在的目录
     * @param pageJsonArraysMap json列表，jsonPath-jsonName-JSONArray
     * @param jsonArraysOwnKeyMap json数组的注释，位于数组上面，说明哪些驼峰key是属于主体的，比如creatTime
     * @param isCover 是否覆盖，是则直接改写原来的内容，用于web场景；否则新建文件，用于插件场景
     * @return List<View> 页面模型
     * @throws AnalyseException
     */
    public static List<View> htmlAnalysis(File dir, Map<String, Map<String, JSONArray>> pageJsonArraysMap
            , Map<String, List<String>> jsonArraysOwnKeyMap, boolean isCover) throws AnalyseException {
        List<View> views = new ArrayList<>();
        //获取html的内容，并解析出资源目录
        try {
            String projectPath = dir.getAbsolutePath();
            String projectName = dir.getName();
            //遍历html文件
            Set<String> srcDirsSet = new HashSet<>();
            File[] subFiles = dir.listFiles();
            //先处理最外面的html，为更快找出引用的文件夹，暂时不处理子文件夹里的html
            for (File file : subFiles) {
                if (file.isFile() && file.getName().endsWith(HTML)) {
                    //寻找引用文件夹和替换
                    View view = new View(projectPath, projectName);
                    view.htmlAnalysis(file, pageJsonArraysMap, jsonArraysOwnKeyMap, isCover, srcDirsSet);
                    views.add(view);
                }
            }
            //处理子文件夹里的html
            for (File file : subFiles) {
                if (file.isDirectory()) {
                    views.addAll(replaceSubHtml(file, srcDirsSet, pageJsonArraysMap, jsonArraysOwnKeyMap, projectPath, projectName, isCover));
                }
            }
        } catch (Exception e) {
            if (isCover) {
                //可以覆盖改写，出错了就可以删除，重新上传即可
                FileUtil.FileDelete(dir);
            }
            throw new AnalyseException(e.getMessage());
        }
        return views;
    }

    /**
     * 处理子目录里的html
     * @param file 文件（夹）
     * @param srcDirsSet 引用的父级目录，即处理过的路径，因为同一个json会被多个页面引用
     * @param pageJsonArraysMap json列表
     * @param jsonArraysOwnKeyMap json数组的注释，位于数组上面，说明哪些驼峰key是属于主体的，比如creatTime
     * @param projectPath 解压后的项目路径
     * @param projectName 项目名称
     * @param isCover 是否覆盖，是则直接改写原来的内容，用于web场景；否则新建文件，用于插件场景
     * @return List<View>
     */
    private static List<View> replaceSubHtml(File file
            , Set<String> srcDirsSet
            , Map<String, Map<String, JSONArray>> pageJsonArraysMap
            , Map<String, List<String>> jsonArraysOwnKeyMap
            , String projectPath
            , String projectName
            , boolean isCover) throws Exception{
        List<View> views = new ArrayList<>();
        if (file.isDirectory()) {
            List<String> notScanDir = Arrays.asList(NOT_SCAN_DIR);
            String dirName = file.getName();
            //跳过以英文点号开始的文件和文件夹，一般是工程辅助文件，比如svn和git
            if (dirName.startsWith(Constant.POINT_EN)
                    //change目录用于比较前后变化，不用处理
                    || notScanDir.contains(dirName)
                    //已经处理过了
                    || srcDirsSet.contains(dirName)
            ) {
                return views;
            }
            File[] subFiles = file.listFiles();
            for (File f : subFiles) {
                views.addAll(replaceSubHtml(f, srcDirsSet, pageJsonArraysMap, jsonArraysOwnKeyMap, projectPath, projectName, isCover));
            }
        }else if (file.isFile() && file.getName().endsWith(HTML)) {
            //寻找引用文件夹和替换
            View view = new View(projectPath, projectName);
            view.htmlAnalysis(file, pageJsonArraysMap, jsonArraysOwnKeyMap, isCover, srcDirsSet);
            //TODO vue解析
            views.add(view);
        }
        return views;
    }

}
