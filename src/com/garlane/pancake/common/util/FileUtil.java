package com.garlane.pancake.common.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.garlane.pancake.common.constant.Constant;
/**
 * @author lixingfa
 * @date 2018年4月2日下午4:25:46
 * 
 */
public class FileUtil {
	private final static Logger log = LoggerFactory.getLogger(FileUtil.class);	
	
	/**文件内容列表<行列表>*/
	private static List<List<String>> fileContents = new ArrayList<List<String>>();
	
	public enum FileType {
	    // 未知
	    UNKNOWN,
	    // 压缩文件
	    ZIP, RAR, _7Z, TAR, GZ, TAR_GZ, BZ2, TAR_BZ2,
	    // 位图文件
	    BMP, PNG, JPG, JPEG,
	    // 矢量图文件
	    SVG,
	    // 影音文件
	    AVI, MP4, MP3, AAR, OGG, WAV, WAVE
	}

	/**
	 * 判断文件的编码格式
	 * @param filePath 文件路径
	 * @return 文件编码格式
	 * @throws Exception
	 */
	public static String codeString(String filePath) throws Exception{
		BufferedInputStream bin = new BufferedInputStream(new FileInputStream(filePath));
		int p = (bin.read() << 8) + bin.read();
		String code = null;		
		switch (p) {
			case 0xefbb:
				code = "UTF-8";
				break;
			case 0xfffe:
				code = "Unicode";
				break;
			case 0xfeff:
				code = "UTF-16BE";
				break;
			default:
				code = "GBK";
		}
		bin.close();
		return code;
	}

	/**
	 * 获取文件真实类型，（通过文件头信息来判断什么类型的，其他文件的头文件信息，如果有需要，可以拿文件来跑跑，看看headHex是啥值就行了。）
	 * @param file 要获取类型的文件。
	 * @return 文件类型枚举。
	 */
	public static FileType getFileType(File file){
	    FileInputStream inputStream =null;
	    try{
	        inputStream = new FileInputStream(file);
	        byte[] head = new byte[4];
	        if (-1 == inputStream.read(head)) {
	            return FileType.UNKNOWN;
	        }
	        int headHex = 0;
	        for (byte b : head) {
	            headHex <<= 8;
	            headHex |= b;
	        }
	        switch (headHex) {
	            case 0x504B0304:
	                return FileType.ZIP;
	            case 0x776f7264:
	                return FileType.TAR;
	            case -0x51:
	                return FileType._7Z;
	            case 0x425a6839:
	                return FileType.BZ2;
	            case -0x74f7f8:
	                return FileType.GZ;
	            case 0x52617221:
	                return FileType.RAR;
	            default:
	                return FileType.UNKNOWN;
	        }
	    }catch (Exception e){
	        e.printStackTrace();
	    }finally {
	        try {
	            if(inputStream!=null){
	                inputStream.close();
	            }
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }
	    return FileType.UNKNOWN;
	}

	/**
	 * 将文件（夹）移动到目标路径
	 * @param sourceFile
	 * @param targetPath
	 */
	public static void fileMove(File sourceFile, String targetPath) throws Exception{
		if (!sourceFile.exists()) {
			throw new Exception("源文件不存在。");
		}
		if (sourceFile.isDirectory()) {
			//目标文件夹已存在也会报错
			FileUtils.moveDirectory(sourceFile, new File(targetPath));
		}else {
			FileUtils.moveFile(sourceFile, new File(targetPath + File.separator + sourceFile.getName()));
		}
	}
	//--------------------------------------------------读取相关------------------------------
	/**
	 * 获取某个文件夹下所有文件及其子文件夹下所有文件的内容
	 * @param pagePath 文件（夹）路径
	 * @return 文件内容列表<行列表>
	 */
	public static List<List<String>> getFileContentsByLines(String pagePath) throws Exception{
		File pageFile = new File(pagePath);
		if (pageFile.isDirectory()) {//是目录
			log.info("\t目录"+pageFile.getName());
			File[] subFiles = pageFile.listFiles();
			for (int i = 0; i < subFiles.length; i++) {
				getFileContentsByLines(subFiles[i].getPath());
			}
		}else {
			try {
				//通过行读取
				fileContents.add(readFileByLines(pagePath));
			} catch (Exception e) {
				log.error("通过行读取文件出错，文件路径：" + pagePath, e);
				throw e;
			}
			log.info("读取"+pageFile.getName()+"的内容");			
		}
		return fileContents;
	}
	
	/**
     * 以字节为单位读取文件，常用于读二进制文件，如图片、声音、影像等文件。
     */
    public static void readFileByBytes(String fileName) throws Exception{
        File file = new File(fileName);
        InputStream in = null;
        try {
            // 一次读一个字节
            in = new FileInputStream(file);
            int tempbyte;
            while ((tempbyte = in.read()) != -1) {
                System.out.write(tempbyte);
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        try {
            log.info("以字节为单位读取文件内容，一次读多个字节：");
            // 一次读多个字节
            byte[] tempbytes = new byte[100];
            int byteread = 0;
            in = new FileInputStream(fileName);
            // 读入多个字节到字节数组中，byteread为一次读入的字节数
            while ((byteread = in.read(tempbytes)) != -1) {
                System.out.write(tempbytes, 0, byteread);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e1) {
                }
            }
        }
    }

    /**
     * 以字符为单位读取文件，常用于读文本，数字等类型的文件
     */
    public static void readFileByChars(String fileName) {
        File file = new File(fileName);
        Reader reader = null;
        try {
            log.info("以字符为单位读取文件内容，一次读一个字节：");
            // 一次读一个字符
            reader = new InputStreamReader(new FileInputStream(file));
            int tempchar;
            while ((tempchar = reader.read()) != -1) {
                // 对于windows下，\r\n这两个字符在一起时，表示一个换行。
                // 但如果这两个字符分开显示时，会换两次行。
                // 因此，屏蔽掉\r，或者屏蔽\n。否则，将会多出很多空行。
                if (((char) tempchar) != '\r') {
                    System.out.print((char) tempchar);
                }
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            log.info("以字符为单位读取文件内容，一次读多个字节：");
            // 一次读多个字符
            char[] tempchars = new char[30];
            int charread = 0;
            reader = new InputStreamReader(new FileInputStream(fileName));
            // 读入多个字符到字符数组中，charread为一次读取字符数
            while ((charread = reader.read(tempchars)) != -1) {
                // 同样屏蔽掉\r不显示
                if ((charread == tempchars.length)
                        && (tempchars[tempchars.length - 1] != '\r')) {
                    System.out.print(tempchars);
                } else {
                    for (int i = 0; i < charread; i++) {
                        if (tempchars[i] == '\r') {
                            continue;
                        } else {
                            System.out.print(tempchars[i]);
                        }
                    }
                }
            }

        } catch (Exception e1) {
            e1.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                }
            }
        }
    }

    /** 以行为单位读取文件，常用于读行的格式化文件，如文本等
     * 
     * @param fileName
     * @return 内容列表，每个值代表一行
     */
    public static List<String> readFileByLines(String fileName) throws Exception{
    	List<String> arrayList = new ArrayList<String>();
    	if (!new File(fileName).exists()) {
			return arrayList;
		}
        BufferedReader reader = null;
        int line = 1;
        try {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), Charset.forName("utf-8")));//gbk转utf-8
            String tempString = null;
            // 一次读入一行，直到读入null为文件结束
            while ((tempString = reader.readLine()) != null) {
            	line++;
            	arrayList.add(tempString);
            }
            reader.close();
        } catch (IOException e) {
        	throw new Exception(fileName + "第" + line + "读取错误");
        } finally {
            if (reader != null) {
                try {
                    reader.close();
	                } catch (IOException e1) {
	                }
            }
        }
        return arrayList;
    }


    /**
     * getFileString:(获取某个文件的内容)
     * @author lixingfa
     * @date 2018年4月2日下午4:56:21
     * @param fileName
     * @param keepLines 是否保留换行
     * @return String
     */
    public static String getFileString(String fileName,boolean keepLines) throws Exception{
    	if (new File(fileName).isDirectory()) {
			throw new Exception("读取文件内容出错，" + fileName + "是一个文件夹！");
		}
        StringBuffer sBuffer = new StringBuffer();
        BufferedReader reader = null;
        int line = 1;
        try {
            //reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), Charset.forName("GBK")));//gbk转utf-8
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
            String tempString = null;
            // 一次读入一行，直到读入null为文件结束
            while ((tempString = reader.readLine()) != null) {
                sBuffer.append(tempString);
                if (keepLines) {
					sBuffer.append("\r\n");
				}
                line++;
            }
            reader.close();
        } catch (IOException e) {
            log.info("读取第" + line + "发生错误");
            throw e;
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                	throw e1;
                }
            }
        }
        return sBuffer.toString();
    }

    /**
     * getDirectoryContent:(获取某个文件或文件夹下的内容，不包含子文件夹的，以<文件,文件>内容字符串的形式返回)
     * @author lixingfa
     * @date 2018年4月2日下午4:56:21
     * @param dirPath
     * @param shortNameType 短名类型，0、完整路径；1、只要文件名，不要后缀；2、要文件名称和后缀
     * @param keepLins 是否保留换行
     * @return Map<String, String>
     */
    public static Map<String, String> getDirectoryContent(String dirPath,int shortNameType,boolean keepLins) throws Exception{
    	Map<String, String> map = new HashMap<String, String>();
    	File file = new File(dirPath);
    	if (file.isDirectory()) {//是目录
			File[] subFiles = file.listFiles();
			for (int i = 0; i < subFiles.length; i++) {
				map.putAll(getDirectoryContent(subFiles[i].getAbsolutePath(),shortNameType,keepLins));
			}
		}else {
			switch (shortNameType) {
				case 0://完整路径
					map.put(dirPath, getFileString(dirPath,keepLins));					
					break;
				case 1://只要文件名，不要后缀
					map.put(dirPath.substring(dirPath.lastIndexOf(Constant.LEFT_FORWARD) + 1, dirPath.lastIndexOf(".")), getFileString(dirPath,keepLins));
					break;
				case 2://要文件名和后缀
					map.put(dirPath.substring(dirPath.lastIndexOf(Constant.LEFT_FORWARD) + 1), getFileString(dirPath,keepLins));
					break;
				default:
					map.put(dirPath, getFileString(dirPath,keepLins));
					break;
			}
		}
    	return map;
    }
    
    /**
     * getDirectoryContent:(获取某个文件或文件夹下的内容，不包含子文件夹的，以<文件,文件>内容字符串的形式返回)，内容保留换行
     * @author lixingfa
     * @date 2018年4月2日下午4:56:21
     * @param dirPath
     * @param shortNameType 短名类型，0、完整路径；1、只要文件名，不要后缀；2、要文件名称和后缀
     * @return Map<String, String>
     */
    public static Map<String, String> getDirectoryContent(String dirPath,int shortNameType) throws Exception{
    	return getDirectoryContent(dirPath, shortNameType, true);
    }
    
  //--------------------------------------------------写相关------------------------------    
    /**
     * 写入文件  覆盖
     * @param string
     * @param path
     * @return
     */
    public static boolean writeTxtFile(String string, String path) {
       PrintStream stream=null;
        try {
            stream = new PrintStream(path);//写入的文件path
            stream.print(string);//写入的字符串
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }finally {
			stream.close();
		}
        return false;
    }
    
    /**
     * 非覆盖的写入文件，在原有的文件内容后面继续写入
     * @param string
     * @param path
     * @return
     */
    public static boolean addFile(String string, String path) {
        //文件的续写
        FileWriter fw = null;
        try {
            fw = new FileWriter(path,true);
            //写入换行
            fw.write("\r\n");//Windows平台下用\r\n，Linux/Unix平台下用\n
            //续写一个hello world!
            fw.write(string);
            fw.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
			try {
				fw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
        return false;
    }
  //--------------------------------------------------删除压相关------------------------------
    /**
     * 文件删除
     * @param file 文件或文件夹
     */
    public static void FileDelete(File file){
    	if (file.isDirectory()) {
			File[] subFiles = file.listFiles();
			for (int i = 0; i < subFiles.length; i++) {
				FileDelete(subFiles[i]);
			}
		}else {
			file.delete();
		}
    }
}
