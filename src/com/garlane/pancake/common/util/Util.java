package com.garlane.pancake.common.util;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class Util {	
	/**
	 * inArrays:(判断字符是否在数组里)
	 * @author lixingfa
	 * @date 2019年4月3日上午9:46:40
	 * @param c 字符
	 * @param s 字符串数组
	 * @return boolean
	 */
	public static boolean inArrays(char c,String[] s){
		return inArrays(String.valueOf(c), s);
	}
	
	/**
	 * inArrays:(判断字符串是否在数组里)
	 * @author lixingfa
	 * @date 2019年4月3日上午9:46:40
	 * @param t 字符串
	 * @param s 字符串数组
	 * @return boolean
	 */
	public static boolean inArrays(String t,String[] s){
		if (t == null) {
			return false;
		}
		for (String string : s) {
			if (string.equals(t)) {
				return true;
			}
		}
		return false;
	}
	/**
	 * inArrays:(判断字符串是否在数组里)
	 * @author lixingfa
	 * @date 2019年4月3日上午9:46:40
	 * @param t 字符串
	 * @param s 字符串数组JSONArray形式
	 * @return boolean
	 */
	public static boolean inArrays(String t,JSONArray s){
		if (t == null) {
			return false;
		}
		for (int i = 0 ; i < s.size(); i++) {
			if (s.getString(i).equals(t)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * inArrays:(判断字符串是否在数组里)
	 * @author lixingfa
	 * @date 2019年4月3日上午9:46:40
	 * @param t 字符串
	 * @param s 字符串数组List形式
	 * @return boolean
	 */
	public static boolean inArrays(String t,List<String> s){
		if (t == null) {
			return false;
		}
		for (int i = 0 ; i < s.size(); i++) {
			if (s.get(i).equals(t)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 统计key-value 之间有多少个
	 * @param rela Map<Object, Map<Object,Integer>>
	 * @param key
	 * @param value
	 */
	public static void mapTotal(Map<Object, Map<Object,Integer>> rela,Object key,Object value) {
		if (rela.containsKey(key)) {
			if (rela.get(key).containsKey(value)) {
				rela.get(key).put(value, rela.get(key).get(value) + 1);
			}else {
				rela.get(key).put(value, 1);
			}
		}else {
			Map<Object, Integer> map = new HashMap<>();
			map.put(value, 1);
			rela.put(key, map);
		}
	}
	
	/**
	 * k-v-k-v
	 * @param kvMap1
	 * @param key1
	 * @param value1
	 * @param key2
	 * @param value2
	 */
	public static void mapTotal(Map<Object, Map<Object, Map<Object, Map<Object,Integer>>>> kvMap1,
			Object key1,Object value1, Object key2,Object value2) {
		if (kvMap1.containsKey(key1)) {
			Map<Object, Map<Object, Map<Object,Integer>>> vMap1 = kvMap1.get(key1);
			if (vMap1.containsKey(value1)) {
				Map<Object, Map<Object,Integer>> kvMap2 = vMap1.get(value1);
				if (kvMap2.containsKey(key2)) {
					Map<Object, Integer> vMap2 = kvMap2.get(key2);
					if (vMap2.containsKey(value2)) {
						vMap2.put(value2, vMap2.get(value2) + 1);
					}else {
						vMap2.put(value2, 1);
					}
				}else {
					Map<Object, Integer> vMap2 = new HashMap<>();
					vMap2.put(value2, 1);
					kvMap2.put(key2, vMap2);
				}
			}else {
				Map<Object, Integer> vMap2 = new HashMap<>();
				vMap2.put(value2, 1);
				Map<Object, Map<Object,Integer>> kvMap2 = new HashMap<>();
				kvMap2.put(key2, vMap2);			
				vMap1.put(value1, kvMap2);
			}
		}else {
			Map<Object, Integer> vMap2 = new HashMap<>();
			vMap2.put(value2, 1);
			Map<Object, Map<Object,Integer>> kvMap2 = new HashMap<>();
			kvMap2.put(key2, vMap2);
			Map<Object, Map<Object, Map<Object,Integer>>> vMap1 = new HashMap<>();			
			vMap1.put(value1, kvMap2);
			kvMap1.put(key1, vMap1);
		}
	}
	/**
	 * 把符合要求的数据放到列表里
	 * @param rela
	 * @param key
	 * @param value
	 */
	public static void mapList(Map<Object, Map<Object,List<JSONObject>>> rela,Object key,Object value,JSONObject json) {
		if (rela.containsKey(key)) {
			if (rela.get(key).containsKey(value)) {
				rela.get(key).get(value).add(json);
			}else {
				List<JSONObject> list = new ArrayList<>();
				list.add(json);
				rela.get(key).put(value, list);
			}
		}else {
			Map<Object, List<JSONObject>> map = new HashMap<>();
			List<JSONObject> list = new ArrayList<>();
			list.add(json);
			map.put(value, list);
			rela.put(key, map);
		}
	}
	
	/**
	 * 配对关系分组
	 * @param matchs 词组
	 * @return Map<String,List<JSONObject>>
	 *
	public static Map<String,List<JSONObject>> toRuleMap(JSONArray matchs) {
		Map<String,List<JSONObject>> map = new HashMap<>();
		for (int i = 0; i < matchs.size(); i++) {
			JSONObject match = matchs.getJSONObject(i);
			String rule = match.getString(LinkConstant.RULE);
			if (!map.containsKey(rule)) {
				map.put(rule, new ArrayList<>());
			}
			map.get(rule).add(match);			
		}
		return map;
	}*/
	
	/**
	 * 从备选数组里随机选取不少于一半的数据
	 * @param datas 备选数组
	 * @return List<JSONObject> 选取的数据
	 */
	public static List<JSONObject> getDatasByRandom(List<JSONObject> datas){
		List<JSONObject> datasTemp = new ArrayList<JSONObject>();
		int half = datas.size() / 2;
		int how = (int)(Math.random() * (half - 1)) + half;//避免了0和越界，而且过半
		Set<Integer> indexSet = new HashSet<Integer>();
		int num = 0;
		while (num < how) {
			int index = (int)(Math.random() * (datas.size() - 1));//避免越界
			JSONObject json = datas.get(index);
			if (indexSet.contains(index)) {
				continue;
			}
			indexSet.add(index);
			datasTemp.add(json);
			num++;
		}
		return datasTemp;
	}
	
	/**
	 * 按keySet随机筛选数据
	 * @param datas 待选数据
	 * @param keySetToJsonMap 空Map
	 * @return List<JSONObject>[] 占大头和小头的数据
	 */
	public static List<JSONObject>[] getDatasByRandom(List<List<JSONObject>> datas, Map<Set<String>, List<JSONObject>> keySetToJsonMap){
		//根据keySet分
		for (List<JSONObject> data : datas) {
			for (JSONObject json : data) {
				Set<String> keySet = json.keySet();
				if (!keySetToJsonMap.containsKey(keySet)) {
					keySetToJsonMap.put(keySet, new ArrayList<>());
				}
				keySetToJsonMap.get(keySet).add(json);				
			}
		}
		//从各个keySet里选出一部分，尽量确保各种情况都选到
		List<JSONObject> datasTemp = new ArrayList<>();
		List<JSONObject> others = new ArrayList<>();
		for (Set<String> keySet : keySetToJsonMap.keySet()) {
			List<JSONObject> list = new ArrayList<>(keySetToJsonMap.get(keySet));
			List<JSONObject> chooses = getDatasByRandom(list);
			datasTemp.addAll(chooses);
			others.addAll(removeAll(list, chooses));
		}
		List<JSONObject>[] ds = new ArrayList[2];
		ds[0] = datasTemp;
		ds[1] = others;
		return ds;
	}
	
	/**
	 * 移除
	 * @param <T>
	 * @param source
	 * @param destination
	 * @return List<T>
	 */
	public static <T> List<T> removeAll(List<T> source, List<T> destination) {
		List<T> result = new LinkedList<T>();
		Set<T> destinationSet = new HashSet<T>(destination);
		for(T t : source) {
			if (!destinationSet.contains(t)) {
				result.add(t);
			}
		}
		return result;
	}
	
	/**
	 * 按keySet随机筛选数据，但保证每种keySet的数据量一样
	 * @param keySetToJsonMap 空Map
	 * @return List<JSONObject>
	 */
	public static List<JSONObject> getDatasByRandomSame(Map<Set<String>, List<JSONObject>> keySetToJsonMap){
		//从各个keySet里选出一部分，尽量确保各种情况都选到
		int min = Integer.MAX_VALUE;
		Map<Set<String>, List<JSONObject>> keySetToJsonChooseMap = new HashMap<>();
		for (Set<String> keySet : keySetToJsonMap.keySet()) {
			List<JSONObject> list = keySetToJsonMap.get(keySet);
			List<JSONObject> chooses = getDatasByRandom(list);
			keySetToJsonChooseMap.put(keySet, chooses);
			if (min > chooses.size()) {
				min = chooses.size();
			}
		}
		//按最小的取一样的数量
		List<JSONObject> datasTemp = new ArrayList<>();
		for (Set<String> keySet : keySetToJsonChooseMap.keySet()) {
			List<JSONObject> chooses = keySetToJsonChooseMap.get(keySet);
			if (chooses.size() > min) {
				datasTemp.addAll(chooses.subList(0, min));				
			}else {
				datasTemp.addAll(chooses);
			}
		}
		return datasTemp;
	}
	
	/**
	 * 从备选数组里随机选取不少于一半的数据
	 * @param datas 备选数组
	 * @return List<JSONObject> 选取的数据
	 */
	@SuppressWarnings("unchecked")
	public static Map[] getDatasByRandom(Map datas){
		Map datasTemp = new HashMap();
		int half = datas.size() / 2;
		int how = (int)(Math.random() * (half - 1)) + half;//避免了0和越界
		Set<Integer> indexSet = new HashSet<Integer>();
		int num = 0;
		List<Object> keys = new ArrayList<Object>(datas.keySet());
		while (num < how) {
			int index = (int)(Math.random() * (datas.size() - 1));//避免越界
			if (indexSet.contains(index)) {
				continue;
			}
			Object key = keys.get(index);
			Object obj = datas.get(key);
			indexSet.add(index);
			datasTemp.put(key, obj);
			num++;
		}
		//得到另一半
		Map others = new HashMap();
		for (int i = 0; i < keys.size(); i++) {
			if (!indexSet.contains(i)) {
				Object key = keys.get(i);
				others.put(key, datas.get(key));
			}
		}
		Map[] ds = new HashMap[2];
		ds[0] = datasTemp;
		ds[1] = others;
		return ds;
	}
	
	/**
	 * 获取Map数组是否有相同的键
	 * @param datas
	 * @return Map or null
	 */
	public static Map getTheMax(Map[] datas) {
		//getDatasByRandom方法选取的数据覆盖率>0.5，所以选>=70%的
		int num = 0;
		for (int k = 0; k < datas.length; k++) {
			Map<Object, Object> total = datas[k];
			if (k > 0) {
				if (total.size() == 0 || num != total.size()) {
					num = -1;
					break;
				}							
			}else {
				num = total.size();
			}
		}
		if (num > 0) {//数量一致
			return datas[0];
		}else {
			return null;			
		}
	}
	
	/**
	 * 获取列表数组的交集
	 * @param datas
	 * @return 交集
	 */
	public static List<JSONObject> getTheIntersection(List<JSONObject>[] datas) {
		Map<JSONObject, Integer> total = new HashMap<>();
		for (int k = 0; k < datas.length; k++) {
			List<JSONObject> list = datas[k];
			for (JSONObject data : list) {
				total.put(data, total.getOrDefault(data, 0) + 1);
			}
		}
		List<JSONObject> list = new ArrayList<>();
		for (JSONObject data : total.keySet()) {
			if (total.get(data) == datas.length) {
				list.add(data);
			}
		}
		return list;
	}
	
	/**
	 * 获取一堆数据里的共有数据
	 * @param datas 数据数组
	 * @return List<Object>
	 */
	public static Collection<Object> getTheSameItems(Collection[] datas) {
		Collection<Object> list = datas[0];
		for (int i = 1; i < datas.length; i++) {
			list.retainAll(datas[i]);
		}
		return list;
	}
	
	/**
	 * 取两个矩阵列表的交集，并保持矩阵形式
	 * @param lists1
	 * @param lists2	 
	 */
	public static void retainAll(List<List<Object>> lists1, List<List<Object>> lists2) {
		for (List<Object> list : lists1) {
			for (Object json : list) {
				boolean find = false;
				for (List<Object> list2 : lists2) {
					if (list2.contains(json)) {//有相同的部分
						list.retainAll(list2);
						find = true;
						break;
					}
				}
				if (find) {
					break;
				}
			}
		}
	}
	
	/**
	 * 清理打不动要求的
	 * @param totalMap 统计map
	 * @param min 最低要求
	 */
	public static void clearTotalMap(Map<JSONObject, Integer> totalMap, int min) {
		Iterator<JSONObject> iterator = totalMap.keySet().iterator();
		while (iterator.hasNext()) {
			JSONObject json = (JSONObject) iterator.next();
			if (totalMap.get(json) < min) {
				iterator.remove();
			}
		}
	}
}
