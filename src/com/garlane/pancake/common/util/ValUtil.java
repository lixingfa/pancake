package com.garlane.pancake.common.util;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.garlane.pancake.common.base.AnalyseException;
import com.garlane.pancake.common.constant.BusinessConstant;
import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.garlane.pancake.common.constant.Constant;

/**
 * 处理json值的工具类
 * @author lixingfa
 *
 */
public class ValUtil {
	private static final List<String> dateTypeList = Arrays.asList(StringUtil.TIME_STRING, StringUtil.TIME_STRING4
			, StringUtil.TIME_STRING5
			, StringUtil.TIME_STRING6, StringUtil.TIME_STRING7, StringUtil.TIME_STRING8);
	
	/**
	 * 给json赋值
	 * @param json
	 * @param key
	 * @param value
	 * @param keyDataType 键对应的值类型
	 * @return boolean 是否补充了key的数据类型
	 */
	public static boolean getTrueDataTypeValueForJson(JSONObject json, String key, String value, Map<String, String> keyDataType) throws AnalyseException {
		String dataType = null;
		if (keyDataType.containsKey(key)) {
			dataType = keyDataType.get(key);
			String oldDataType = null;
			if (json.get(key) != null) {
				oldDataType = json.get(key).getClass().getTypeName();
				if (oldDataType.equals(BusinessConstant.classTypeEnum.JSONARRAY)) {
					return false;//数组一般表示范围，会出现在年龄、薪酬等，跟其他数据的具体值不一样
				}
			}
			if (dataType.equals(BusinessConstant.classTypeEnum.BIGDECIMAL)) {
				if (StringUtils.isBlank(value)) {
					json.put(key, 0);
				}else {
					try {
						json.put(key, new BigDecimal(value));						
					} catch (Exception e) {
						throw new AnalyseException(value + " 转换成整数时出错，请确保列数据格式一致。");
					}
				}
			}else {
				//日期时间类型
				try {					
					SimpleDateFormat ft = new SimpleDateFormat (dataType);
					getDate(ft, key, value, json);
				} catch (ParseException e) {
					throw new AnalyseException(value + " 赋值时错误。请确保日期、时间的格式一致。低精度时间不能出现在高精度之后。年月只支持XXXX年XX月格式。");
				}				
			}
			return false;
		}else {
			if (value == null || "null".equals(value) || StringUtils.isBlank(value)) {
				throw new AnalyseException(json.toJSONString() + key + "值为空，无法匹配类型。");
			}
			//日期时间类型
			try {
				SimpleDateFormat ft = null;
				//不能规定日期格式，低精度的格式可以兼容高精度的值，比如年月可以兼容年月日，但会丢失精度。反之报错。
				if (StringUtil.isMatchRex(value, StringUtil.TIME_REX)) {//年月
					ft = new SimpleDateFormat (StringUtil.TIME_STRING4);
					dataType = StringUtil.TIME_STRING4;
				}else if (StringUtil.isMatchRex(value, StringUtil.TIME_REX1)) {//年月日
					ft = new SimpleDateFormat (StringUtil.TIME_STRING5);
					dataType = StringUtil.TIME_STRING5;
				}else if (StringUtil.isMatchRex(value, StringUtil.TIME_REX2)) {//年月日 时
					ft = new SimpleDateFormat (StringUtil.TIME_STRING6);
					dataType = StringUtil.TIME_STRING6;
				}else if (StringUtil.isMatchRex(value, StringUtil.TIME_REX3)) {//年月日 时分
					ft = new SimpleDateFormat (StringUtil.TIME_STRING7);
					dataType = StringUtil.TIME_STRING7;
				}else if (StringUtil.isMatchRex(value, StringUtil.TIME_REX4)) {//年月日 时分秒
					ft = new SimpleDateFormat (StringUtil.TIME_STRING);
					dataType = StringUtil.TIME_STRING;
				}else if (StringUtil.isMatchRex(value, StringUtil.TIME_REX4)) {//时分秒
					ft = new SimpleDateFormat (StringUtil.TIME_REX5);
					dataType = StringUtil.TIME_STRING8;
				}
				if (ft != null) {
					getDate(ft, key, value, json);
				}
			} catch (ParseException e) {
				throw new AnalyseException(value + "转换成日期时错误。");
			}
			if (dataType == null) {//没找到日期
				//数字类型				
				if (StringUtil.isMatchRex(value, NumberUtil.NUM_REX)) {//整数
					json.put(key, new BigDecimal(value));
					dataType = json.get(key).getClass().getTypeName();
				}
			}
			if (dataType != null) {
				keyDataType.put(key, dataType);
				return true;
			}			
		}
		//取值范围，因为不确定子元素是啥，所以不存值类型
		String[] range;
		JSONArray array = null;
		if (StringUtil.isMatchRex(value, NumberUtil.NUM_RANGE_REX1)) {//取值范围1
			range = value.split(Constant.SHORT_BAR);
			array = new JSONArray();
		}else if (StringUtil.isMatchRex(value, NumberUtil.NUM_RANGE_REX2)) {//取值范围2
			range = value.split("至");
			array = new JSONArray();
		}else {
			range = null;
		}
		if (array != null) {
			range[0] = range[0].trim();
			range[1] = range[1].trim();
			if (StringUtil.isMatchRex(range[0], NumberUtil.NUM_ZHENG_REX)) {//整数
				array.add(Integer.parseInt(range[0]));
			}else if (StringUtil.isMatchRex(range[0], NumberUtil.NUM_XIAO_REX)) {//小数
				array.add(Float.parseFloat(range[0]));
			}
			if (StringUtil.isMatchRex(range[1], NumberUtil.NUM_ZHENG_REX)) {//整数
				array.add(Integer.parseInt(range[1]));
			}else if (StringUtil.isMatchRex(range[1], NumberUtil.NUM_XIAO_REX)) {//小数
				array.add(Float.parseFloat(range[1]));
			}
			json.put(key, array);
		}
		//非空，字符串兜底
		else {
			json.put(key, value);
			dataType = json.get(key).getClass().getTypeName();
			keyDataType.put(key, dataType);
		}
		return false;
	}
	
	/**
	 * 判断数据格式是否属于日期时间类型
	 * 因为日期时间类型分为多种，有些场景需要细化
	 * @param dataType 数据格式
	 * @return
	 */
	public static boolean isDateType(String dataType) {		
		if (dateTypeList.contains(dataType)) {//年月
			return true;
		}
		return false;
	}
	
	/**
	 * 获得日期
	 * @param ft 时间
	 * @param key
	 * @param value
	 * @param json
	 * @return
	 * @throws ParseException
	 */
	public static Date getDate(SimpleDateFormat ft, String key, String value, JSONObject json) throws ParseException {
		String v = value.replaceAll("[年月\\\\/.]", Constant.SHORT_BAR).replaceAll("\n", Constant.SPACE);//年月肯定有，都替换成短杆
		if (v.endsWith("日")) {
			v = v.replaceAll("日", Constant.EMPTY);
		}
		if (v.endsWith("时")) {
			v = v.replaceAll("日", Constant.EMPTY).replaceAll("时", Constant.EMPTY);
		}
		if (v.endsWith("分")) {
			v = v.replaceAll("日", Constant.EMPTY).replaceAll("时", Constant.COLON_EN).replaceAll("分", Constant.EMPTY);
		}
		if (v.endsWith("秒")) {
			v = v.replaceAll("日", Constant.EMPTY).replaceAll("[时分]", Constant.COLON_EN).replaceAll("秒", Constant.EMPTY);
		}
		Date date = ft.parse(v);
		json.put(key, date);

		return date;
	}
}
