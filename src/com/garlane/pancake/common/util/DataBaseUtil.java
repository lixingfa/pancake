package com.garlane.pancake.common.util;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.garlane.pancake.common.base.AnalyseException;
import com.garlane.pancake.common.constant.BusinessConstant;
import com.garlane.pancake.common.constant.Constant;
import com.garlane.pancake.common.constant.DataBaseConstant;

/**
 * 数据库工具
 * @author lixingfa
 *
 */
public class DataBaseUtil {
	private static final String KEY_NAME = "[a-z0-9]+[A-Z]+[a-zA-Z0-9]*";
	private static final String UP = "[A-Z]{1}";
	private static final String UUID = "uuid";//oracle主键使用
	private static final String PRIKEY = "prikey";//MYSQL主键使用
	private static final String MYSQL = "MYSQL";
	private static final String CREATETIME = "createTime";
	private static final String CREATEDATE = "createDate";
	private static final String CREATE_TIME = "create_time";
	private static final String UPDATE_TIME = "update_time";
	/**查询语句，构造JSON*/
	public static final String SELECT = "select";
	/**建表语句*/
	public static final String CREATE = "create";
	/**插入语句*/
	public static final String INSERT = "insert";
	/**用来替代insert*/
	public static final String REPLACE = "replace";
	/**最大长度*/
	public static final String MAXLENGTH = "maxLength";
	/**不可以为空*/
	public static final String NOTNULL = "notNull";
	/**值为无符号类型，即正数*/
	public static final String UNSIGNED = "unSigned";
	/**升序*/
	public static final String ORDER = "order";
	/**升序*/
	public static final String ASC = "asc";
	/**降序*/
	public static final String DESC = "desc";
	/**取值范围*/
	public static final String RANGES = "ranges";
	/**取值范围*/
	public static final String REX = "rex";
	
	/**
	 * 从json中拆分数据实体
	 * @param pageJsonArraysMap 页面的json数组Map
	 * @param jsonArraysOwnKeyMap json数组的注释，位于数组上面，说明哪些驼峰key是属于主体的，可以放一个空的map
	 * @param dataBaseType 数据库类型
	 * @return Map<String, Object> sqlMap
	 */
	public static Map<String, Object> getDateEntityFromJson(Map<String
			, Map<String, JSONArray>> pageJsonArraysMap
			, Map<String, List<String>> jsonArraysOwnKeyMap
			, String dataBaseType) {
		/*
		 * page：页面
		 * mapKey：传递给页面的json数组/对象的名字
		 * key：传递给页面的json数组/对象的key
		 * mainTable：mapKey对象的主表
		 * table：jsonkey开头的小写部分，原本属于的表
		 */		
		//1、从所有josn的key中抽取表名
		Map<String, Integer> tableNameTotal = new HashMap<>();//表名统计map
		//字段与表名的映射map
		Map<String, String> keyAndTableNameMap = new HashMap<>();
		//去不去s的快速寻找
		Map<String, String> mapKeyAndMainTableMap = new HashMap<>();
		//把JsonArray缩减成列表，方便查找每个key的情况，page-mapKey-key-值，因为每个页面的排序可能不一样
		Map<String, Map<String, Map<String, List<Object>>>> pageKeyListMap = new HashMap<>();
		//json各个key对应的值类型
		Map<String, Map<String, Map<String, String>>> pageKeyDataTypeMap = new HashMap<>();
		for (String page : pageJsonArraysMap.keySet()) {
			Map<String, JSONArray> jsonArraysMap = pageJsonArraysMap.get(page);
			Map<String, Map<String, List<Object>>> keyListMap = new HashMap<>();
			Map<String, Map<String, String>> keyDataTypeMap = new HashMap<>();
			for(String mapKey : jsonArraysMap.keySet()){
				JSONArray jsonArray = jsonArraysMap.get(mapKey);
				keyListMap.put(mapKey, new HashMap<>());
				String mainTable;
				if (mapKey.endsWith("s")) {
					mainTable = mapKey.substring(0, mapKey.length() - 1);
				}else {
					mainTable = mapKey;
				}
				mapKeyAndMainTableMap.put(mapKey, mainTable);
				//合并原始数据，避免重复
				jsonArray = JSONUtil.mergeJsonBySameKV(jsonArray);
				jsonArraysMap.put(mapKey, jsonArray);
				//拿一个出来分析
				Map<String, String> keyDataType = new HashMap<>();
				List<String> ownKeyList = jsonArraysOwnKeyMap.getOrDefault(mapKey, new ArrayList<>());
				//每个表固定有的
				ownKeyList.add(CREATEDATE);
				ownKeyList.add(CREATETIME);
				if (jsonArray.size() > 0) {
					JSONObject json = jsonArray.getJSONObject(0);
					for(String key : json.keySet()){
						keyListMap.get(mapKey).put(key, new ArrayList<>());
						//替换值的类型
						ValUtil.getTrueDataTypeValueForJson(json, key, json.getString(key), keyDataType);
						//在前面转换过类型了
						String dataType = keyDataType.get(key);
						try {
							if (BusinessConstant.classTypeEnum.BIGDECIMAL.equals(dataType)) {
								keyListMap.get(mapKey).get(key).add(json.getBigDecimal(key));						
							}else if (ValUtil.isDateType(dataType)) {
								keyListMap.get(mapKey).get(key).add(json.getDate(key));
							}else {
								keyListMap.get(mapKey).get(key).add(json.getString(key));
							}
						} catch (Exception e) {
							throw new AnalyseException(key + "转换成"+ dataType +"时出错，请检查该列表的第一个值是否存在类型歧义。"
									+ json.toJSONString());
						}
						if (!keyAndTableNameMap.containsKey(key)) {
							if(key.matches(KEY_NAME) && !ownKeyList.contains(key)){
								//有驼峰，且不属于自身的那些key
								//包含全小写部分
								String[] subs = key.split(UP);
								String tableName = subs[0];
								tableNameTotal.put(tableName, tableNameTotal.getOrDefault(tableName, 0));
								keyAndTableNameMap.put(key, tableName);
							}else {
								//不属于驼峰，直接放入主表
								keyAndTableNameMap.put(key, mainTable);
							}						
						}
					}					
				}
				keyDataTypeMap.put(mapKey, keyDataType);
			}
			pageKeyListMap.put(page, keyListMap);
			pageKeyDataTypeMap.put(page, keyDataTypeMap);
		}
		//初始化新表数据，只认可出现多次的新表
		Map<String, JSONArray> dataList = new HashMap<>();
		for (String tableName : tableNameTotal.keySet()){
			if (tableNameTotal.get(tableName) == 1){
				continue;
			}
			dataList.put(tableName, new JSONArray());//表新增的数据
		}
		//2、从原始数据中抽取出表json，并记录新旧key的映射
		//数据mapKey-表名-表字段-原来的key
		Map<String, Map<String, Map<String, Map<String, String>>>> pageDataTableKeyAndOldKeyMap = new HashMap<>();
		//table - key - mapKey对应的表
		Map<String, Map<String, String>> tableKeyMapKeyTableMap = new HashMap<>();
		//排序和正则表达式，mapKey-jsonKey-order
		Map<String, LinkedHashMap<String, String>> orderMap = new HashMap<>();
		//ascii表的差值
		int cha ='z'-'Z';
		//jsonKey对应的table，即最开始全小写的那段
		Map<String, String> jsonkeyToTableNameMap = new HashMap<>();
		//去掉全小写那段后key首字母变成小写的映射
		Map<String, String> jsonkeyToTableKeyMap = new HashMap<>();
		for (String page : pageJsonArraysMap.keySet()) {
			Map<String, JSONArray> jsonArraysMap = pageJsonArraysMap.get(page);
			Map<String, Map<String, Map<String, String>>> dataTableKeyAndOldKeyMap = new HashMap<>();
			Map<String, Map<String, List<Object>>> keyListMap = pageKeyListMap.get(page);
			Map<String, Map<String, String>> keyDataTypeMap = pageKeyDataTypeMap.get(page);
			for(String mapKey : jsonArraysMap.keySet()){
				//jsonArray.size() == 0时没有值
				List<String> ownKeyList = jsonArraysOwnKeyMap.getOrDefault(mapKey, new ArrayList<>());
				//新拆出来的表-key对应的旧key
				Map<String, Map<String, String>> tableKeyAndOldKeyMap = new HashMap<>();
				String mapKeyTable = mapKeyAndMainTableMap.get(mapKey);			
				tableKeyAndOldKeyMap.put(mapKeyTable, new HashMap<>());//加入本身
				dataList.put(mapKeyTable, new JSONArray());
				Map<String, String> keyDataType = keyDataTypeMap.get(mapKey);
				//对每个json进行抽取
				JSONArray jsonArray = jsonArraysMap.get(mapKey);
				for (int i = 0; i < jsonArray.size(); i++){
					JSONObject json = jsonArray.getJSONObject(i);
					//用于记录当前行是否加了对应表的json，每行只能产生一个json
					Set<String> addJsonSet = new HashSet<>();
					for(String key : json.keySet()){
						String tableName = keyAndTableNameMap.get(key);
						JSONArray tableDateList = dataList.get(tableName);
						if (tableDateList == null) {
							continue;
						}
						if (!tableKeyAndOldKeyMap.containsKey(tableName)) {						
							tableKeyAndOldKeyMap.put(tableName, new HashMap<>());//加入对应的表
						}
						if (!addJsonSet.contains(tableName)){
							tableDateList.add(new JSONObject());
							addJsonSet.add(tableName);
						}
						if (!tableKeyMapKeyTableMap.containsKey(tableName)) {
							tableKeyMapKeyTableMap.put(tableName, new HashMap<>());
						}
						//拿最新的那个
						JSONObject tableDateJson = tableDateList.getJSONObject(tableDateList.size() - 1);
						//有驼峰并且不是Time或 Date结尾
						if(key.matches(KEY_NAME) && !ownKeyList.contains(key)){
							String tableKey = jsonkeyToTableNameMap.get(key);
							if (tableKey == null){
								//去掉表名，替换首字母变小写，得到表的字段名
								tableKey = key.replace(tableName, Constant.EMPTY);
								if (jsonkeyToTableKeyMap.containsKey(tableKey)) {
									tableKey = jsonkeyToTableKeyMap.get(tableKey);
								}else {
									char[] cs = tableKey.toCharArray();
									//不要求线程安全，速度更快
									StringBuilder sBuilder = new StringBuilder();
									boolean lastNotUp = true;
									for (char c : cs){
										if (c <= 'Z'){
											if (lastNotUp) {
												c = (char)(c + cha);
												sBuilder.append("_");										
											}
											lastNotUp = false;										
										}else {
											lastNotUp = true;
										}
										sBuilder.append(c);
									}
									//去掉最前面的短杠
									jsonkeyToTableKeyMap.put(tableKey, sBuilder.substring(1));
									tableKey = sBuilder.substring(1);
								}
								jsonkeyToTableNameMap.put(key, tableKey);
								tableKeyMapKeyTableMap.get(tableName).put(tableKey, mapKeyTable);
							}
							tableKeyAndOldKeyMap.get(tableName).put(tableKey, key);
							tableDateJson.put(tableKey, json.get(key));
						}else {
							tableKeyAndOldKeyMap.get(tableName).put(key, key);
							tableDateJson.put(key, json.get(key));
						}
						if (i > 0) {
							//i == 0的值在寻找类型时就确定了，无需再次处理
							String dataTypeStr = keyDataType.get(key);
							if (ValUtil.isDateType(dataTypeStr)) {
								try {
									keyListMap.get(mapKey).get(key).add(ValUtil.getDate(new SimpleDateFormat (dataTypeStr), key, json.getString(key), json));
								} catch (Exception e) {
									throw new AnalyseException(key + "转换成" + dataTypeStr + "时出错，请检查该列表的第一个值是否存在类型歧义。"
											+ json.toJSONString());
								}
							}else {
								BusinessConstant.classTypeEnum dataType = BusinessConstant.getClassType(dataTypeStr);
								if (BusinessConstant.classTypeEnum.BIGDECIMAL.equals(dataType)) {
									keyListMap.get(mapKey).get(key).add(json.getBigDecimal(key));
								} else if (BusinessConstant.classTypeEnum.STRING.equals(dataType)) {
									keyListMap.get(mapKey).get(key).add(json.getString(key));
								} else {
									keyListMap.get(mapKey).get(key).add(json.getString(key));
								}
							}
						}
					}
				}
				dataTableKeyAndOldKeyMap.put(mapKey, tableKeyAndOldKeyMap);
				//升降序、全字段/区间/无，某个字段按区间来，就看区间对应哪个字段是一样的值
				if (!keyListMap.containsKey(mapKey)) {
					continue;
				}			
				Map<String, List<Object>> keyValList = keyListMap.get(mapKey);
				orderMap.put(mapKey, OrderUtil.getKeyOrder(keyValList));
			}
			pageDataTableKeyAndOldKeyMap.put(page, dataTableKeyAndOldKeyMap);
		}
		//3、合并抽离出来的json
		for (String tableName : dataList.keySet()){
			//内部合并
			JSONArray tableDateList = JSONUtil.mergeJsonBySameKV(dataList.get(tableName));
			dataList.put(tableName, tableDateList);
		}		
		//4、寻找能确定唯一记录的key集合，使用list是为多条件联合做准备
		Map<String, List<String>> tableOnlyDataKeyMap = new HashMap<>();
		for (String tableName : dataList.keySet()){
			JSONArray jsonArray = dataList.get(tableName);
			List<String> onlyDataKeys = new ArrayList<>();
			if (jsonArray.size() > 0) {
				JSONObject json = jsonArray.getJSONObject(0);
				if (jsonArray.size() == 1) {
					onlyDataKeys.addAll(json.keySet());
				}else{
					//新json的唯一值
					for (String key : json.keySet()){
						Set<Object> valSet = new HashSet<>();
						valSet.add(json.get(key));
						for(int i = 1; i < jsonArray.size(); i++){
							valSet.add(jsonArray.getJSONObject(i).get(key));						
						}
						if (valSet.size() == jsonArray.size()){
							onlyDataKeys.add(key);
						}
					}
					//不能用一个key来确定，就需要寻找多个的方案，最典型的就是地名，层次越多就越准确
					if (onlyDataKeys.size() == 0) {
						//但多个会增加复杂度，可以直接加入table的wwid
						if (MYSQL.equals(dataBaseType)) {
							onlyDataKeys.add(PRIKEY);
						}else {
							onlyDataKeys.add(UUID);
						}
					}
				}				
			}
			tableOnlyDataKeyMap.put(tableName, onlyDataKeys);
		}
		//5、生成查询SQL
		Map<String, Map<String, String>> selectSqlMap = new HashMap<>();
		for (String page : pageDataTableKeyAndOldKeyMap.keySet()) {
			Map<String, Map<String, Map<String, String>>> dataTableKeyAndOldKeyMap = pageDataTableKeyAndOldKeyMap.get(page);
			Map<String, String> sqlMap = new HashMap<>();
			for (String dataName : dataTableKeyAndOldKeyMap.keySet()){	
				//该数据对应的表及字段
				Map<String, Map<String, String>> tableKeyOldKeyMap = dataTableKeyAndOldKeyMap.get(dataName);
				//组装和补全本表
				StringBuilder selectBuilder = new StringBuilder();
				StringBuilder tableBuilder = new StringBuilder();
				StringBuilder whereBuilder = new StringBuilder();
				for(String table : tableKeyOldKeyMap.keySet()){
					//table				
					tableBuilder.append(Constant.COMMA_EN).append(table);
					//select
					Map<String, String> keyOldKeyMap = tableKeyOldKeyMap.get(table);
					//where
					List<String> singleValKeys = tableOnlyDataKeyMap.get(table);
					List<String> whereKeys = new ArrayList<>();
					for (String key : keyOldKeyMap.keySet()){
						//查询的key里可以作为关联条件
						if(singleValKeys.contains(key)){
							whereKeys.add(key);
						}
						//需要查询的字段
						selectBuilder.append(Constant.COMMA_EN).append(table).append(Constant.POINT_EN).append(key);
						if (!key.equals(keyOldKeyMap.get(key))) {
							selectBuilder.append(" as ").append(keyOldKeyMap.get(key));
						}
					}
					if (tableKeyMapKeyTableMap.containsKey(table)) {
						if(whereKeys.size() > 0) {
							//组装
							for (String key : whereKeys){
								String baseTable = tableKeyMapKeyTableMap.get(table).get(key);
								if (baseTable == null) {
									continue;
								}
								whereBuilder.append(" and ").append(table).append(Constant.POINT_EN).append(key)
								.append(" = ").append(baseTable).append(Constant.POINT_EN).append(keyOldKeyMap.get(key));
							}
						}
//						else {
//						String baseTable = tableKeyDataMap.get(table).get(Constant.EMPTY);
							//除唯一值key外，最少的key可以确定唯—一条记录
//					whereBuilder.append(" and ").append(table).append(Constant.POINT_EN).append(UUID)
//					.append("=").append(baseTable).append(Constant.POINT_EN).append(table).append("Uuid");					
//						}
					}
				}
				if (selectBuilder.length() == 0) {
					continue;
				}
				String select = "\n select " + selectBuilder.substring(1)
				+ "\n from "+ tableBuilder.substring(1)
				+ "\n where 1 = 1" + whereBuilder;
				//排序
				LinkedHashMap<String, String> order = orderMap.get(dataName);
				if (order.size() > 0) {
					StringBuilder orderBuilder = new StringBuilder();
					for (String orderKey : order.keySet()) {
						orderBuilder.append(Constant.COMMA_EN).append(Constant.SPACE).append(orderKey).append(Constant.SPACE)
						.append(order.get(orderKey));
					}
					select = select + "\n order by" + orderBuilder.substring(1);
				}
				sqlMap.put(dataName, select);
			}
			selectSqlMap.put(page, sqlMap);
		}
		//6、生成其他insert语句，加上uuid，考虑更新的情况
		Map<String, Object> sqlMap = new HashMap<>();
		sqlMap.put(SELECT, selectSqlMap);
		for (String tableName : dataList.keySet()){
			StringBuilder insertBuilder = new StringBuilder();
			JSONArray jsonArray = dataList.get(tableName);
			if (jsonArray.size() == 0) {
				continue;
			}
			//插入语句
			Map<String, BigDecimal> maxValueMap = new HashMap<>();
			Map<String, Integer> maxLengthMap = new HashMap<>();
			Map<String, Boolean> notNullMap = new HashMap<>();
			//重复的区间
			Map<String, Boolean> unSignedMap = new HashMap<>();
			Map<String, Boolean> sameLengthMap = new HashMap<>();
			insertBuilder.append("insert into ").append(tableName).append(Constant.PHESE_LEFT);//.append(UUID);
			//用最多属性的做标杆
			JSONObject maxSizeJson = jsonArray.getJSONObject(0);
			for (int i = 1; i < jsonArray.size();i++){
				JSONObject json = jsonArray.getJSONObject(i);
				if (json.size() > maxSizeJson.size()) {
					maxSizeJson = json;
				}
			}
			//初始化字段和变量
			StringBuilder keyBuilder = new StringBuilder();
			for (String key : maxSizeJson.keySet()){
				keyBuilder.append(Constant.COMMA_EN).append(key);
				maxValueMap.put(key, new BigDecimal(0));
				maxLengthMap.put(key, 0);
				notNullMap.put(key, true);
				unSignedMap.put(key, true);
				sameLengthMap.put(key, true);
			}
			keyBuilder.append(Constant.PHESE_RIGHT);
			//遍历
			StringBuilder valBuilders = new StringBuilder();
			for (int i= 0; i < jsonArray.size(); i++){
				JSONObject json = jsonArray.getJSONObject(i);
				StringBuilder valBuilder = new StringBuilder();//.append(Constant.QUOT).append(StringUtil.getUUID()).append(Constant.QUOT);
				for (String key : maxSizeJson.keySet()){
					//也可以用ValUtil.getTrueDataTypeValueForJson()重新赋值
					String value = String.valueOf(json.get(key)).trim();
					if (value.equals("null") || value.length() == 0) {
						value = Constant.EMPTY;//空字符串
						notNullMap.put(key, false);
					}
					if (value.length() > maxLengthMap.get(key)) {
						if (maxLengthMap.get(key) != 0) {
							sameLengthMap.put(key, false);
						}
						maxLengthMap.put(key, value.length());
					}
					if (value.indexOf(Constant.SHORT_BAR) == 0) {
						unSignedMap.put(key, false);
					}
					//整数或小数
					if (StringUtil.isMatchRex(value, NumberUtil.NUM_ZHENG_REX) 
							|| StringUtil.isMatchRex(value, NumberUtil.NUM_XIAO_REX)){
						valBuilder.append(Constant.COMMA_EN).append(json.get(key));
						BigDecimal val = json.getBigDecimal(key);
						if (val.compareTo(maxValueMap.get(key)) == 1) {//1大于 0等于
							maxValueMap.put(key, val);
						}
					}else {
						//其他的都要加引号
						valBuilder.append(Constant.COMMA_EN).append(Constant.QUOT).append(value).append(Constant.QUOT);
					}
				}
				valBuilders.append(Constant.PHESE_LEFT).append(valBuilder.substring(1, valBuilder.length()))
				.append(Constant.PHESE_RIGHT).append(Constant.COMMA_EN);
			}
			//外带的属性
//			Map<String, Object> tablePropertyMap = new HashMap<>();
//			tablePropertyMap.put(MAXLENGTH, maxLengthMap);
//			tablePropertyMap.put(NOTNULL, notNullMap);
//			tablePropertyMap.put(UNSIGNED, unSignedMap);
//			sqlMap.put(tableName, tablePropertyMap);
			Map<String, String> createAndInsertMap = new HashMap<>();
			//加上一个换行格式更好 
			insertBuilder.append(keyBuilder.substring(1, keyBuilder.length())).append(" values")
			.append(valBuilders.substring(0, valBuilders.length() - 1)).append(Constant.SEMICOLON_EN)
			.append(Constant.LINE_FEED);
			//首次用初始化数据
			createAndInsertMap.put(INSERT, insertBuilder.toString());
			//更新用
//			sqlMap.get(tableName).put(REPLACE, null);
			//建表语句
			createAndInsertMap.put(CREATE, getTable(tableName, maxSizeJson
					, maxValueMap, maxLengthMap, notNullMap, unSignedMap, sameLengthMap
					, dataBaseType));
			
			sqlMap.put(tableName, createAndInsertMap);			
		}
		return sqlMap;
	}
	
	/**
	 * 获取建表语句
	 * @param tableName 表名
	 * @param json 样例Json
	 * @param maxValueMap 最大值
	 * @param maxLengthMap 最大长度
	 * @param notNullMap 允许为空
	 * @param unSignedMap 无符号
	 * @param sameLengthMap 等长
	 * @return String 建表语句
	 */
	public static String getTable(String tableName, JSONObject json
			, Map<String, BigDecimal> maxValueMap, Map<String, Integer> maxLengthMap
			,Map<String, Boolean> notNullMap ,Map<String, Boolean> unSignedMap, Map<String, Boolean> sameLengthMap
			,String dataBaseType) {
		String table;
		if (MYSQL.equals(dataBaseType))
			table = getTableMysql(tableName, json, maxValueMap, maxLengthMap, notNullMap, unSignedMap, sameLengthMap);
		else {
			table = getTableMysql(tableName, json, maxValueMap, maxLengthMap, notNullMap, unSignedMap, sameLengthMap);
		}
		return table;
	}
	
	/**
	 * 获取建表语句
	 * @param tableName 表名
	 * @param json 样例Json
	 * @param maxValueMap 最大值
	 * @param maxLengthMap 最大长度
	 * @param notNullMap 允许为空
	 * @param unSignedMap 无符号
	 * @param sameLengthMap 等长
	 * @return String 建表语句
	 */
	private static String getTableMysql(String tableName, JSONObject json
			, Map<String, BigDecimal> maxValueMap, Map<String, Integer> maxLengthMap
			,Map<String, Boolean> notNullMap ,Map<String, Boolean> unSignedMap, Map<String, Boolean> sameLengthMap) {
		StringBuilder createBuilder = new StringBuilder();
		createBuilder.append("create table ").append(Constant.POINT_QUOT).append(tableName).append(Constant.POINT_QUOT)
		.append(Constant.PHESE_LEFT).append(Constant.LINE_FEED);
		//MYSQL表的主键
		createBuilder.append(PRIKEY).append(" BIGINT(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,").append(Constant.LINE_FEED);
		//字段			
		for (String key : json.keySet()){
			if (key.equals(CREATE_TIME) || key.equals(UPDATE_TIME)) {
				//每个表固定会有
				continue;
			}
			Map<String, String> keyDataType = new HashMap<>();
			ValUtil.getTrueDataTypeValueForJson(json, key, json.getString(key), keyDataType);
			String dataTypeStr = keyDataType.get(key);
			String column = getColumn(key, maxValueMap.get(key), dataTypeStr, BusinessConstant.getClassType(dataTypeStr), maxLengthMap.get(key)
					, notNullMap.get(key), unSignedMap.get(key), sameLengthMap.get(key), MYSQL);
			createBuilder.append(column).append(Constant.LINE_FEED);
		}
		//增加创建字段和修改时间字段
		createBuilder.append("CREATE_TIME DATETIME NOT NULL DEFAULT NOW(),").append(Constant.LINE_FEED);
		createBuilder.append("UPDATE_TIME DATETIME NOT NULL DEFAULT NOW()").append(Constant.LINE_FEED);
		//表结尾
		createBuilder.append(Constant.PHESE_RIGHT).append(Constant.SEMICOLON_EN).append(Constant.LINE_FEED);		
		return createBuilder.toString();
	}
	
	/**
	 * 获取字段的创建语句，仿照ValUtil的方法
	 * @param key 字段
	 * @param maxValue 最大值
	 * @param dataTypeStr 数据的原形式
	 * @param dataType 值的类型
	 * @param maxLength 最大长度
	 * @param notNull 数据库类型
	 * @param unSigned 无符号值，即正数
	 * @param sameLength 长度一致，用于字符类型的判断
	 * @param dataBaseType 数据库类型
	 * @return 获取字段的创建语句
	 */
	public static String getColumn(String key, BigDecimal maxValue
			, String dataTypeStr
			, BusinessConstant.classTypeEnum dataType
			, int maxLength, boolean notNull
			, boolean unSigned, boolean sameLength,String dataBaseType) {
		String column;
		switch (dataBaseType) {
			case MYSQL:
				column = getColumnMysql(key, maxValue, dataTypeStr, dataType, maxLength, notNull, unSigned, sameLength);
				break;
			default:
				column = getColumnMysql(key, maxValue, dataTypeStr, dataType, maxLength, notNull, unSigned, sameLength);
				break;
		}
		return column;
	}
	
	/**
	 * 获取字段的创建语句，仿照ValUtil的方法
	 * @param key 字段
	 * @param maxValue 最大值
	 * @param dataTypeStr 数据的原形式
	 * @param dataType 值的类型
	 * @param maxLength 最大长度
	 * @param notNull 数据库类型
	 * @param unSigned 无符号值，即正数
	 * @param sameLength 长度一致，用于字符类型的判断
	 * @return 获取字段的创建语句
	 */
	private static String getColumnMysql(String key, BigDecimal maxValue
			, String dataTypeStr
			, BusinessConstant.classTypeEnum dataType
			, int maxLength
			, boolean notNull, boolean unSigned, boolean sameLength) {
		int scale = maxValue.scale();
		StringBuilder Builder = new StringBuilder();
		Builder.append(Constant.POINT_QUOT).append(key).append(Constant.POINT_QUOT);
		//使用自动递增的bigint做主键
		//整数
		if (dataType.equals(BusinessConstant.classTypeEnum.BIGDECIMAL)) {
			if (scale == 0) {
				long value = maxValue.longValue();
				if (value <= 127) {//(-128，127)
					Builder.append(Constant.SPACE + DataBaseConstant.mysqlDataTypeEnum.TINYINT + Constant.SPACE);
				}else if (value <= 32767) {//(-32 768，32 767)
					Builder.append(Constant.SPACE + DataBaseConstant.mysqlDataTypeEnum.SMALLINT + Constant.SPACE);
				}else if (value <= 8388607) {//(-8 388 608，8 388 607)
					Builder.append(Constant.SPACE + DataBaseConstant.mysqlDataTypeEnum.MEDIUMINT + Constant.SPACE);
				}else if (value <= 2147483647) {//(-2 147 483 648，2 147 483 647)
					Builder.append(Constant.SPACE + DataBaseConstant.mysqlDataTypeEnum.INT + Constant.SPACE);
				}else {//(-9,223,372,036,854,775,808，9 223 372 036 854 775 807)
					Builder.append(Constant.SPACE + DataBaseConstant.mysqlDataTypeEnum.BIGINT + Constant.SPACE);
				}			
				//无符号值，即都是整数，会造成取值范围不同，SQL里用来运算，则不能加这个
				if (unSigned) {
					Builder.append(" UNSIGNED ");				
				}
			}
			//小数，保留两位，避免使用浮点数，问题比较多
			else if (dataType.equals(BusinessConstant.classTypeEnum.BIGDECIMAL)) {
				Builder.append(Constant.SPACE + DataBaseConstant.mysqlDataTypeEnum.DECIMAL
						+ Constant.PHESE_LEFT + maxLength +",2" + Constant.PHESE_RIGHT + Constant.SPACE);
			}
		}
		//日期时间类型，TIMESTAMP是时间戳，插入和更新时会自动赋值，但有取值范围局限性
		else if (dataTypeStr.equals(StringUtil.TIME_STRING5) //年月日
				|| dataTypeStr.equals(StringUtil.TIME_STRING4)) {//年月
			Builder.append(Constant.SPACE + DataBaseConstant.mysqlDataTypeEnum.DATE + Constant.SPACE);
		}else if (StringUtil.TIME_STRING8.equals(dataTypeStr)){ //时分秒
			Builder.append(Constant.SPACE + DataBaseConstant.mysqlDataTypeEnum.TIME + Constant.SPACE);
		}else if (StringUtil.TIME_STRING.equals(dataTypeStr)//年月日 时分秒
				|| StringUtil.TIME_STRING7.equals(dataTypeStr)//年月日 时分
				|| StringUtil.TIME_STRING6.equals(dataTypeStr)) {//年月日 时
			Builder.append(Constant.SPACE + DataBaseConstant.mysqlDataTypeEnum.DATETIME + Constant.SPACE);
		}
		//兜底的字符串类型
		else if (sameLength) {
			//定长，取出时会去掉首尾空格
			if (maxLength < 255) {
				Builder.append(Constant.SPACE + DataBaseConstant.mysqlDataTypeEnum.CHAR
						+ Constant.PHESE_LEFT + maxLength + Constant.PHESE_RIGHT + Constant.SPACE);
			}else if (maxLength < 65530) {
				Builder.append(Constant.SPACE + DataBaseConstant.mysqlDataTypeEnum.VARCHAR
						+ Constant.PHESE_LEFT + maxLength + Constant.PHESE_RIGHT + Constant.SPACE);
			}else {
				//避免使用BLOB
				Builder.append(Constant.SPACE + DataBaseConstant.mysqlDataTypeEnum.TEXT + Constant.SPACE);
			}
		}else {
			if (maxLength < 65530) {
				//变长，取出时不会去掉首尾空格
				Builder.append(Constant.SPACE + DataBaseConstant.mysqlDataTypeEnum.VARCHAR
						+ Constant.PHESE_LEFT + maxLength + Constant.PHESE_RIGHT + Constant.SPACE);
			}else {
				//避免使用BLOB
				Builder.append(Constant.SPACE + DataBaseConstant.mysqlDataTypeEnum.TEXT + Constant.SPACE);
			}
		}
		//不能为空，尽量不为空，否则会导致索引失效和占用额外存储空间，使用默认值代替比较好
		if (notNull) {
			Builder.append(" NOT NULL ");
		}
		Builder.append(",");
		return Builder.toString();
	}
}
