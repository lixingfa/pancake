package com.garlane.pancake.common.util;

import java.util.*;

import com.garlane.pancake.common.constant.BusinessConstant;

public class OrderUtil {
	/**
	 * 获取排序字符串
	 * 与其他key级联的key优先
	 * 都没有级联则同值区间多的优先，因为同值区间少，尤其是都没有的时候，通常表示唯一自增主键
	 * @param keyValListMap key的值列表Map，存在不同长度值的String key会被排除
	 * @return LinkedHashMap<String, String> 用来记录排序字段的map
	 */
	public static LinkedHashMap<String, String> getKeyOrder(Map<String, List<Object>> keyValListMap) {
		//选出日期、数值和长度一致的Stringkey
		Map<String, List<Object>> keyValListMapTemp = new HashMap<>();
		for (String key : keyValListMap.keySet()) {			
			List<Object> keyValList = keyValListMap.get(key);
			if (BusinessConstant.classTypeEnum.STRING.equals(keyValList.get(0).getClass().getTypeName())) {
				//字符串类型
				Set<Integer> stringLengthSet = new HashSet<>();
				for (int i = 0; i < keyValList.size(); i++) {
					stringLengthSet.add(String.valueOf(keyValList.get(i)).length());
				}
				if (stringLengthSet.size() == 1) {
					//长度相同
					keyValListMapTemp.put(key, keyValList);
				}
			}else {
				//非字符串类型
				keyValListMapTemp.put(key, keyValList);
			}			
		}
		return getKeyOrderByStringSameLength(keyValListMapTemp);
	}
	
	/**
	 * 获取排序字符串
	 * 与其他key级联的key优先
	 * 都没有级联则同值区间多的优先，因为同值区间少，尤其是都没有的时候，通常表示唯一自增主键
	 * @param keyValListMap key的值列表Map，String类型列表需确保长度一致
	 * @return LinkedHashMap<String, String> 用来记录排序字段的map
	 */
	public static LinkedHashMap<String, String> getKeyOrderByStringSameLength(Map<String, List<Object>> keyValListMap) {
		//1、获取全排序的字段
		Map<String, List<Integer[]>> allKeySectionsMap = new HashMap<>();
		for (String key : keyValListMap.keySet()) {
			List<Integer[]> sections = getOder(keyValListMap.get(key));
			if (sections.get(0)[0] != 0) {
				allKeySectionsMap.put(key, sections);
			}
		}
		//2、选出合适的
		LinkedHashMap<String, String> orderMap = new LinkedHashMap<>();
		List<Integer[]> sections = new ArrayList<>();
		for (String key : allKeySectionsMap.keySet()) {
			LinkedHashMap<String, String> orderMapTemp = new LinkedHashMap<>();
			List<Integer[]> sectionsTemp = allKeySectionsMap.get(key);
			if (sectionsTemp.get(0)[0] == 1) {
				orderMapTemp.put(key, "ASC");			
			}else {
				orderMapTemp.put(key, "DESC");
			}
			getKeyOrder(keyValListMap, sectionsTemp, null, orderMapTemp);
			//与其他key级联的key优先，都没有级联则同值区间多的优先
			if (orderMapTemp.size() > orderMap.size()
					|| sectionsTemp.size() > sections.size()) {
				orderMap.clear();
				orderMap.putAll(orderMapTemp);
				sections.clear();
				sections.addAll(sectionsTemp);
			}
		}
		return orderMap;
	}
	
	/**求列表的排序情况，第一个数组只有一个值，后面的数组表示相同的值区间：
	 * -1降序，0无，1升序
	* @param datas 实现了comparable的对象列表，如果是String类型，需保证长度一致
	* @return List<Integer[]> 
	*/
	public static List<Integer[]> getOder(List<Object> datas){
		List<Integer[]> sectionList = new ArrayList<>();
		Integer[] firstSection = new Integer[1];
		//1、寻找升降序，从0开始找，同时存在升降时，结果就是无序
		boolean hasGt = false;
		boolean haslt = false;
		Integer[] section = new Integer[2];
		section[0] = 0;
		for (int i = 1; i < datas.size(); i++){
			int result = ((Comparable)datas.get(i)).compareTo(datas.get(i - 1));
			if (result != 0) {
				//String如果第一个字符和参数的第一个字符相等，则以第二个字符和参数的第二个字符做比较，以此类推,直至不等为止，返回该字符的ASCII码差值
				//String如果两个字符串不一样长，可对应字符又完全一样，则返回两个字符串的长度差值
				//String长度一致时，可比
				if (result >= 1){
					hasGt = true;
				}else if(result <= -1){
					haslt = true;
				}
				if (hasGt && haslt) {
					//有升有降不是全排序，无法判断
					firstSection[0] = 0;
					sectionList.add(0, firstSection);
					return sectionList;
				}
				//与后面的值不同
				section[1] = (i - 1);
				sectionList.add(section);
				//新的一个序列从i开始
				section = new Integer[2];
				section[0] = i;
			}
		}
		if (hasGt) {
			firstSection[0] = 1;
		}else {
			firstSection[0] = -1;
		}
		sectionList.add(0, firstSection);
		section[1] = (datas.size() - 1);
		sectionList.add(section);
		return sectionList;
	}		
	
	/**
	 * 递归拼接排序字段
	 * @param keyValListMap key值的列表map
	 * @param sectionList 具有全排序的区间
	 * @param sectionLists
	 * @param orderMap<String, String> 用来记录排序字段的map
	 */
	private static void getKeyOrder(Map<String, List<Object>> keyValListMap
			, List<Integer[]> sectionList
			, List<List<Integer[]>> sectionLists
			, LinkedHashMap<String, String> orderMap) {
		List<List<Integer[]>> subSectionLists = new ArrayList<>();
		for (String key : keyValListMap.keySet()) {
			if (orderMap.keySet().contains(key)) {
				continue;
			}
			//值列表
			List<Object> keyValList = keyValListMap.get(key);
			//验证在区间里是否都是相同的排序
			Set<Integer> subOrderSet = new HashSet<>();
			int order = 0;
			if (sectionList != null) {
				order = check(sectionList, keyValList, subOrderSet, subSectionLists);
			}else if (sectionLists != null) {
				for (int i = 0; i < sectionLists.size(); i++) {
					int subOrder = check(sectionLists.get(i), keyValList, subOrderSet, subSectionLists);
					if (subOrder != 0) {
						order = subOrder;
					}
				}
			}
			//所有区间都只有一种排序方式
			if (subOrderSet.size() == 1) {
				if (order == 1) {
					orderMap.put(key, "ASC");				
				}else {
					orderMap.put(key, "DESC");	
				}
				break;
			}else {
				subSectionLists.clear();
			}
		}
		//这一轮找到了，就继续找
		if (subSectionLists.size() > 0) {
			getKeyOrder(keyValListMap, null, subSectionLists, orderMap);
		}
	}
	
	/**
	 * 检查
	 * @param sectionList
	 * @param keyValList
	 * @param subOrderSet
	 * @param subSectionLists
	 * @return int 最近的排序情况
	 */
	private static int check(List<Integer[]> sectionList
			, List<Object> keyValList
			, Set<Integer> subOrderSet
			, List<List<Integer[]>> subSectionLists
			) {
		int order = 0;
		for (int i = 1; i < sectionList.size(); i++) {
			Integer[] section = sectionList.get(i);
			if (section[0] < section[1]) {
				List<Object> subList = keyValList.subList(section[0], section[1] + 1);
				List<Integer[]> subSectionList = getOder(subList);
				subSectionLists.add(subSectionList);
				if (subSectionList.get(0)[0] != 0) {
					order = subSectionList.get(0)[0];
					subOrderSet.add(subSectionList.get(0)[0]);
				}else {
					//区间排序不同
					subOrderSet.clear();
					subSectionLists.clear();
					break;
				}
			}
		}
		return order;
	}
}