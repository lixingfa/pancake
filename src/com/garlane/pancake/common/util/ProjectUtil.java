package com.garlane.pancake.common.util;

import com.alibaba.fastjson.JSONArray;
import com.garlane.pancake.common.base.AnalyseException;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 项目工具类
 */
public class ProjectUtil {
    /**static目录*/
    private static final String STATIC_PATH = "/src/main/resources/static/";
    /**templates目录*/
    private static final String TEMPLATES_PATH = "/src/main/resources/templates/";
    /**json目录下的change目录*/
    private static final String CHANGE_PATH = "/change";
    /**index主页*/
    private static final String INDEX_PAGE = "index.html";
    /**html文件*/
    private static final String HTML = ".html";
    /**json文件*/
    private static final String JSON = "json";
    /**var*/
    private static final String VAR = "var";
    /**注释的开头*/
    private static final String ANNOTATION_BEGIN = "<!--";
    /**注释的结尾*/
    private static final String ANNOTATION_END = "-->";

    /**注释的正则表达式*/
    private static final String ANNOTATION_REX = "<!--[ \n]*[a-zA-Z0-9 \n]+[ \n]*-->";
    /**LINK的正则表达式*/
    private static final String LINK_REX = "<link[ \n]+[ /='\"\\w]*href[ \n]*=[ \n]*['\"]{1}[-_/.\\w]+['\"]{1}[ /\n]*>";
    /**LINK中的href部分*/
    private static final String LINK_HREF_REX = "href[ \n]*=[ \n]*['\"]{1}[/.\\w]+['\"]{1}";
    /**SCRIPT表达式*/
    private static final String SCRIPT_REX = "<script[ \n]+[ /='\"\\w]*src[ \n]*=[ \n]*['\"]{1}[-_/.\\w]+['\"]{1}[ /\n]*>[ \n]*</script>[ \n]*";
    /**SCRIPT中的SRC部分*/
    private static final String SCRIPT_SRC_REX = "src[ \n]*=[ \n]*['\"]{1}[-/.\\w]+['\"]{1}";

    /**
     * 页面解析、替换和重写
     * @param dir page所在的目录
     * @param pageJsonArraysMap json列表，jsonPath-jsonName-JSONArray
     * @param jsonArraysOwnKeyMap json数组的注释，位于数组上面，说明哪些驼峰key是属于主体的，比如creatTime
     * @param isWeb 页面上传模式
     * @throws AnalyseException
     */
    public static void htmlAnalysis(File dir, Map<String, Map<String, JSONArray>> pageJsonArraysMap
            , Map<String, List<String>> jsonArraysOwnKeyMap, boolean isWeb) throws AnalyseException {
        //获取index.html的内容，并解析出资源目录
        try {
            String projectPath = dir.getAbsolutePath();
            String projectName = dir.getName();
            //1、从主页开始找，可以省很多损耗。如果没有主页，系统生成以后，也没有主入口，所以必须有index
//            File indexHtml = new File(dir.getAbsolutePath() + File.separator + INDEX_PAGE);
//            if (!indexHtml.exists()) {
//                throw new AnalyseException(dir.getAbsolutePath() + "目录下找不到" + INDEX_PAGE);
//            }
            //2、遍历html文件
            Set<String> srcDirsSet = new HashSet<>();
            File[] subFiles = dir.listFiles();
            //先处理最外面的html，找出引用的文件夹，暂时不处理文件夹里的html
            for (File file : subFiles) {
                if (file.isFile() && file.getName().endsWith(HTML)) {
                    //寻找引用文件夹和替换
//                    srcDirsSet.addAll(getSrcDirsFromHtmlContentAndRewrite(file, pageJsonArraysMap, jsonArraysOwnKeyMap, projectPath, projectName));
                }
            }
            //处理子文件夹里的html
            for (File file : subFiles) {
                if (file.isDirectory()) {
//                    replaceSubHtml(file, srcDirsSet, pageJsonArraysMap, jsonArraysOwnKeyMap, projectPath, projectName);
                }
            }
        } catch (Exception e) {
            if (isWeb) {
                FileUtil.FileDelete(dir);//删除所在的目录
            }
            throw new AnalyseException(e.getMessage());
        }
    }

    /**
     * 解析json的内在逻辑
     * @param pageJsonArraysMap json列表，jsonPath-jsonName-JSONArray
     * @param jsonArraysOwnKeyMap json数组的注释，位于数组上面，说明哪些驼峰key是属于主体的，比如creatTime
     * @param dataBaseType 数据库类型
     * @throws AnalyseException
     */
    public static void htmlAnalysis(Map<String, Map<String, JSONArray>> pageJsonArraysMap
            , Map<String, List<String>> jsonArraysOwnKeyMap
            , String dataBaseType
            , boolean isWeb) throws AnalyseException {
        try {
            //3、json内在逻辑处理
//            Map<String, Object> sqlMap = DataBaseUtil.getDateEntityFromJson(pageJsonArraysMap, jsonArraysOwnKeyMap, dataBaseType);//整理数据实体
            //4、
            //逻辑判定，比如请假了就不能上班、时间线要对得上
            //json整合与入库


            //即使声明了要释放文件资源，但这个过程是不确定的，资源回收可以解决这个问题，避免待会无法移动
            System.gc();//进行一次资源回收，但之前读取文件的操作未必会已经释放完毕，可能需要等待
            //、移动文件到系统目录
//			String systemBasePath = new File(Constant.EMPTY).getCanonicalPath();
//			//.1在本工程的static目录下创建projectName
//			String staticDirPath = systemBasePath + File.separator + STATIC_PATH + projectName;
//			for (String srcDir : srcDirsSet) {
//				File file = new File(projectPath + File.separator + srcDir);
//				FileUtil.fileMove(file, staticDirPath + File.separator + srcDir);
//			}
//			//.2其他文件（夹）移动到templates
//			String templatesDirPath = systemBasePath + File.separator + TEMPLATES_PATH + projectName;
//			subFiles = dir.listFiles();
//			for (File file : subFiles) {
//				String name = file.getName();
//				//跳过以英文点号开始的文件和文件夹，一般是工程辅助文件，比如.svn和.git
//				if (name.startsWith(Constant.POINT_EN) || JSON.equals(name)) {
//					FileUtil.FileDelete(file);//删除
//				}
//			}
//			//剩下的文件移动
//			FileUtil.fileMove(dir, templatesDirPath + File.separator);

        } catch (Exception e) {
//            FileUtil.FileDelete(dir);//删除
            throw new AnalyseException(e.getMessage());
        }

    }
}
