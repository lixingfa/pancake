package com.garlane.pancake.common.util;

import java.util.*;

import com.garlane.pancake.common.base.AnalyseException;
import com.garlane.pancake.common.constant.BusinessConstant;
import com.garlane.pancake.common.model.View;
import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.garlane.pancake.common.constant.Constant;

import static com.garlane.pancake.common.model.View.VAR;

public class JSONUtil {
	/**json文件中的注释的正则表达式*/
	private static final String ANNOTATION_REX = "<!--[ \n]*[a-zA-Z0-9 \n]+[ \n]*-->";

	//所有json数据整合成新的还是要保留，因为做demo过程中很容易有错漏，人工很难检查
	//如果发现不一致，则报错
	/**
	 * 格式化json字符串
	 * @param jsonString json字符串
	 * @param jsonPath json的路径
	 * @return JSONArray 格式化的后json
	 */
	public static JSONArray jsonStringParse(String jsonString, String jsonPath){
		//json内容格式化
		JSONArray array;
		try {
			jsonString = jsonString.substring(jsonString.indexOf(Constant.EQ) + 1).trim();
			if (jsonString.startsWith(Constant.BRACKET_LEFT)) {
				array = JSONArray.parseArray(jsonString);
			}else {
				JSONObject jsonObject = JSONObject.parseObject(jsonString);
				array = new JSONArray();
				array.add(jsonObject);
			}
		} catch (Exception e) {
			throw new AnalyseException("格式化json时出现错误。json文件路径：" + jsonPath);
		}
		return array;
	}

	/**
	 * 解析json数组字符串
	 * @param jsonStrList json数组字符串
	 * @param jsonArraysOwnKeyMap json数组的注释，位于数组上面，说明哪些驼峰key是属于主体的，比如creatTime
	 */
	public static Map<String, JSONArray> analysisJsonString(List<String> jsonStrList
			, Map<String, List<String>> jsonArraysOwnKeyMap){
		//当前json文件中，名字与数据的映射
		Map<String, JSONArray> jsonArraysMap = new HashMap<>();
		StringBuilder jsonStrBuilder = new StringBuilder();//用来装载在文件中的一个json或json数组
		String jsonName = Constant.EMPTY;
		for (String jsonStr : jsonStrList) {//逐行读取
			jsonStr = jsonStr.trim();
			//该行为空
			if (StringUtils.isBlank(jsonStr)) {//空行了
				continue;
			}
			//注释
			jsonStr = jsonStr.trim();
			if (jsonStr.matches(ANNOTATION_REX)) {
				//从注释中获取备注的信息
				String[] ownKeys = jsonStr.replaceAll(View.ANNOTATION_BEGIN, Constant.EMPTY)
						.replaceAll(View.ANNOTATION_END, Constant.EMPTY).trim()
						.split(Constant.SPACE);
				//在后续判断时还要加入CREATEDATE和CREATETIME
				List<String> jsonArraysOwnKeyList = new ArrayList<>(Arrays.asList(ownKeys));
				jsonArraysOwnKeyMap.put(jsonName, jsonArraysOwnKeyList);
				continue;
			}
			//新的json开始
			if (jsonStr.contains(VAR)) {
				//拿到变量名称，进行ajax组装和替换
				jsonName = jsonStr.substring(jsonStr.indexOf(VAR) + 2, jsonStr.indexOf(Constant.EQ)).trim();
//				if (jsonStrBuilder.length() > 0) {//组装了一堆json信息
//					findReplaceStr(jsonName, content, jsonPath
//							, repalceMap, clearSet, jsonNameMap);
//					jsonArraysMap.put(jsonName, JSONUtil.jsonStringParse(jsonStrBuilder.toString(), jsonPath));
//					jsonStrBuilder = new StringBuilder();
//				}
			}
			jsonStrBuilder.append(jsonStr);
		}
		if (jsonStrBuilder.length() > 0) {//组装了一堆json信息
//			findReplaceStr(jsonName, content, jsonPath
//					, repalceMap, clearSet, jsonNameMap);
//			jsonArraysMap.put(jsonName, JSONUtil.jsonStringParse(jsonStrBuilder.toString(), jsonPath));
		}
		return jsonArraysMap;
	}

	/**
	 * 获取两个列表共同的片段集
	 * @param array1
	 * @param array2
	 * @return List<JSONArray> 片段集
	 */
	public static List<JSONArray> getSameSubLists(JSONArray array1, JSONArray array2) {
		List<JSONArray> list = new ArrayList<>();
		JSONArray sub = new JSONArray();
		for (int i = 0; i < array1.size(); i++) {
			if (array2.contains(array1.get(i))) {
				sub.add(array1.get(i));//共同拥有
			}else {
				if (sub.size() > 0) {//之前的片段有值
					list.add(sub);
					sub = new JSONArray();
				}
			}
		}
		if (sub.size() > 0) {//最后的片段
			list.add(sub);
		}
		return list;
	}
	
	/**
	 * 获取json最基本的值对象
	 * @param keys 键按层级关系递进的字符串，用短杠相连，首次进入请填null
	 * @param json json对象
	 * @return List<JSONObject> 拆解后直到基本对象的json列表
	 */
	public static List<JSONObject> getBaseValueObjects(String keys, JSONObject json) {
		List<JSONObject> baseValueObjects = new ArrayList<>();
		//不需要单独记录keySet，因为是很容易得到的，记录了反而给处理带来麻烦，因为没有value，跟其他的不一样
		for (String key : json.keySet()) {
			String ks;
			if (keys != null) {//不是初次进来
				ks = keys + Constant.SHORT_BAR + key;
			}else {
				ks = key;
			}
			Object value = json.get(key);
			BusinessConstant.classTypeEnum dataType = BusinessConstant.getClassType(value.getClass().getTypeName());
			JSONObject baseValueObject = new JSONObject();
			baseValueObject.put(BusinessConstant.VALUE, value);
			baseValueObject.put(BusinessConstant.KEYS, ks);
			baseValueObject.put(BusinessConstant.DATA_TYPE, dataType);
			baseValueObjects.add(baseValueObject);
			//进一步拆解
			List<JSONObject> subList = null;
			if (BusinessConstant.classTypeEnum.LIST.equals(dataType)
					|| BusinessConstant.classTypeEnum.JSONARRAY.equals(dataType)) {
				subList = getBaseValueObjects(ks, json.getJSONArray(key));
			}else if (BusinessConstant.classTypeEnum.LINKEDHASHMAP.equals(dataType)
					|| BusinessConstant.classTypeEnum.JSONOBJ.equals(dataType)) {
				subList = getBaseValueObjects(ks, json.getJSONObject(key));
			}
			if (subList != null) {
//				baseValueObject.put(SUB_LIST, subList);
				baseValueObjects.addAll(subList);
			}
		}
		return baseValueObjects;
	}
	
	/**
	 * 获取JSONArray最基本的值对象
	 * @param keys 键按层级关系递进的字符串，用短杠相连，首次进入请填null
	 * @param array json数组对象
	 * @return List<JSONObject>
	 */
	public static List<JSONObject> getBaseValueObjects(String keys, JSONArray array) {
		List<JSONObject> baseValueObjects = new ArrayList<>();
		for (int i = 0; i < array.size(); i++) {
			String ks;
			if (keys != null) {//不是初次进来
				ks = keys + Constant.SHORT_BAR + i;
			}else {
				ks = String.valueOf(i);
			}
			Object value = array.get(i);
			BusinessConstant.classTypeEnum dataType = BusinessConstant.getClassType(value.getClass().getTypeName());
			//值对象
			JSONObject baseValueObject = new JSONObject();
			baseValueObject.put(BusinessConstant.VALUE, value);
			baseValueObject.put(BusinessConstant.INDEX, i);//这个索引，是选项在列表里的索引，不是环境的总索引
			baseValueObject.put(BusinessConstant.DATA_TYPE, dataType);
			baseValueObject.put(BusinessConstant.KEYS, ks);
			baseValueObjects.add(baseValueObject);
			//进一步拆解
			List<JSONObject> subList = null;
			if (BusinessConstant.classTypeEnum.LIST.equals(dataType) 
					|| BusinessConstant.classTypeEnum.JSONARRAY.equals(dataType)) {
				subList = getBaseValueObjects(ks, array.getJSONArray(i));
			}else if (BusinessConstant.classTypeEnum.LINKEDHASHMAP.equals(dataType) 
					|| BusinessConstant.classTypeEnum.JSONOBJ.equals(dataType)) {
				subList = getBaseValueObjects(ks, array.getJSONObject(i));
			}
			if (subList != null) {
//				baseValueObject.put(SUB_LIST, subList);
				baseValueObjects.addAll(subList);
			}
		}
		return baseValueObjects;
	}
	
	/**
	 * 获取JSONArray最基本的值对象，并按最顶层级分组
	 * @param array json数组对象
	 * @return List<JSONObject>
	 */
	public static List<List<JSONObject>> getBaseValueObjectsByFirstItem(JSONArray array) {
		List<List<JSONObject>> baseValueObjectsGroup = new ArrayList<>();
		for (int i = 0; i < array.size(); i++) {
			List<JSONObject> baseValueObjects = new ArrayList<>();
			String ks = String.valueOf(i);
			Object value = array.get(i);
			BusinessConstant.classTypeEnum dataType = BusinessConstant.getClassType(value.getClass().getTypeName());
			//值对象
			JSONObject baseValueObject = new JSONObject();
			baseValueObject.put(BusinessConstant.VALUE, value);
			baseValueObject.put(BusinessConstant.INDEX, i);//这个索引，是选项在列表里的索引，不是环境的总索引
			baseValueObject.put(BusinessConstant.DATA_TYPE, dataType);
			baseValueObject.put(BusinessConstant.KEYS, ks);
			baseValueObjects.add(baseValueObject);
			//进一步拆解
			List<JSONObject> subList = null;
			if (BusinessConstant.classTypeEnum.LIST.equals(dataType)
					|| BusinessConstant.classTypeEnum.JSONARRAY.equals(dataType)) {
				subList = getBaseValueObjects(ks, array.getJSONArray(i));
			}else if (BusinessConstant.classTypeEnum.LINKEDHASHMAP.equals(dataType)
					|| BusinessConstant.classTypeEnum.JSONOBJ.equals(dataType)) {
				subList = getBaseValueObjects(ks, array.getJSONObject(i));
			}
			if (subList != null) {
//				baseValueObject.put(SUB_LIST, subList);
				baseValueObjects.addAll(subList);
			}
			baseValueObjectsGroup.add(baseValueObjects);
		}
		return baseValueObjectsGroup;
	}
	
	/**
	 * 找出对于单个json一直出现的、不同值、结伴出现的
	 * @param objs 多个json，拆解出来的对象列表，一个List<JSONObject>就是一个json拆出来的列表
	 * @return List<Object>
	 * 0一直出现的List<JSONObject>
	 * 1同一对象不同值Map<JSONObject, List<JSONObject>>
	 * 2在同一个json内部结伴出现的List<List<JSONObject>>
	 * 3在不是同一个json内部结伴出现的List<List<JSONObject>>
	 */
	public static List<Object> getAlwaysDifferentCompanyObjs(List<List<JSONObject>> objs) {
		List<Object> resultlList = new ArrayList<>();
		//1、统计每种情况出现的数量（每个json对于每种情况只会拆分出一个）
		Map<JSONObject, Integer> objTotalMap = new HashMap<>();
		for (List<JSONObject> list : objs) {//统计
			for (JSONObject json : list) {
				if (objTotalMap.containsKey(json)) {
					//TODO 如果是数字、日期等可比较的值，则收集并统计是否有取值范围，包括处于某个区间，大于或小于某个值
					objTotalMap.put(json, objTotalMap.get(json) + 1);
				}else {
					objTotalMap.put(json, 1);
				}
			}
		}
		//2、找出一直出现的和结伴出现的
		List<JSONObject> alwaysObj = new ArrayList<>();//一直出现的
		Map<Integer, List<JSONObject>> sizeMap = new HashMap<>();//结伴出现的
		Map<JSONObject, List<JSONObject>> differentResultMap = new HashMap<>();//去除结果后的集合
		for (JSONObject json : objTotalMap.keySet()) {
			int total = objTotalMap.get(json);
			if (total == objs.size()) {
				alwaysObj.add(json);//一直成立的
				continue;
			}
			//放入sizeMap
			if (!sizeMap.containsKey(total)) {
				sizeMap.put(total, new ArrayList<>());
			}
			sizeMap.get(total).add(json);
			//
			JSONObject key = (JSONObject) json.clone();
			key.remove(BusinessConstant.COMMON_RESULT);//把结果去掉，就得到不带结果的key
			if (!differentResultMap.containsKey(key)) {
				differentResultMap.put(key, new ArrayList<>());
			}
			differentResultMap.get(key).add(json);
		}
		resultlList.add(alwaysObj);//把一直出现的放第一位
		//3、清理不是不同结果构成总数的
		Iterator<JSONObject> iterator = differentResultMap.keySet().iterator();
		while (iterator.hasNext()) {
			JSONObject key = (JSONObject) iterator.next();
			List<JSONObject> list = differentResultMap.get(key);
			Map<Object, Integer> crTotalMap = new HashMap<>();//比较的结果集，不同结果应该呈现不均匀分布，才可以给小众找原因
			if (list.size() > 1) {//必须大于1，否则没意义，不同结果至少2个才叫不同
				int total = 0;
				for (JSONObject json : list) {
					String cr = json.getString(BusinessConstant.COMMON_RESULT);
					crTotalMap.put(key, crTotalMap.getOrDefault(cr, 0) + 1);
					//
					int n = objTotalMap.get(json);//每个json出现的次数
					total = total + n;
				}
				if (total != objs.size()) {//不是每次都出现的，也就是即使结果不同，但作为比较主体每次都应该有，才能区分大众和小众
					iterator.remove();
					continue;
				}
			}else {
				iterator.remove();
				continue;
			}
			//找到最大的，要求最大的占比符合差异分布
			float max = 0f;
			for (Object cr : crTotalMap.keySet()) {
				float r = (float)crTotalMap.get(cr) / list.size();
				if (r > max) {
					max = r;
				}
			}
			if (crTotalMap.size() < 10) {
				float big = crTotalMap.size();
				if (10 - big > big) {
					big = 10 - big;
				}
				if (max < big / 10) {
					iterator.remove();//不符合不均匀占比，种类越少，占大头的份额就越多
				}
			}else {
				throw new AnalyseException("不同结果种类超过了10。");
			}
		}
		resultlList.add(differentResultMap);
		//4、找出出现多次并且有同伴的
		Map<JSONObject, List<List<JSONObject>>> jsonToInSamllComp = new HashMap<>();//每个json对应的内部原始组小集合
		for (List<JSONObject> list : objs) {//在同一个原始组合里，且总体出现次数一致，才是一个小组合
			Map<Integer, List<JSONObject>> map = new HashMap<>();
			//在同一个原始组合里
			for (JSONObject json : list) {
				if (alwaysObj.contains(json)) {
					continue;//这里要去掉一直成立的
				}
				int n = objTotalMap.get(json);//json在已知组合里出现的次数
				if (!map.containsKey(n)) {
					map.put(n, new ArrayList<>());
				}
				map.get(n).add(json);//总体出现次数一致，才是一个小组合
			}
			for (Integer n : map.keySet()) {
				if (n < 3) {
					continue;//事应过三
				}
				List<JSONObject> l = map.get(n);
				if (l.size() > 1) {//要求大于1，即有同伴的
					for (JSONObject json : l) {
						if (!jsonToInSamllComp.containsKey(json)) {
							jsonToInSamllComp.put(json, new ArrayList<>());
						}
						jsonToInSamllComp.get(json).add(l);
					}
				}
			}
		}
		//对相同元素的组合进行合并
		List<List<JSONObject>> inComps = new ArrayList<>();//内部小组合
		for (JSONObject json : jsonToInSamllComp.keySet()) {
			List<List<JSONObject>> jsonSamllComps = jsonToInSamllComp.get(json);//与json有关的小组合
			if (jsonSamllComps == null) {
				continue;
			}
			//对它们取交集
			List<JSONObject> comp = new ArrayList<>();//公用数据，避免影响其他的
			comp.addAll(jsonSamllComps.get(0));
			for (int i = 1; i < jsonSamllComps.size(); i++) {
				comp.retainAll(jsonSamllComps.get(i));
			}
			if (comp.size() > 1) {
				inComps.add(comp);
				for (JSONObject j : comp) {//已经归属到同一组，剩下的元素就不用再找了
					jsonToInSamllComp.put(j, null);//与上面的为null则跳过相呼应
				}
			}
		}
		resultlList.add(inComps);
		//5、找出不在同一个原始组里的小组合
		List<List<JSONObject>> outComps = new ArrayList<>();//外部小组合
		for (Integer n : sizeMap.keySet()) {
			if (n < 3) {
				continue;//事应过三
			}
			List<JSONObject> list = sizeMap.get(n);//出现次数相同的
			List<JSONObject> outComp = new ArrayList<>();
			for (JSONObject json : list) {
				if (!jsonToInSamllComp.containsKey(json)) {
					outComp.add(json);//是否不允许交叉？
				}
			}
			if (outComp.size() > 1) {
				outComps.add(outComp);
			}
		}
		resultlList.add(outComps);
		return resultlList;
	}
	
	/**
	 * 根据key，获取真正的值
	 * @param data
	 * @param key 以短杠做层级分隔的键
	 * @return Object
	 */
	public static Object getTrueValue(JSONObject data, String key) {
		if (key.contains(Constant.SHORT_BAR)) {
			String[] keys = key.split(Constant.SHORT_BAR);//以短杠切割
			Object obj = data;
			for (int i = 0; i < keys.length; i++) {
				String k = keys[i];
				String typeName = obj.getClass().getTypeName();
				if (typeName.equals(BusinessConstant.classTypeEnum.JSONOBJ) || typeName.equals(BusinessConstant.classTypeEnum.LINKEDHASHMAP)) {
					JSONObject json;
					if (typeName.equals(BusinessConstant.classTypeEnum.JSONOBJ)) {
						json = ((JSONObject)obj);//数组会被提前跳过						
					}else {
						json = new JSONObject((LinkedHashMap)obj);
					}
					if (json.containsKey(k)) {
						if (i < keys.length - 1) {
							String next = keys[i + 1];
							if (next.equals(BusinessConstant.ObjJSONArrayRelationEnum.LIST_BEGIN)
									|| next.equals(BusinessConstant.ObjJSONArrayRelationEnum.LIST_END)) {//开始或结束
								if (json.get(k) instanceof JSONArray) {
									JSONArray array = json.getJSONArray(k);
									if (next.equals(BusinessConstant.ObjJSONArrayRelationEnum.LIST_BEGIN)) {
										obj = array.get(0);
									}else {
										obj = array.get(array.size() - 1);
									}
									i = i + 2;
								}else {
									return null;
								}
							}else {
								obj = json.get(k);
							}
						}else {//最后一个
							obj = json.get(k);
						}
					}else {
						return null;
					}
				}else if (typeName.equals(BusinessConstant.classTypeEnum.LIST) || typeName.equals(BusinessConstant.classTypeEnum.JSONARRAY)) {
					JSONArray array = (JSONArray)obj;
					return array.get(Integer.parseInt(k));
				}else {
					return null;//不是json了，但key还在延续					
				}
			}
			return obj;
		}else {
			return data.get(key);			
		}
	}	
	
	/**
	 * 根据key，获取真正的值
	 * @param array 数组
	 * @param key 以短杠做层级分隔的键
	 * @return Object
	 */
	public static Object getTrueValue(JSONArray array, String key) {
		String[] keyLevel = key.split(Constant.SHORT_BAR);//以短杠切割
		JSONArray tempArray = array;
		for (int j = 0; j < keyLevel.length; j++) {
			if (!NumberUtil.isEnNum(keyLevel[j])) {							
				break;
			}
			int index = Integer.parseInt(keyLevel[j]);
			String typeName = tempArray.get(index).getClass().getTypeName();
			if (BusinessConstant.classTypeEnum.JSONARRAY.equals(typeName)
					|| BusinessConstant.classTypeEnum.LIST.equals(typeName)) {
				tempArray = tempArray.getJSONArray(index);				
			}else {//json
				key = key.substring(key.lastIndexOf(keyLevel[j]) + 2);//本身，加一个短杠，再加真正索引开始的位置
				JSONObject data = tempArray.getJSONObject(index);
				return getTrueValue(data, key);
			}
		}
		return null;
	}
	
	
	/**
	 * 获取json2没有的key，判断两个json是否描述同一个事物
	 * @param json
	 * @param json2
	 * @param fatherKey
	 * @return json2不包含的key
	 */
	public static List<String> getDiffrentKeysInJson2(JSONObject json, JSONObject json2, String fatherKey) {
		List<String> notKey = new ArrayList<>();
		for (String key : json.keySet()) {
			Object val = json.get(key);
			if (!json2.containsKey(key)) {//json2没有这个key
				if (StringUtils.isNotBlank(fatherKey)) {
					notKey.add(fatherKey + key);
				}else {
					notKey.add(key);					
				}
			}else {//都有这个key
				Object val2 = json2.get(key);
//				if(!val.equals(val2)) {
//					throw
//				}				
				String typeName = val.getClass().getTypeName();
				String typeName2 = val2.getClass().getTypeName();
				if (typeName.equals(typeName2)) {//类型相同继续添加
					if (BusinessConstant.classTypeEnum.LINKEDHASHMAP.equals(typeName)) {
						notKey.addAll(getDiffrentKeysInJson2(json.getJSONObject(key), json2.getJSONObject(key), key));
					}else if (BusinessConstant.classTypeEnum.LIST.equals(typeName)) {
						//没遇到过这种情况
					}
				}else {
					continue;//数据的类型都不同，按理说不是同一个事物的描述
				}
			}
		}
		//找出json2有，json没有的
		for (String key : json2.keySet()) {
			if (!json.containsKey(key)) {//json没有这个key
				if (StringUtils.isNotBlank(fatherKey)) {
					notKey.add(fatherKey + key);
				}else {
					notKey.add(key);					
				}
			}
		}
		return notKey;
	}
	
	/**
	 * 合并json
	 * @param json
	 * @param tempJson
	 */
	public static void mergeJson(JSONObject json, JSONObject tempJson) {
		for (String key : tempJson.keySet()) {
			Object val = tempJson.get(key);
			if (!json.containsKey(key)) {//没有才采用，否则不覆盖
				json.put(key, val);
			}else {//json有对应的key，则需要进一步分析
				String typeName = val.getClass().getTypeName();
				if (BusinessConstant.classTypeEnum.LINKEDHASHMAP.equals(typeName)) {
					mergeJson(json.getJSONObject(key), tempJson.getJSONObject(key));
				}else if (BusinessConstant.classTypeEnum.LIST.equals(typeName)) {
					JSONArray tempSubArray = tempJson.getJSONArray(key);
					JSONArray jsonSubArray = json.getJSONArray(key);
					jsonSubArray.addAll(tempSubArray);//TODO 是否还应该进一步分析？但双向循环也不好判断
				}				
			}
		}
	}
	
	/**
	 * 根据相同的KV合并json
	 * @param jsonArray json集合
	 * @return JSONArray
	 */
	public static JSONArray mergeJsonBySameKV(JSONArray jsonArray) {
		List<JSONObject> jsonList = new ArrayList<>(jsonArray.toJavaList(JSONObject.class));
		jsonList = mergeJsonBySameKV(jsonList);
		JSONArray array = new JSONArray();
		for (JSONObject json : jsonList) {
			array.add(json);
		}
		return array;
	}
	
	/**
	 * 根据相同的KV合并json
	 * @param jsonList json集合
	 * @return List<JSONObject>
	 */
	public static List<JSONObject> mergeJsonBySameKV(List<JSONObject> jsonList) {		
		Map<String, Map<Object, Set<JSONObject>>> kVJsonSetMap = new HashMap<>();
		//统计
		for (JSONObject json : jsonList) {
			for (String key : json.keySet()) {
				if (!kVJsonSetMap.containsKey(key)) {
					kVJsonSetMap.put(key, new HashMap<>());
				}
				Object val = json.get(key);
				Map<Object, Set<JSONObject>> vJsonSetMap = kVJsonSetMap.get(key);
				if (!vJsonSetMap.containsKey(val)) {
					vJsonSetMap.put(val, new HashSet<>());
				}
				vJsonSetMap.get(val).add(json);
			}
		}
		//json之间相同值的key	
		for (int i = 0; i < jsonList.size(); i++) {
			JSONObject json = jsonList.get(i);
			for (int j = i + 1; j < jsonList.size(); j++) {
				JSONObject json2 = jsonList.get(j);
				if (json2.size() == 0) {
					continue;
				}
				//找出相同的KV
				Set<String> sameKeySet = new HashSet<>();
				for (String key : json.keySet()) {
					Object val = json.get(key);
					if (json2.containsKey(key)) {
						if (val.equals(json2.get(key))) {
							sameKeySet.add(key);
						}else {
							//同key，但值不同，说明存在冲突或不能组合
							sameKeySet.clear();
							break;
						}						
					}
				}				
				if (sameKeySet.size() > 0) {
					json.putAll(json2);
					json2.clear();
				}
			}
		}
		Iterator<JSONObject> iterator = jsonList.iterator();
		while (iterator.hasNext()) {
			JSONObject json = iterator.next();
			if (json.size() == 0) {
				iterator.remove();
			}
		}
		return jsonList;
	}
	
	/**
	 * 获得json所属的key
	 * @param envKey 带有环境前缀的key
	 * @return String
	 */
	public static String getTrueJsonKey(String envKey) {
		String[] keyLevel = envKey.split(Constant.SHORT_BAR);
		int j = 0;
		for (; j < keyLevel.length; j++) {
			if (!NumberUtil.isEnNum(keyLevel[j])) {							
				break;
			}
		}
		int index = envKey.lastIndexOf(keyLevel[j]);
		return envKey.substring(index);//替换回不带索引前缀的格式
	}
		
}
