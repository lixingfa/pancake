package com.garlane.pancake.common.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class NumberUtil {

	/**正负数*/
	public static final String NUM_REX = "[-]{0,1}[0-9]*[.]{0,1}[0-9]+";
	/**整数*/
	public static final String NUM_ZHENG_REX = "[-]{0,1}[0-9]+";
	/**小数*/
	public static final String NUM_XIAO_REX = "[-]{0,1}[0-9]*[.]{1}[0-9]+";
	/**中文数字*/
	public static final String NUM_REX_CHINESE = "[零一二三四五六七八九十百千万亿兆点]+";
	/**数值范围1阿拉伯数字短杠形式，只允许正数，否则-会发生混淆*/
	public static final String NUM_RANGE_REX1 = "[0-9]*[.]{0,1}[0-9]+[ ]*-[ ]*[0-9]*[.]{0,1}[0-9]+";
	/**数值范围2阿拉伯数字至形式*/
	public static final String NUM_RANGE_REX2 = "[-]{0,1}[0-9]*[.]{0,1}[0-9]+[ ]*至[ ]*[-]{0,1}[0-9]*[.]{0,1}[0-9]+";
	/**
	 * 获取字符串列表的正则表达式
	 * @param s 字符串列表
	 * @return String 正则表达式
	 */
	public static String getNumRex(List<String> s) {
		Set<String> rexSet = new HashSet<>();
		for (String str : s) {
			rexSet.add(getNumRex(str));
		}
		if (rexSet.size() != 1) {
			return null;
		}else {
			return new ArrayList<String>(rexSet).get(0);
		}
	}
	
	/**
	 * 获取某个字符串的数字正则表达式
	 * @param s 字符串
	 * @return String 正则表达式
	 */
	public static String getNumRex(String s) {
		if (StringUtil.isMatchRex(s, NUM_ZHENG_REX)) {
			return NUM_ZHENG_REX;
		}else if (StringUtil.isMatchRex(s, NUM_REX)) {
			return NUM_REX;
		}else if (StringUtil.isMatchRex(s, NUM_REX_CHINESE)) {
			return NUM_REX_CHINESE;
		}
		return null;
	}
	
	/**
	 * 判断字符串是不是数字
	 * @param s 字符串
	 * @return boolean
	 */
	public static boolean isNum(String s) {
		if (isEnNum(s)) {
			return true;
		}else if (isChineseNum(s)) {
			return true;
		}else {
			return false;
		}
	}
	
	/**
	 * 判断一组字符串是否都是阿拉伯数字
	 * @param list 字符串列表
	 * @return List<Integer>/null
	 */
	public static List<Integer> allIsEnNum(List<String> list) {
		boolean all = true;
		List<Integer> nums = new ArrayList<>();
		for (String s : list) {			
			if (!isEnNum(s)) {
				all = false;
				break;
			}
			nums.add(Integer.parseInt(s));
		}
		if (all) {
			return nums;
		}else {
			return null;
		}
	}
	
	/**
	 * 判断字符串是不是阿拉伯数字
	 * @param s 字符串
	 * @return boolean
	 */
	public static boolean isEnNum(String s) {
		return StringUtil.isMatchRex(s, NUM_REX);
	}
	
	/**
	 * 判断字符串是不是中文数字
	 * @param s 字符串
	 * @return boolean
	 */
	public static boolean isChineseNum(String s) {
		return StringUtil.isMatchRex(s, NUM_REX_CHINESE);
	}
	
	/**
	 * 找出一堆数值的取值区间
	 * @param nums 数值
	 * @return List<Integer[]> 取值区间
	 */
	public static List<List<Integer>> getNumRanges(List<Integer> nums){
		List<List<Integer>> ranges = new ArrayList<>();
		Set<Integer> sortSet = new TreeSet<Integer>(Comparator.naturalOrder());
		sortSet.addAll(nums);
		List<Integer> list = new ArrayList<>(sortSet);
		int flag = list.get(0);
		int begin = flag;
		for (int i = 1; i < list.size(); i++) {
			flag++;
			if (flag != list.get(i)) {
				List<Integer> range = new ArrayList<>(2);
				range.add(begin);
				range.add(list.get(i - 1));
				ranges.add(range);
				flag = list.get(i);
				begin = flag;
			}
		}
		List<Integer> range = new ArrayList<>(2);
		range.add(begin);
		range.add(list.get(list.size() - 1));
		ranges.add(range);
		return ranges;
	}
	
	/**
	 * 对某些索引数组进行处理，使之得到正确的索引组合
	 * @param indexs 索引数组
	 * @return List<List<Integer>>
	 */
	public static List<List<Integer>> getRightOrder(List<List<Integer>> indexs){
		//[0,1,3][3,4][5,11],[6,7]
		//[0,1,3][3,4][5][6,7]，11被干掉了
		//把每组数据都按升序排一遍
		for (List<Integer> list : indexs) {
			Collections.sort(list);
		}
		for (int i = indexs.size() - 2; i >= 0; i--) {
			List<Integer> before = indexs.get(i + 1);
			List<Integer> index = indexs.get(i);
			Iterator<Integer> iterator = index.iterator();
			while (iterator.hasNext()) {
				Integer item = iterator.next();
				if (item > before.get(before.size() - 1)) {
					iterator.remove();
				}
			}
			if (index.size() == 0) {
				return null;//有的节点都没有了
			}
		}
		return indexs;
	}
		
	/**
	 * 找出相加等于某个值的组合
	 * @param nums 备选数字
	 * @param result 结果
	 * @return List<List<Integer>>
	 *
	public static List<List<Integer>> findCombs(List<Integer> nums, int result){
		return findCombs(nums, 0, -1, result);
	}
	public static List<List<Integer>> findCombs(List<Integer> nums, int sum, int currIndex, int result){
        List<List<Integer>> lists = new ArrayList<>();
        for(int i = currIndex + 1; i < nums.size(); i++){
            int s = nums.get(i) + sum;
            if(s < result){//继续寻找
                List<List<Integer>> iLink = findCombs(nums, s, i, result);
                for(List<Integer> list : iLink){
                    list.add(0 , nums.get(i));
                }
                if(iLink.size() > 0){
                	lists.addAll(iLink);
                }
            }else if(s > result){//超了
                continue;
            }else{//相等
                List<Integer> list = new ArrayList<>();
                list.add(nums.get(i));
                lists.add(list);
            }
        }
        return lists;
    }*/

	
	//以前我都在做些什么乱七八糟的……
	/**
	 * toEnNumberChar:(字符变阿拉伯数字字符组)
	 * @author lixingfa
	 * @date 2019年4月24日下午2:19:02
	 * @param s
	 * @return char[]
	 * @throws Exception
	 *
	public static char[] toEnNumberChar(String s) throws Exception{
		if (Pattern.matches("[1-9]{1}[0-9]*", s)) {
			return s.toCharArray();
		}else {//中文
			return chineseNumber2Chars(s);
		}
	}
	/**
	 * 字符串转Long
	 * @param s 字符串
	 * @return Long整型
	 * @throws Exception
	 *
	public static Long toEnNumber(String s) throws Exception{
		if (Pattern.matches("[1-9]{1}[0-9]*", s)) {
			try {
				return Long.parseLong(s);				
			} catch (Exception e) {
				throw new Exception(s + "转换Long出错");
			}
		}else {//中文
			return Long.parseLong(String.valueOf(chineseNumber2Chars(s)));
		}
	}
	
	/**
	 * 格式化结果，数字返回数字，中文返回中文
	 * @param s 之前的入参
	 * @return 数字或中文形式
	 *
	public static String toString(String s,String res) throws Exception{
		if (Pattern.matches("[1-9]{1}[0-9]*", s)) {
			return res;
		}else {
			return toChinese(res);
		}
	}
	
	/**
	 * add:(加法)
	 * @author lixingfa
	 * @date 2019年4月24日下午2:25:02
	 * @param one
	 * @param other
	 * @return String
	 *
	public static String add(char[] one,char[] other){
		StringBuffer sb = new StringBuffer();
		int small = 0;
		char[] sc;
		int big = 0;
		char[] bc;
		if (one.length > other.length) {
			big = one.length;
			bc = one;
			small = other.length;
			sc = other;
		}else {
			big = other.length;
			bc = other;
			small = one.length;
			sc = one;
		}
		int jin = 0;
		int b = big - 1;
		for (int s = small - 1; s >= 0; s--,b--) {//尾部对齐
			 int t = Integer.parseInt(String.valueOf(sc[s])) + Integer.parseInt(String.valueOf(bc[b])) + jin;
			 jin = 0;
			 if (t >= 10) {
				jin = 1;
			}
			sb.insert(0, t % 10);
		}
		for (; b >= 0; b--) {
			int t = Integer.parseInt(String.valueOf(bc[b])) + jin;
			jin = 0;
			 if (t >= 10) {
				jin = 1;
			}
			sb.insert(0, t % 10);
		}
		if (jin != 0) {
			sb.insert(0, jin);
		}
		return sb.toString();
	}
	
	/**
	 * add:(减法)
	 * @author lixingfa
	 * @date 2019年4月24日下午2:25:02
	 * @param one
	 * @param other
	 * @return String
	 *
	public static String minus(char[] one,char[] other){
		StringBuffer sb = new StringBuffer();
		int small = 0;
		char[] sc;
		int big = 0;
		char[] bc;
		if (one.length >= other.length) {
			big = one.length;
			bc = one;
			small = other.length;
			sc = other;
		}else {
			big = other.length;
			bc = other;
			small = one.length;
			sc = one;
		}
		int jie = 0;//借位
		int b = big - 1;
		for (int s = small - 1; s >= 0; s--,b--) {//尾部对齐
			int bt = Integer.parseInt(String.valueOf(bc[b]));
			if (jie != 0) {
				bt = bt - jie;
				jie = 0;
			}
			int t = bt - Integer.parseInt(String.valueOf(sc[s]));
			if (t < 0) {
				jie++;
				t = t + 10;
			}
			sb.insert(0, t);
		}
		for (; b >= 0; b--) {
			int t = Integer.parseInt(String.valueOf(bc[b])) - jie;
			jie = 0;
			 if (t < 0) {
				jie++;
				t = t + 10;
			}
			sb.insert(0, t);
		}
		if (jie != 0) {
			sb.insert(0, jie);
		}
		//从头到尾开始除去0
		int index = -1;
		for (int i = 0; i < sb.length(); i++) {
			if(sb.charAt(i) != '0'){
				index = i;
				break;
			}
		}
		String result;
		if (index != -1) {
			result = sb.substring(index);
		}else {
			return "0";
		}
		if (one.length < other.length) {
			return "-" + result;
		}
		return result;
	}
	/**
	 * toChinese:(阿拉伯数字变中文数字)
	 * @author lixingfa
	 * @date 2019年4月24日下午2:20:25
	 * @param string
	 * @return
	 *
	private static String toChinese(String string) {
		try {
			char[] num = new char[]{'零','一','二','三','四','五','六','七','八','九'};
	        char[] unit = new char[]{'十','百','千','万','十','百','千','亿','十','百','千','兆','十','百','千','万','亿'};
			StringBuffer result = new StringBuffer();			
			boolean zero = false;//之前是0
			boolean begin = false;
			int lastUnit = -1;
			boolean needZero = false;
			int index = 0;
			int i = string.length() - 1;
			if (string.startsWith("-")) {
				i--;
			}
			for (; i >= 0; i--) {
				int n = string.charAt(i) - '0';
				if (n == 0) {
					if (index == 4 || index == 8 || index == 12) {//看看是不是个位
						lastUnit = index - 1;
					}
					if (zero) {//之前也有0
						needZero = true;
					}
					zero = true;
					index++;
					continue;
				}else {
					if (needZero && begin) {
						result.insert(0, '零');
						needZero = false;
						zero = false;
					}
					result.insert(0, num[n]);
					if (lastUnit != -1) {
						result.insert(1, unit[lastUnit]);
					}
					if (index > 0) {
						result.insert(1, unit[index - 1]);
					}
					begin = true;
					index++;
				}
			}
			if (string.startsWith("-")) {
				result.insert(0, '负');
			}
			return result.toString() + "(" + string + ")";			
		} catch (Exception e) {
			return string;
		}
	}
	
	/**
	 * chineseNumber2Chars:(这里用一句话描述这个方法的作用)
	 * @author lixingfa
	 * @date 2019年4月24日下午2:20:56
	 * @param chineseNumber
	 * @return
	 *
	private static char[] chineseNumber2Chars(String chineseNumber){
		chineseNumber = chineseNumber.replace("两", "二");
		StringBuffer s = new StringBuffer();
        char[] num = new char[]{'零','一','二','三','四','五','六','七','八','九'};
        char[] unit = new char[]{'十','百','千','万','亿'};
        Map<Integer, Integer> unitInfo = new LinkedHashMap<Integer, Integer>();//keySet按原序
        //寻找单位
        for (int i = 0; i < chineseNumber.length(); i++) {
        	char c = chineseNumber.charAt(i);
        	boolean n = false;
        	for (int j = 0; j < num.length; j++) {
				if (c == num[j]) {
					n = true;
					break;
				}
			}
        	if (!n) {//单位
				for (int j = 0; j < unit.length; j++) {
					if (unit[j] == c) {
						unitInfo.put(i, j);
						break;
					}
				}
			}
        }
        //开始组装
        boolean shunjie = false;//顺接，即当前是万亿，之后会出现万，这样就不用补亿的零
        for (int i = 0; i < chineseNumber.length(); i++) {
        	char c = chineseNumber.charAt(i);
        	if (unitInfo.containsKey(i)) {//单位        		
        		if (i == chineseNumber.length() - 1) {//末尾是单位，补零
        			int how = unitInfo.get(i);
        			if (how == 4) {//亿
        				how = how + 3;
        			}
        			for (int j = how; j >= 0; j--) {
        				s.append(0);
        			}
        		}else if (unitInfo.containsKey(i + 1)) {//者下一位也是单位，
        			if (c == '十') {
        				if (i == 0) {//十是简写
        					s.append(1);
        				}
        				s.append(0);
        			}else {
        				if (unitInfo.get(i + 1) > unitInfo.get(i)) {//下一位是升高的
        					int how = unitInfo.get(i);
        					if (how == 4) {//亿
								how = how + 3;
							}
        					for (int j = how; j >= 0; j--) {
                				s.append(0);
                			}
						}
					}
				}
			}else{
				if (c == '零') {//补零
					shunjie = false;
        			int nowUnitIndex = -1;
        			int next = -1;//个位
        			for (Integer j : unitInfo.keySet()) {
						if (j < i) {
							nowUnitIndex = unitInfo.get(j);							
						}
					}
        			//查看是否顺接
        			for (Integer j : unitInfo.keySet()) {
						if (j > i && nowUnitIndex == unitInfo.get(j) + 1) {
							shunjie = true;
						}
					}
        			for (Integer j : unitInfo.keySet()) {
						if(j > i){//只用找最近的
							next = unitInfo.get(j);
							break;
						}
					}
        			int how = 0;
        			if (next == -1) {
						how = nowUnitIndex;
					}else {
						how = nowUnitIndex - next - 1;						
						if (nowUnitIndex == 4 && !shunjie) {
							how = how + 3;//
						}
					}
        			for (int j = 0; j < how; j++) {//补零
						s.append(0);
					}
				}else {
					for (int j = 0; j < num.length; j++) {
						if (num[j] == c) {
							s.append(j);
							break;
						}
					}
				}
			}
		}        
        return s.toString().toCharArray();
	}
	
	/**
	 * 从多种可能的索引里找到正确的一组
	 * @param indexs List<List<Integer>>
	 * @return List<Integer>
	 *
	public static Integer[] getTrueList(List<List<Integer>> indexs){
		List<Integer> pIndexs = indexs.get(0);
		List<Integer[]> results = new ArrayList<>();
		for (int i = 0; i < pIndexs.size(); i++) {
			Integer[] result = new Integer[indexs.size()];
			result[0] = pIndexs.get(i);
			getTrueListValue(result[0], 1, indexs, result);//处理
			if (result[result.length - 1] != null) {
				results.add(result);
			}
		}
		if (results.size() == 1) {
			return results.get(0);
		}else {
			return null;
		}
	}
	
	/**
	 * 获取可能的值
	 * @param last 上一个的取值
	 * @param bigIndex 当前索引
	 * @param indexs List<List<Integer>> 型列表
	 * @param result int[] 符合的结果数组
	 *
	private static void getTrueListValue(int last, int bigIndex, List<List<Integer>> indexs,Integer[] result) {
		List<Integer> pIndexs = indexs.get(bigIndex);
		for (int i = 0; i < pIndexs.size(); i++) {
			Integer index = pIndexs.get(i);
			if (index > last) {
				result[bigIndex] = index;
				bigIndex++;
				if (bigIndex < indexs.size()) {
					getTrueListValue(index, bigIndex, indexs, result);					
				}
			}
		}
	}
	*/
}
