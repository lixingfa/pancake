package com.garlane.pancake.common.model.view;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 页面获取数据的动作
 */
@Data
public class GetDataAction {
//    /**json数据库，session，url*/
//    public enum fromEnum {JSON, SESSION, URL}

    public GetDataAction(){
//        this.setFrom(fromEnum.JSON);
        this.isSingle = false;
    }

    /**被赋值的短语*/
    private String theSetedPhrase;
//    /**数据来源*/
//    private fromEnum from;
    /**json数组的名称*/
    private String jsonName;
    /**获取单个数据*/
    private boolean isSingle;
    /**筛选数据的条件*/
    private String condition;
    /**排序的条件*/
    private String order;
    /**要替换的代码行，由于注释的存在，以及会出现多行，所以还是采用跟起始位置最近的，
     * 这原来html里的这部分代码要替换成 theSetedPhrase + ajax的形势*/
    private List<String> replaceCodeList = new ArrayList<>();
    /**vueDataName，将对应的行替换成 vueDataName : null的格式*/
    private String vueDataName;
}
