package com.garlane.pancake.common.model.view;

/**
 * 设置动作
 */
public class SetValAction {
    /**组件的id，唯一标识*/
    private String id;
    /**值，放到显示层的都是字符串，如果是变量则以.分隔*/
    private String value;

}
