package com.garlane.pancake.common.model;

import com.garlane.pancake.common.model.database.Table;

import java.util.ArrayList;
import java.util.List;

/**
 * 数据库模型
 */
public class DataBase {
    /**数据库名*/
    private String name;
    /**表*/
    List<Table> tables = new ArrayList<>();

    public DataBase(String name){
        this.name = name;
    }

    /**
     * 获取表
     * @param tableName 表名
     * @return 数据表模型
     */
    public Table getTable(String tableName){
        for (int i = 0; i < tables.size(); i++) {
            if (tables.get(i).getName().equals(tableName)) {
                return tables.get(i);
            }
        }
        return null;
    }

}
