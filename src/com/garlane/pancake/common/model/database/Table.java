package com.garlane.pancake.common.model.database;

import lombok.Data;

import java.util.List;

/**
 * 数据表模型
 */
@Data
public class Table {
    /**表名*/
    private String name;
    /**列*/
    private List<Column> columnList;
}
