package com.garlane.pancake.common.model.database;

/**
 * 数据列模型
 * @author Lixingfa
 *
 */
public class Column {
	/**名称*/
	private String name;
	/**标题*/
	private String title;
	/**页面数据类型*/
	//private Constant.dataType dataType;
	/**示例数据最大长度*/
	private Long dataLength;
	/**不为空*/
	private boolean notNull = false;
	/**小数位数*/
	private int decimals = 0;
	/**默认值*/
	private String Default = null;

	public Column(String name, String title, Long dataLength){
		this.name = name;
		this.title = title;
		this.dataLength = dataLength;
	}
	
	public Column(String name, String title){
		if (name == null) {
			name = "";
		}
		if (title == null) {
			title = "";
		}
		this.name = name;
		this.title = title;
		this.dataLength = 30l;
	}

}
