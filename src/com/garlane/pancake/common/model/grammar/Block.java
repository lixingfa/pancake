package com.garlane.pancake.common.model.grammar;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lixingfa
 * @description: 代码块
 * @date 2022/4/8 14:05
 */
public class Block {
    List<Sentence> sentences = new ArrayList<>();
}
