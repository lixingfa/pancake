package com.garlane.pancake.common.model.grammar;

import com.garlane.pancake.common.constant.BusinessConstant;

/**
 * @author lixingfa
 * @description: 数据实体
 * @date 2022/4/9 23:34
 */
public class Data {
    /**名称*/
    private String name;
    /**类型*/
    private BusinessConstant.classTypeEnum classType;
    /**值*/
    private Object value;
    /**值是变量还是常量*/

}
