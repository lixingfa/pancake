package com.garlane.pancake.common.model.grammar;

import com.garlane.pancake.common.constant.BusinessConstant;

/**
 * @author lixingfa
 * @description: 布尔运算
 * @date 2022/4/11 15:58
 */
@lombok.Data
public class BooleanOperation {
    /**左边的数据*/
    private Data one;
    /**比较类型*/
    private BusinessConstant.compereEnum compereEnum;
    /**右边的数据*/
    private Data theOther;
}
