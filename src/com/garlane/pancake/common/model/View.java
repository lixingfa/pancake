package com.garlane.pancake.common.model;

import com.alibaba.fastjson.JSONArray;
import com.garlane.pancake.common.base.AnalyseException;
import com.garlane.pancake.common.constant.Constant;
import com.garlane.pancake.common.model.view.GetDataAction;
import com.garlane.pancake.common.util.FileUtil;
import com.garlane.pancake.common.util.JSONUtil;
import com.garlane.pancake.common.util.StringUtil;
import com.garlane.pancake.frame.vue3.Vue3Util;
import lombok.Data;

import java.io.File;
import java.util.*;

/**
 * 展示层
 */
@Data
public class View {
    /**项目的完整路径，用于拼接具体资源地址*/
    private String projectPath;
    /**项目名称*/
    private String projectName;
    /**页面路径*/
    private String path;

    /**新创建的项目*/
    private static final String PROJECT_PATH = "/project";

    /**json文件*/
    private static final String JSON = "json";
    /**var*/
    public static final String VAR = "var ";
    /**注释的开头*/
    public static final String ANNOTATION_BEGIN = "<!--";
    /**注释的结尾*/
    public static final String ANNOTATION_END = "-->";


    /**LINK的正则表达式*/
    private static final String LINK_REX = "<link[ \n]+[ /='\"\\w]*href[ \n]*=[ \n]*['\"]{1}[-_/.\\w]+['\"]{1}[ /\n]*>";
    /**LINK中的href部分*/
    private static final String LINK_HREF_REX = "href[ \n]*=[ \n]*['\"]{1}[/.\\w]+['\"]{1}";
    /**SCRIPT表达式*/
    private static final String SCRIPT_REX = "<script[ \n]+[ /='\"\\w]*src[ \n]*=[ \n]*['\"]{1}[-_/.\\w]+['\"]{1}[ /\n]*>[ \n]*</script>[ \n]*";
    /**SCRIPT中的SRC部分*/
    private static final String SCRIPT_SRC_REX = "src[ \n]*=[ \n]*['\"]{1}[-/.\\w]+['\"]{1}";
    /**session*/
    private static final String SESSION_REX = "[$]{1}.session.get\\(['\"]{1}[_a-zA-Z]+[_0-9a-zA-Z]*['\"]{1}\\)";

    /**
     * 构建函数
     * @param projectPath html项目的路径
     * @param projectName html项目的名称
     */
    public View(String projectPath, String projectName){
        this.projectPath = projectPath;
        this.projectName = projectName;
    }

    /**
     * 解析当前页面，从主页中获取引用的父级目录，并替换
     * @param htmlFile html文件
     * @param pageJsonArraysMap json列表，jsonPath-jsonName-JSONArray
     * @param jsonArraysOwnKeyMap json数组的注释，位于数组上面，说明哪些驼峰key是属于主体的，比如creatTime
     * @param isCover 是否覆盖，是则直接改写原来的内容，用于web场景；否则新建文件，用于插件场景
     * @param srcDirsSet 引用的父级目录
     */
    public void htmlAnalysis(File htmlFile
            , Map<String, Map<String, JSONArray>> pageJsonArraysMap
            , Map<String, List<String>> jsonArraysOwnKeyMap
            , boolean isCover
            , Set<String> srcDirsSet) throws Exception {
        String filePath = htmlFile.getAbsolutePath();
        //1、获取html文件的内容
        String content = FileUtil.getFileString(filePath, true).trim();
        if (content.length() == 0) {
            throw new AnalyseException(filePath + "没有内容。");
        }
        //2、找出引用资源目录，进而找到引用的json
        Map<String, String> srcDirReplaceMap = new HashMap<>();
        //2.1、找出Link
        Set<String> linkStrings = StringUtil.getRexs(content, LINK_REX);
        for (String linString : linkStrings) {
            //拿到link地址
            Set<String> hrefStrings = StringUtil.getRexs(linString, LINK_HREF_REX);
            for (String hrefString : hrefStrings) {
                //去掉引号，然后从等号开始获取
                String srcDir = hrefString.replaceAll("['\"]", Constant.EMPTY);
                String srcDirReplace = srcDirReplace(srcDir, srcDirsSet, hrefString);
                srcDirReplaceMap.put(srcDir, srcDirReplace);
            }
        }
        //2.2、找出src引用
        Set<String> scriptStrings = StringUtil.getRexs(content, SCRIPT_REX);
        try {
//            Map<String, String> repalceMap = new HashMap<>();
//            Map<String, String> jsonNameMap = new HashMap<>();
//            Set<String> clearSet = new HashSet<>();
            for (String scriptString : scriptStrings) {
                //拿到src
                Set<String> srcStrings = StringUtil.getRexs(scriptString, SCRIPT_SRC_REX);
                for (String srcString : srcStrings) {
                    String src = srcString.replaceAll("['\"]", Constant.EMPTY);
                    if (src.toLowerCase().endsWith(JSON)) {//json引用，读取json的内容
                        //1、处理json文件
                        srcJson(src, pageJsonArraysMap, jsonArraysOwnKeyMap);
                        //2、替换处理，srcString即引用行要被处理掉，只会有一个值
                        content = content.replace(scriptString, Constant.EMPTY);
                    }else {//js等其他引用，获取引用的根路径
                        //去掉引号，然后从等号开始获取
                        String srcJs = src.substring(src.indexOf(Constant.EQ) + 1, src.indexOf(Constant.RIGHT_FORWARD));
                        String srcJsReplace = srcDirReplace(srcJs, srcDirsSet, src);
                        srcDirReplaceMap.put(srcJs, srcJsReplace);
                    }
                }
            }
            //JQ实际用到了哪些字段，并不是都用上了
            //替换和写入
            boolean parallel = true;
//            content = ajaxStrRepalce(content, parallel, null, null);
            //清理内容
//            for (String clearString : clearSet) {
//                content = content.replace(clearString, Constant.EMPTY);
//            }
        } catch (Exception e) {
            throw new AnalyseException(e.getMessage() + "，文件路径：" + filePath);
        }
        //2.3、替换，加上项目目录
        for (String srcDir : srcDirsSet) {
            String srcDirReplace = srcDirReplaceMap.get(srcDir);
            char firstChar = srcDirReplace.charAt(0);
            char lastChar = srcDirReplace.charAt(srcDirReplace.length() - 1);
            content = content.replace(srcDirReplace, firstChar + projectName + Constant.RIGHT_FORWARD + srcDir + lastChar);
        }
        //3、vue解析和替换
        List<String> lines = FileUtil.readFileByLines(filePath);
        //得到数据请求信息
        List<GetDataAction> getDataActionList = Vue3Util.getJsonDataActionMap(lines);

        //4、替换session
        Set<String> sessionStrings = StringUtil.getRexs(content, SESSION_REX);
        for (String session : sessionStrings) {
            //TODO 替换成正确获取session的方式
            content = content.replace(session, Constant.NULL);
        }

        //有js/json引用就会有内容变化
        if (scriptStrings.size() > 0) {
            //非覆盖形式，需要新建文件
            if (!isCover){
                filePath = projectPath + PROJECT_PATH + filePath;
                //创建文件
                File f = new File(filePath);
                boolean b = f.createNewFile();
                if (b){
                    throw new AnalyseException("创建文件时发生异常：" + filePath);
                }
            }
            if(!FileUtil.writeTxtFile(content, filePath)) {
                throw new AnalyseException("写入文件时发生异常：" + filePath);
            }
        }
    }

    /**
     * 处理引用的Json文件
     * @param jsonPath 引用的Json路径
     * @param pageJsonArraysMap json列表，jsonPath-jsonName-JSONArray
     * @param jsonArraysOwnKeyMap json数组的注释，位于数组上面，说明哪些驼峰key是属于主体的，比如creatTime
     * @throws Exception 异常
     */
    private void srcJson(String jsonPath
            , Map<String, Map<String, JSONArray>> pageJsonArraysMap
            , Map<String, List<String>> jsonArraysOwnKeyMap) throws Exception {
        //同一个页面的两个json，有
        jsonPath = jsonPath.substring(jsonPath.indexOf(Constant.EQ)).trim();
        jsonPath = projectPath + File.separator + jsonPath;
        //1、拿到当前json文件中，名字与数据的映射
        List<String> jsonStrList = FileUtil.readFileByLines(jsonPath);
        Map<String, JSONArray> jsonArraysMap = JSONUtil.analysisJsonString(jsonStrList, jsonArraysOwnKeyMap);
        //2、有些json没有内容
        if (jsonArraysMap.size() > 0) {
            pageJsonArraysMap.put(jsonPath, jsonArraysMap);
        }
    }

    /**
     * 获取替换前后的路径
     * @param srcDir 资源目录
     * @param srcDirsSet 资源目录集合
     * @param path 引用的路径
     * @return String 替换后的路径
     */
    private String srcDirReplace(String srcDir, Set<String> srcDirsSet, String path){
        srcDir = srcDir.substring(srcDir.indexOf(Constant.EQ) + 1, srcDir.indexOf(Constant.RIGHT_FORWARD));
        srcDirsSet.add(srcDir);
        //把这个目录的前后加上，就是要替换的了
        return path.substring(path.indexOf(srcDir) - 1, path.indexOf(srcDir) + srcDir.length() + 1);
    }

    /**
     * 替换获取数据的动作
     * @param content 整个html的内容
     * @param getDataActionList 获取数据的动作
     */
    public String getDataActionReplace(String content, List<GetDataAction> getDataActionList){
        for (GetDataAction getDataAction : getDataActionList) {
            //vue变量替换，将对应的行替换成 vueDataName : null的格式
            String vueDataName = getDataAction.getVueDataName();
            String jsonName = getDataAction.getJsonName();
            Set<String> vueDataNameSet = StringUtil.getRexs(content, vueDataName + "[ ]*:[ ]*" + jsonName);
            for (String replaceCode : vueDataNameSet) {
                content = content.replace(replaceCode, vueDataName + Constant.COLON_EN + Constant.NULL);
            }
            //动作替换
            List<String> replaceCodeList = getDataAction.getReplaceCodeList();
            if (replaceCodeList.size() > 0){
                String replaceCode = replaceCodeList.get(0);
                //第一给替换内容往往是比较独特的赋值语句，即使有多个，挨个替换即可
                int index = content.indexOf(replaceCode);
                //组装替换语句
                String axiosStr = axiosRepalceStr(getDataAction);
                //用字符串替换的replace，而不是用正则表达式的replaceAll，否则经常因为括号等出错
                content = content.replace(replaceCode, axiosStr);
                //index所处的位置是replaceCode开头，所以replaceCode变了，加上新字符串的长度即可
                index = index + axiosStr.length();
                //而往后的有些是花括号之类的内容，需要取离最开始索引最近的，因为处理的时候是经过格式化的，去掉了索引，所以不能简单截取长度
                for (int i = 1; i < replaceCodeList.size(); i++){
                    replaceCode = replaceCodeList.get(i);
                    //获得所有索引
                    List<Integer> indexs = StringUtil.indexOfs(content, replaceCode);
                    //找到距离最近的那个
                    int min = Integer.MAX_VALUE;
                    int end = 0;
                    for (Integer j : indexs){
                        int temp = Math.abs(j - index);
                        if (temp < min){
                            min = temp;
                            end = j;
                        }else{
                            break;
                        }
                    }
                    StringBuilder builder = new StringBuilder(content);
                    builder.delete(index, end + replaceCode.length());
                    content = builder.toString();
                }
            }
        }
        return content;
    }

    /**
     * axios调用替换
     * @param getDataAction 获取数据的动作
     * @return String 用来替换的axios内容
     */
    public String axiosRepalceStr(GetDataAction getDataAction) throws AnalyseException{
        StringBuilder axios = new StringBuilder(getDataAction.getTheSetedPhrase()).append(" axios.get('");
        String jsonName = getDataAction.getJsonName();
        axios.append(Constant.RIGHT_FORWARD).append(jsonName).append(Constant.RIGHT_FORWARD).append("get");
        if (getDataAction.isSingle() && jsonName.endsWith("s")){
            jsonName = jsonName.substring(0, jsonName.length() - 2);
        }
        //首字母变大写，减去变成小写的差值
        char c = (char)(jsonName.charAt(0) - ('z'-'Z'));
        axios.append(c).append(jsonName.substring(1));
        return axios.toString();
//        return getDataAction.getTheSetedPhrase() + " axios\n" +
//                "      .get('https://www.runoob.com/try/ajax/json_demo.json')\n" +
//                "      .then(response => (this.info = response))\n" +
//                "      .catch(function (error) { // 请求失败处理\n" +
//                "        console.log(error);\n" +
//                "    });";
    }
}