package com.garlane.pancake.common.constant;

public class DataBaseConstant {
    public enum mysqlDataTypeEnum{TINYINT, SMALLINT, MEDIUMINT, INT, BIGINT, DECIMAL
        , DATE, TIME, DATETIME
        ,CHAR, VARCHAR, TEXT}
}
