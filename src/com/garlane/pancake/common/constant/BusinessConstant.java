/**
 * 
 */
package com.garlane.pancake.common.constant;
import java.math.BigDecimal;
import java.util.*;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.garlane.pancake.common.base.AnalyseException;

public class BusinessConstant {
	//常量，变量
	public enum valueTypeEnum {CONSTANT, VARIABLE}
	//大于，大于等于，等于，小于等于，小于，不等于
	public enum compereEnum {GT, GTEQ, EQ, LTEQ, LT, UNEQ}
	//相等，正向开头，正向结尾，正向包含， 反向开头，反向结尾，反向包含，不相等，没关系
	enum StringRelationEnum {EQ, STARTS_WITH_P, ENDS_WITH_P, CONTAINS_P, STARTS_WITH_F, ENDS_WITH_F, CONTAINS_F, UNEQ, NONE}
	//正向字符串长度等于整数，负向字符串长度等于整数，字符串包含整数
	enum StringBigdecimalRelationEnum {STRING_LENGTH_EQ_P, STRING_LENGTH_EQ_F, STRING_CONTAINS_INT, NONE}
	//用到的java类型
	public enum classTypeEnum {STRING, BIGDECIMAL, DATE, LINKEDHASHMAP, LIST, JSONOBJ, JSONARRAY}
	public static classTypeEnum getClassType(String classType){
		if ("java.lang.String".equals(classType)){
			return classTypeEnum.STRING;
		}else if ("java.math.BigDecimal".equals(classType)){
			return classTypeEnum.BIGDECIMAL;
		}if ("java.util.Date".equals(classType)){
			return classTypeEnum.DATE;
		}if ("java.util.LinkedHashMap".equals(classType)){
			return classTypeEnum.LINKEDHASHMAP;
		}if ("java.util.ArrayList".equals(classType)){
			return classTypeEnum.LIST;
		}if ("com.alibaba.fastjson.JSONObject".equals(classType)){
			return classTypeEnum.JSONOBJ;
		}if ("com.alibaba.fastjson.JSONArray".equals(classType)){
			return classTypeEnum.JSONARRAY;
		}
		return null;
	}

	//小于数组边界，数组开始，数组中间，数组的元素，数组结束，大于数组边界，在数组内部但不是数组元素
	public enum ObjJSONArrayRelationEnum {LT_BOUNDARY, LIST_BEGIN, LIST_MIDDLE, ELEMENT, LIST_END, GT_BOUNDARY, IN_BOUNDARY, NONE}
	public final static String COMMON_RESULT = "cr";//通常结果，即一般性结果,list和json比较临时存放用的
	/**当输入为数组时，输出的某些字段值为索引*/
	public final static String INDEX = "index";
	/**自然意义，如年龄18，不与特定的输入挂钩*/
//	public final static String NATURE = "nature";
	/**标志*/
	public static final String FLAG = "flag";
	
	//INNER类型
	/**关系类型vv\tv\vt*/
//	public final static String RELA_TYPE = "rt";
	/**关联关系*/
	public final static String RELAS = "rs";
	/**字段1*/
	public final static String KEY1 = "k1";
	/**字段2*/
	public final static String KEY2 = "k2";
	/**数据类型1*/
	public final static String DATA_TYPE1 = "t1";
	/**数据类型2*/
	public final static String DATA_TYPE2 = "t2";
	/**值,tv类型时需要*/
	public final static String VALUE1 = "v1";
	/**值,vt类型时需要*/
	public final static String VALUE2 = "v2";
	/**具体结果，一直只有一个值时会出现，比如1+1=2，只是参考，权重不大*/
	public final static String DETAIL_RESULT = "dr";
	/**当前情形一共有多少种结果*/
	public final static String RESULT_NUM = "rn";
	//each类型
	/**字段集1*/
	public final static String KEY_SET1 = "ks1";
	/**字段集2*/
	public final static String KEY_SET2 = "ks2";
	/**正向*/
	public final static String POSITIVE = "p";
	/**没有任何关系*/
	public final static String NONE = "none";

	/**值*/
	public final static String VALUE = "v";
	/**值的数据类型*/
	public final static String DATA_TYPE = "dt";
	/**键集合*/
	public final static String KEYS = "ks";
	
	/**
	 * 获取两个值之间的关系
	 * @param json1 用于拆解的json1
	 * @param json2 用于拆解的json2
	 * @param baseValObjs1 基本值对象
	 * @param baseValObjs2 基本值对象
	 * @param commonRelas 通常值列表
	 * @param detailRelas 具体值列表
	 * @param commonDetailValObjectMap 通常比值、具体值的映射，用于两个对象比较时还原具体值
	 * @param innerRulesMap 按keySet分好的构造规律（内部规律）
	 */
	public static void getTwoKeyValueRelas(JSONObject json1, JSONObject json2
			, List<JSONObject> baseValObjs1, List<JSONObject> baseValObjs2
			, List<JSONObject> commonRelas, List<JSONObject> detailRelas
			, Map<JSONObject, JSONObject> commonDetailValObjectMap
			, Map<Set<String>, JSONObject> innerRulesMap) {
		//这里需要处理在json和数组里的情况
		for (int i = 0; i < baseValObjs1.size(); i++) {
			JSONObject baseValObj = baseValObjs1.get(i);
			for (int j = 0; j < baseValObjs2.size(); j++) {
				getTwoKeyValueRelas(json1, json2, baseValObj, baseValObjs2.get(j), commonRelas, detailRelas, commonDetailValObjectMap, innerRulesMap);
			}
		}
	}
	
	/**
	 * 获取单个Json两个值之间的关系
	 * @param baseValObjs 基本值对象
	 * @param commonRelas 通常比较值
	 * @param detailRelas 具体比较值
	 * @param commonDetailValObjectMap 通常比值、具体值的映射，用于两个对象比较时还原具体值
	 */
	public static void getTwoKeyValueRelas(List<JSONObject> baseValObjs
			, List<JSONObject> commonRelas
			, List<JSONObject> detailRelas
			, Map<JSONObject, JSONObject> commonDetailValObjectMap) {
		for (int i = 0; i < baseValObjs.size(); i++) {
			JSONObject baseValObj = baseValObjs.get(i);
			for (int j = i + 1; j < baseValObjs.size(); j++) {
				getTwoKeyValueRelas(null, null, baseValObj, baseValObjs.get(j), commonRelas, detailRelas, commonDetailValObjectMap, null);
			}
		}
	}
	
	/**
	 * 获取两个值之间的关系
	 * @param json1 为null时表示是单个json求值
	 * @param json2 为null时表示是单个json求值
	 * @param baseValObj 基本值对象，节点和叶子都有，被拆得很细了
	 * @param baseValObj2 基本值对象，节点和叶子都有，被拆得很细了
	 * @param commonRelas 通常值列表
	 * @param detailRelas 具体值列表
	 * @param commonDetailValObjectMap 通常比值、具体值的映射，用于两个对象比较时还原具体值
	 * @param innerRulesMap 按keySet分好的构造规律，为null时则不使用
	 */
	public static void getTwoKeyValueRelas(JSONObject json1, JSONObject json2,
			JSONObject baseValObj, JSONObject baseValObj2
			, List<JSONObject> commonRelas
			, List<JSONObject> detailRelas
			, Map<JSONObject, JSONObject> commonDetailValObjectMap
			, Map<Set<String>, JSONObject> innerRulesMap) {		
		//获得使用的元素
		String flagKey = baseValObj.getString(KEYS);
		Object flagValue = baseValObj.get(VALUE);
		classTypeEnum flagValueDataType = getClassType(baseValObj.getString(DATA_TYPE));
		//获得使用的元素
		String key = baseValObj2.getString(KEYS);
		Object value = baseValObj2.get(VALUE);
		classTypeEnum valueDatatype = getClassType(baseValObj2.getString(DATA_TYPE));
		//获取值来源
		JSONObject valSource1 = null;
		JSONObject key1ValSource = null;
		Object keyType1 = VALUE;
		JSONObject valSource2 = null;
		JSONObject key2ValSource = null;
		Object keyType2 = VALUE;
		if (innerRulesMap != null) {
			JSONObject innerRule1 = innerRulesMap.get(json1.keySet());
			valSource1 = innerRule1.getJSONObject(RuleConstant.VALUE_SOURCE);
			if (!valSource1.containsKey(flagKey)) {//没有这个key的valSource
				if (classTypeEnum.LIST.equals(flagValueDataType) || classTypeEnum.JSONARRAY.equals(flagValueDataType)) {
					for (String k : valSource1.keySet()) {
						if (k.indexOf(flagKey + Constant.SHORT_BAR) == 0) {
							key1ValSource = valSource1.getJSONObject(k);							
						}
					}
					if (key1ValSource != null) {
						keyType1 = key1ValSource.getOrDefault(Constant.TYPE, VALUE);//默认是值类型						
					}else {
						throw new AnalyseException("找不到" + key + "的值来源。");
					}
				}
				//classTypeEnum.LINKEDHASHMAP会被拆解得更细，只有list需要判断位置，其他的情况就没有了，比如r、type这类
			}else {
				key1ValSource = valSource1.getJSONObject(flagKey);
				keyType1 = key1ValSource.getOrDefault(Constant.TYPE, VALUE);//默认是值类型				
			}
			JSONObject innerRule2 = innerRulesMap.get(json2.keySet());
			valSource2 = innerRule2.getJSONObject(RuleConstant.VALUE_SOURCE);
			if (!valSource2.containsKey(key)) {//没有这个key的valSource
				if (classTypeEnum.LIST.equals(valueDatatype) || classTypeEnum.JSONARRAY.equals(valueDatatype)) {
					for (String k : valSource2.keySet()) {
						if (k.indexOf(key + Constant.SHORT_BAR) == 0) {
							key2ValSource = valSource2.getJSONObject(k);							
						}
					}
					if (key2ValSource != null) {
						keyType2 = key2ValSource.getOrDefault(Constant.TYPE, VALUE);//默认是值类型						
					}else {
						throw new AnalyseException("找不到" + key + "的值来源。");
					}
				}
				//classTypeEnum.LINKEDHASHMAP会被拆解得更细，只有list需要判断位置，其他的情况就没有了，比如r、type这类
			}else {
				key2ValSource = valSource2.getJSONObject(key);
				keyType2 = key2ValSource.getOrDefault(Constant.TYPE, VALUE);//默认是值类型
			}
		}
		//不能擅自为业务做限制，找不出规律的，肯定会被过滤的
//		if (!keyType1.equals(keyType2)) {
//			return;
//		}
		//默认一个结果
		JSONObject rela = new JSONObject();
		rela.put(KEY1, flagKey);
		rela.put(KEY2, key);
//		rela.put(DATA_TYPE1, flagValueDataType);
//		rela.put(DATA_TYPE2, valueDatatype);
		rela.put(Constant.TYPE, VALUE);//类型，值比较
		rela.put(COMMON_RESULT, null);//通常结果
		//具体值记录
		JSONObject detailValObject = new JSONObject();
		detailValObject.put(DATA_TYPE1, flagValueDataType);
		detailValObject.put(DATA_TYPE2, valueDatatype);
		detailValObject.put(VALUE1, flagValue);
		detailValObject.put(VALUE2, value);
		if (flagValueDataType.equals(classTypeEnum.BIGDECIMAL)) {//整型或小数
			BigDecimal v1 = baseValObj.getBigDecimal(VALUE);
			//索引比完，值也可以比
			if (flagValueDataType.equals(valueDatatype)) {//都是整型或小数
				BigDecimal v2 = baseValObj2.getBigDecimal(VALUE);
				JSONObject detail = getBigDecimalResult(v1, v2, rela);
				detailRelas.add(detail);
				//都是整型时才可能是索引，而且约定第二个是环境
				if (baseValObj2.containsKey(INDEX)) {
					int index = baseValObj2.getIntValue(INDEX);
					//值比较补充索引
					rela.put(RuleConstant.INDEX_VALUE, new JSONArray());
					rela.getJSONArray(RuleConstant.INDEX_VALUE).add(index);
					//索引比较
					JSONObject indexRela = (JSONObject)rela.clone();
					indexRela.put(Constant.TYPE, INDEX);//索引类型
					indexRela.put(RuleConstant.INDEX_VALUE, new JSONArray());//索引对应的值
					indexRela.getJSONArray(RuleConstant.INDEX_VALUE).add(value);
					getBigDecimalResult(v1, v2, indexRela);
					commonRelas.add(indexRela);
				}
			}else if (valueDatatype.equals(classTypeEnum.STRING)) {//字符串
				String valueStr = String.valueOf(value);
				StringBigdecimalRelationEnum result = getBigDecimalStringResult(v1, valueStr);
				if (NONE.equals(result)) {
					return;
				}
				rela.put(COMMON_RESULT, result);
				rela.put(RESULT_NUM, 2);
				if (StringBigdecimalRelationEnum.STRING_CONTAINS_INT.equals(result)) {
					//正数在字符串中的位置，如果连第一个索引都不一样，那么所有的索引就更不一样了
					JSONObject detail = (JSONObject)rela.clone();
					detail.put(COMMON_RESULT, valueStr.indexOf(String.valueOf(value)));
					detailRelas.add(detail);
				}
			}else if(valueDatatype.equals(classTypeEnum.LINKEDHASHMAP) || valueDatatype.equals(classTypeEnum.JSONOBJ)){

			}else if(valueDatatype.equals(classTypeEnum.LIST) || classTypeEnum.JSONARRAY.equals(valueDatatype)){
				rela.put(COMMON_RESULT, getObjAndListResult(flagValueDataType, v1, baseValObj2.getJSONArray(VALUE)));
				rela.put(RESULT_NUM, 7);
			}else {
				throw new AnalyseException("出现新的类型，需要完善。");
			}
		}else if(flagValueDataType.equals(classTypeEnum.STRING)){//字符型
			String keyValue = String.valueOf(flagValue);
			if (flagValueDataType.equals(valueDatatype)) {//都是字符	
				String valueStr = String.valueOf(value);
				StringRelationEnum result = getStringResult(keyValue, valueStr);
				if (NONE.equals(result)) {
					return;
				}
				rela.put(COMMON_RESULT, result);
				rela.put(RESULT_NUM, 8);
			}else if (valueDatatype.equals(classTypeEnum.BIGDECIMAL)) {//数字
				StringBigdecimalRelationEnum result = getStringBigDecimalResult(keyValue, baseValObj2.getBigDecimal(VALUE));
				if (NONE.equals(result)) {
					return;
				}
				if (StringBigdecimalRelationEnum.STRING_CONTAINS_INT.equals(result)) {
					//正数在字符串中的位置，如果连第一个索引都不一样，那么所有的索引就更不一样了
					JSONObject detail = (JSONObject)rela.clone();								
					detail.put(COMMON_RESULT, keyValue.indexOf(String.valueOf(value)));
					detailRelas.add(detail);
				}
				rela.put(COMMON_RESULT, result);
				rela.put(RESULT_NUM, 1);
			}else if(valueDatatype.equals(classTypeEnum.LINKEDHASHMAP) || valueDatatype.equals(classTypeEnum.JSONOBJ)){
//					throw new AnalyseException("需完善String-linkedhashmap，虽然子项也会比较");
			}else if(valueDatatype.equals(classTypeEnum.LIST) || valueDatatype.equals(classTypeEnum.JSONARRAY)){
//					throw new AnalyseException("需完善String-list，虽然子项也会比较");
			}else {
				throw new AnalyseException("出现新的类型，需要完善。");
			}
		}else if (flagValueDataType.equals(classTypeEnum.DATE)) {//日期时间
			Date date = baseValObj.getDate(VALUE);
			if (valueDatatype.equals(classTypeEnum.DATE)) {//都是日期比较
				Date date2 = baseValObj2.getDate(VALUE);
				if (date.after(date2)) {
					rela.put(COMMON_RESULT, compereEnum.GT);
				}else if (date.before(date2)) {
					rela.put(COMMON_RESULT, compereEnum.LT);
				}else {
					rela.put(COMMON_RESULT, compereEnum.EQ);
				}
				rela.put(RESULT_NUM, 3);
				JSONObject detail = (JSONObject)rela.clone();
				detail.put(COMMON_RESULT, date.getTime() - date2.getTime());
				detailRelas.add(detail);
			}else if (valueDatatype.equals(classTypeEnum.LIST) || classTypeEnum.JSONARRAY.equals(valueDatatype)) {
				rela.put(COMMON_RESULT, getObjAndListResult(flagValueDataType, new BigDecimal(date.getTime()), baseValObj2.getJSONArray(VALUE)));
				rela.put(RESULT_NUM, 7);
			}
		}else if (flagValueDataType.equals(classTypeEnum.LINKEDHASHMAP) || flagValueDataType.equals(classTypeEnum.JSONOBJ)) {//JSONObject
//				throw new AnalyseException("需完善linkedhashmap-，虽然子项也会比较");
		}else if (flagValueDataType.equals(classTypeEnum.LIST) || flagValueDataType.equals(classTypeEnum.JSONARRAY)) {//列表，只处理大方向，细节不深入
			JSONArray array = baseValObj.getJSONArray(VALUE);
			if (valueDatatype.equals(classTypeEnum.BIGDECIMAL)) {
				rela.put(COMMON_RESULT, getObjAndListResult(valueDatatype, baseValObj2.getBigDecimal(VALUE), array));
				rela.put(RESULT_NUM, 7);
			}else if (valueDatatype.equals(classTypeEnum.DATE)) {
				rela.put(COMMON_RESULT, getObjAndListResult(valueDatatype, new BigDecimal(baseValObj2.getDate(VALUE).getTime()), array));
				rela.put(RESULT_NUM, 7);
			}
		}else {
			throw new AnalyseException("出现新的类型，需要完善。");
		}
		if (rela.get(COMMON_RESULT) != null) {
			commonRelas.add(rela);
			commonDetailValObjectMap.put(rela, detailValObject);//两个对象比较的时候不会有问题，多个的时候不适用
		}
	}
	
	/**
	 * 获取两个值之间的关系
	 * @param flagValue 第一个值
	 * @param value 第二个值
	 */
	public static String getTwoKeyValueRelas(Object flagValue, Object value) {
		JSONObject baseValObj = new JSONObject();		
		baseValObj.put(VALUE, flagValue);
		baseValObj.put(DATA_TYPE, flagValue.getClass().getTypeName());
		
		JSONObject baseValObj2 = new JSONObject();
		baseValObj2.put(VALUE, value);
		baseValObj2.put(DATA_TYPE, value.getClass().getTypeName());
		List<JSONObject> commonRelas = new ArrayList<JSONObject>();
		getTwoKeyValueRelas(null, null, baseValObj, baseValObj2, commonRelas, new ArrayList<>(), new HashMap<>(), null);
		if (commonRelas.size() > 0) {
			return commonRelas.get(0).getString(COMMON_RESULT);
		}else {
			return null;
		}
	}
	
	/**
	 * 可比较值与数组的关系
	 * @param objClassType obj对象类型
	 * @param obj 对象
	 * @param array JSONArray
	 * @return ObjJSONArrayRelationEnum
	 */
	private static ObjJSONArrayRelationEnum getObjAndListResult(classTypeEnum objClassType, Object obj
			, JSONArray array) {
		if(array.size() == 0){
			return ObjJSONArrayRelationEnum.NONE;
		}
		List<BigDecimal> v = new ArrayList<>();
		classTypeEnum itemDataType = getClassType(array.get(0).getClass().getTypeName());
		if (classTypeEnum.BIGDECIMAL.equals(itemDataType)) {//BIGDECIMAL
			for (int i = 0; i < array.size(); i++) {
				v.add(array.getBigDecimal(i));
			}
			v.sort(Comparator.naturalOrder());//升序
		}else if (classTypeEnum.DATE.equals(itemDataType) && itemDataType.equals(objClassType)) {//日期与日期相比
			for (int i = 0; i < array.size(); i++) {
				v.add(new BigDecimal(array.getDate(i).getTime()));
			}
			v.sort(Comparator.naturalOrder());//升序
		}
		BigDecimal boundaryMin = v.get(0);
		BigDecimal boundaryMax = v.get(v.size() - 1);
		BigDecimal bigDecimal = new BigDecimal(String.valueOf(obj));
		int flagMin = boundaryMin.compareTo(bigDecimal);
		int flagMax = boundaryMax.compareTo(bigDecimal);
		if (flagMin == 1) {//下边界大于该数
			return ObjJSONArrayRelationEnum.LT_BOUNDARY;
		}else if (flagMax == -1) {//上边界小于该数
			return ObjJSONArrayRelationEnum.GT_BOUNDARY;
		}else if(flagMin == 0){
			return ObjJSONArrayRelationEnum.LIST_BEGIN;
		}else if (flagMax == 0) {//等于上边界
			return ObjJSONArrayRelationEnum.LIST_END;
		}else if (bigDecimal.compareTo((boundaryMin.add(boundaryMax)).divide(new BigDecimal(2))) == 0) {
			return ObjJSONArrayRelationEnum.LIST_MIDDLE;
		}if (v.contains(bigDecimal)) {//属于数组的元素
			return ObjJSONArrayRelationEnum.ELEMENT;
		}else if(flagMin == 1 && flagMax == -1){
			return ObjJSONArrayRelationEnum.IN_BOUNDARY;
		}
		return ObjJSONArrayRelationEnum.NONE;
	}
	
	/**
	 * 获取两个数字的比较的结果
	 * @param v1 数字1
	 * @param v2 数字2
	 * @param rela 用于记录关系的JSON
	 * @return JSONObject 记录了差值的json
	 */
	public static JSONObject getBigDecimalResult(BigDecimal v1, BigDecimal v2, JSONObject rela) {
		JSONObject detail = (JSONObject)rela.clone();
		int flag = v1.compareTo(v2);
		if (flag == 0) {
			rela.put(COMMON_RESULT, compereEnum.EQ);
		}else if (flag == -1) {
			rela.put(COMMON_RESULT, compereEnum.LT);
		}else {
			rela.put(COMMON_RESULT, compereEnum.GT);
		}
		detail.put(COMMON_RESULT, v1.subtract(v2));//存储真实的差值
		return detail;

	}
	
	/**
	 * 获取两个String比较的结果
	 * @param keyValue 字符串1
	 * @param valueStr 字符串2
	 * @return StringRelationEnum
	 */
	public static StringRelationEnum getStringResult(String keyValue, String valueStr) {
		if (keyValue == null || valueStr == null) {
			return StringRelationEnum.NONE;
		}
		if (keyValue.equals(valueStr)) {
			return StringRelationEnum.EQ;
		}else if (keyValue.startsWith(valueStr)) {
			return StringRelationEnum.STARTS_WITH_P;
		}else if (keyValue.endsWith(valueStr)) {
			return StringRelationEnum.ENDS_WITH_P;
		}else if (keyValue.contains(valueStr)) {
			return StringRelationEnum.CONTAINS_P;
		}else if (valueStr.startsWith(keyValue)) {
			return StringRelationEnum.STARTS_WITH_F;
		}else if (valueStr.endsWith(keyValue)) {
			return StringRelationEnum.ENDS_WITH_F;
		}else if (valueStr.contains(keyValue)) {
			return StringRelationEnum.CONTAINS_F;
		}else {
			return StringRelationEnum.UNEQ;
		}
	}
	
	/**
	 * 获取bigDecimal与String比较的结果
	 * @param bigDecimal 数值
	 * @param str 字符串
	 * @return StringBigdecimalRelationEnum
	 */
	public static StringBigdecimalRelationEnum getBigDecimalStringResult(BigDecimal bigDecimal, String str) {		
		if (bigDecimal.equals(new BigDecimal(String.valueOf(str.length())))) {
			return StringBigdecimalRelationEnum.STRING_LENGTH_EQ_F;
		}else if (str.contains(bigDecimal.toString())) {
			return StringBigdecimalRelationEnum.STRING_CONTAINS_INT;
		}else {
			return StringBigdecimalRelationEnum.NONE;
		}
	}
	
	/**
	 * 获取String与integer比较的结果
	 * @param str 字符串
	 * @param bigDecimal 数值
	 * @return StringBigdecimalRelationEnum
	 */
	public static StringBigdecimalRelationEnum getStringBigDecimalResult(String str, BigDecimal bigDecimal) {
		if (bigDecimal.equals(new BigDecimal(String.valueOf(str.length())))) {
			return StringBigdecimalRelationEnum.STRING_LENGTH_EQ_P;
		}else if (str.contains(bigDecimal.toString())) {
			return StringBigdecimalRelationEnum.STRING_CONTAINS_INT;
		}else {
			return StringBigdecimalRelationEnum.NONE;
		}
	}
}
