package com.garlane.pancake.common.constant;
/**
 * 规则静态参数
 * @author lixingfa
 *
 */
public class RuleConstant {
	/**环境与结果，比如原来的字词搭配，分好词的句子是环境，对应的ijl就是几个*/
	public final static String ENV_AND_RESULT = "envAndResult";
	/**环境*/
	public final static String ENV = "env";
	/**结果*/
	public final static String RESULT = "result";
	
	
	/**内在规则，也称构成规律，分为json（自身）和jsons（组）构成规律*/
	public final static String INNER_RULE = "innerRule";
	/**内部规律分类*/
//	public final static String INNER_TYPE = "inType";
	/**json*/
	public final static String JSON = "json";
	/**jsons*/
	public final static String JSONS = "jsons";
	/**父一级别*/
	public final static String FATHER = "father";
	/**同值*/
	public final static String SAME_VAL = "sameVal";
	
	/**值来源*/
	public final static String VALUE_SOURCE = "valSource";
	/**结果与环境的关系*/
	public final static String RESULT_ENV_RELA = "resultEnvRela";
	/**结果添加到环境中*/
	public final static String RESULT_ADD_ENV = "envAndResult";
	/**值范围_上边界*/
	public final static String VAL_RANGE = "valRange";//
	/**值范围_上边界*/
	public final static String VAL_UP_RANGE = "valUpRange";//
	/**值范围_上边界允许跨越的次数*/
	public final static String VAL_UP_RANGE_NUM = "valUpRangeNum";//
	/**值范围_下边界*/
	public final static String VAL_DOWN_RANGE = "valDownRange";//
	/**值范围_下边界允许跨越的次数*/
	public final static String VAL_DOWN_RANGE_NUM = "valDownRangeNum";//
	
	/**索引与值，在总结json的值来源时会用到*/
	public final static String INDEX_VALUE = "indexValue";
	/**字段集，在某个集合里的规律*/
	public final static String KEY_SET = "ks";
	/**值与值，type、k1、k2、t1、t2、dr*/
	public final static String VALUE_VALUE = "vv";
	/**值与类型，type、k1、t1、t2、v*/
	public final static String TYPE_VALUE = "tv";
	/**数据类型*/
	public final static String DATA_TYPE = "dt";
	/**数据类型1*/
	public final static String DATA_TYPE1 = "t1";
	/**数据类型2*/
	public final static String DATA_TYPE2 = "t2";
	/**值,tv类型时需要*/
	public final static String VALUE1 = "v1";
	/**值,vt类型时需要*/
	public final static String VALUE2 = "v2";
	/**分为三种，常值、比值、比值具体值，每种又分为总是出现的、组合出现的（又分为）、拥有不同的结果*/
	public final static String RULES = "rules";
	//值规则类型
	/**总是出现的*/
	public final static String ALWAYS = "alw";
	/**相同规则，不同结果*///例如i < j 与 i > j 构成全部的ij关系
	public final static String DIFFERENT_RESULT = "diffR";
	/**不同结果的原因*/
	public final static String DIFFERENT_RESULT_REASONS = "diffRR";
	/**总是一起出现的*///例如符合配对
	public final static String COMPANY = "cp";
	
	/**原因*/
	public final static String REASON = "reason";
	
	/**相互规则，即与其他同类之间的关系规则*/
	public final static String EACH_RULE = "eachRule";//两个json相互
	
	
}
