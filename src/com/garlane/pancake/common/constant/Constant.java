package com.garlane.pancake.common.constant;

/**用到的常量
 * @author lixingfa
 * @date 2019年4月16日上午9:55:57
 * 
 */
public class Constant {
	public static final String TYPE = "type";
	/**null*/
	public final static String NULL = "null";
	/**空字符串*/
	public final static String EMPTY = "";
	/**换行*/
	public final static String LINE_FEED = "\n";
	/**英文逗号*/
	public final static String COMMA_EN = ",";
	/**中文逗号*/
	public final static String COMMA_CH = "，";
	/**短杠*/
	public final static String SHORT_BAR = "-";
	/**长杠*/
	public final static String LONG_BAR = "—";
	/**英文引号*/
	public final static String QUOT = "\"";
	/**英文单引号*/
	public final static String SINGLE_QUOT = "'";
	/**返点引号*/
	public final static String POINT_QUOT = "`";
	/**中文引号左“*/
	public final static String QUOT_CH_LEFT = "“";
	/**中文引号右”*/
	public final static String QUOT_CH_RIGHT = "”";
	/**顿号*/
	public final static String CAESURA = "、";
	/**书名号左《*/
	public final static String BOOK_LEFT = "《";
	/**书名号右》*/
	public final static String BOOK_RIGHT = "》";
	/**省略号*/
	public final static String ELLIPSIS = "……";
	/**空格*/
	public final static String SPACE = " ";
	/**加号*/
	public final static String PLUS = "+";
	public final static String LEFT_FORWARD = "\\";
	public final static String RIGHT_FORWARD = "/";
	/**小于*/
	public final static String LT = "<";
	/**大于*/
	public final static String GT = ">";
	/**等号*/
	public final static String EQ = "=";
	/**双等号*/
	public final static String DOUBLE_EQ = "==";
	/**＃号*/
	public final static String WELL = "#";
	/**小括号左*/
	public final static String PHESE_LEFT = "(";
	/**小括号右*/
	public final static String PHESE_RIGHT = ")";
	/**花括号左*/
	public final static String BRACE_LEFT = "{";
	/**花括号右*/
	public final static String BRACE_RIGHT = "}";
	/**空json对象*/
	public final static String EMPTY_JSON = "{}";
	/**[*/
	public final static String BRACKET_LEFT = "[";
	/**]*/
	public final static String BRACKET_RIGHT = "]";
	/**中文分号*/
	public final static String SEMICOLON_CH = "；";
	/**英文分号*/
	public final static String SEMICOLON_EN = ";";
	/**中文冒号*/
	public final static String COLON_CH = "：";
	/**英文冒号*/
	public final static String COLON_EN = ":";
	/**中文叹号*/
	public final static String EXCLAMATORY_MARK_CH = "！";
	/**英文叹号*/
	public final static String EXCLAMATORY_MARK_EN = "!";
	/**中文问号*/
	public final static String QUESTION_MARK_CH = "？";
	/**英文问号*/
	public final static String QUESTION_MARK_EN = "?";
	/**中文点号，在半空中那个*/
	public final static String POINT_CH = "·";
	/**英文点号*/
	public final static String POINT_EN = ".";
	/**中文句号*/
	public final static String PERIOD_CH = "。";
}
