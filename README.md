# pancake

#### 介绍
这是一个可以根据html+js框架做的页面直接生成JAVA后端+js框架前端的IDEA插件。
我们把html+js框架做的页面称为面子，这个插件称为里子。
面子目前支持vue3+jq+dubbo+mysql。

#### 软件架构
软件架构说明


#### 安装教程

1. 打开IDEA，File-New-Project from Version Control，将代码克隆到本地。
2. File-New-Project，选择IntelliJ Platform Plugin，新建IDEA插件，配置SDK等具体步骤请自行查阅资料。
3. 项目路径选择刚才克隆下来的pancake，就可以在原来的基础上创建插件项目，确保本地可以正常运行。
4. File-Project Structure-Libraries，将resources下的jar添加到项目路径。

#### 使用说明

1. 可以像启动SpringBoot项目那样启动插件，点击IDEA右上方Run和Debug那个下拉框，配置号信息，即可启动运行。
2. 如果编译时说加了@Data注解的实体类java:找不到符号，
3. 则File - Settings - Build - Compiler - Annotation Processors 中勾选Enable annotation processing 后确认即可。
4. 插件运行时，是打开一个新的IEDA，在顶部Help菜单后新增一个pancake的菜单。
5. 点击pancake，选择页面路径，会弹出一个小窗口。在小窗口里选择html项目所在的路径，例如dotors项目。
6. 然后点击执行，就可以在html项目根路径下得到依据该项目生成的项目源码。
###面子的编码规范
1.html项目的资源放在assets目录，自定义js函数文件放在js目录，这两个目录和以.开头的目录插件不会处理。
2.请使用Vue.createApp({ /* 选项 */ })的方式配置vue对象，pancake是用Vue.createApp(作为Vue开始标识的。
3.代码尽量严谨，例如不要把.filter( 写成 .filter ( ，左括号与filter之间多出空格或换行。
页面虽然不报错，但对解析插件来说增加很多工作量，也不是好的编码习惯。
解析插件会尽量兼容这些书写不严谨的情况，但也有兼顾不到的时候，会造成不符合预期的结果。
4.js虽然不强制以;结束一个赋值语句，但我们编码时最好用;明确表明该句子完结。
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
