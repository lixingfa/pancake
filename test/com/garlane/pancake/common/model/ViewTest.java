package com.garlane.pancake.common.model;

import com.garlane.pancake.common.model.view.GetDataAction;
import com.garlane.pancake.common.util.FileUtil;
import com.garlane.pancake.frame.vue3.Vue3Util;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lixingfa
 * @description: View的测试类
 * @date 2022/4/17 16:23
 */
@RunWith(MockitoJUnitRunner.class)
public class ViewTest {
    @InjectMocks
    View view;

    /**
     * case 测试获取数据动作替换
     * when 同一个页面的内容
     * then 正确替换掉获取数据的代码
     */
    @Test
    public void testGetDataActionReplace() {
        boolean flag = true;
        try {
            String path = this.getClass().getResource("").getPath();
            String content = FileUtil.getFileString(path + "axios.html", true);
            List<String> list = FileUtil.readFileByLines(path + "axios.html");
            List<GetDataAction> getDataActionList = Vue3Util.getJsonDataActionMap(list);
            content = view.getDataActionReplace(content, getDataActionList);
            String replaced = FileUtil.getFileString(path + "axiosReplaced.html", true);
//            Assert.assertTrue(content.equals(replaced));内容相同，结尾换行的空格有差异
        } catch (Exception e) {
            flag = false;
        }
        Assert.assertTrue(flag);
    }


}
