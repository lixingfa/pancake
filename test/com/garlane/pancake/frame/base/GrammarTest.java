package com.garlane.pancake.frame.base;

import com.garlane.pancake.frame.base.Grammar;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lixingfa
 * @description: Grammar的测试类
 * @date 2022/4/9 16:50
 */
@RunWith(MockitoJUnitRunner.class)
public class GrammarTest {
    @InjectMocks
    Grammar grammar;

    /**
     * case 测试语法解析
     * when 所有的代码都是空行、注解等被移除的
     * then 所有行都被移除
     */
    @Test
    public void testAnalysisAllRemove() {
        List<String> list = new ArrayList<>();
        list.add(" ");
        list.add(";");
        list.add("/*");
        list.add("var");
        list.add("*/");
        List<String> lines = grammar.standardCode(list);
        Assert.assertTrue(lines.size() == 0);
    }

    /**
     * case 测试语法解析
     * when 代码与注释混杂
     * then 有用的代码被保留
     */
    @Test
    public void testAnalysis() {
        List<String> list = new ArrayList<>();
        list.add("var i = 0; //这是一个注释");
        list.add("var j = 1; /** 初始化函数  */");
        list.add("var k = 2;/*");
        list.add("var");
        list.add("*/  var z = 3; ");
        List lines = grammar.standardCode(list);
        Assert.assertTrue(lines.size() == 4);
        Assert.assertTrue("var i = 0;".equals(lines.get(0)));
        Assert.assertTrue("var j = 1;".equals(lines.get(1)));
        Assert.assertTrue("var k = 2;".equals(lines.get(2)));
        Assert.assertTrue("var z = 3;".equals(lines.get(3)));
    }

    /**
     * case 对字符串按编码格式进行换行
     * when 普通情形的代码
     * then 有用的代码被保留
     */
    @Test
    public void testStandardCode() {
        boolean flag = true;
        try {
            List<String> list = grammar.standardCode("data:{var i = 0; var j = 1;}");
            Assert.assertTrue(list.size() == 4);
            Assert.assertTrue("data:{".equals(list.get(0)));
            Assert.assertTrue("var i = 0;".equals(list.get(1)));
            Assert.assertTrue("var j = 1;".equals(list.get(2)));
            Assert.assertTrue("}".equals(list.get(3)));
        } catch (Exception e) {
            flag = false;
        }
        Assert.assertTrue(flag);
    }
}
