package com.garlane.pancake.frame.vue3;

import com.garlane.pancake.common.model.view.GetDataAction;
import com.garlane.pancake.common.util.FileUtil;
import com.garlane.pancake.frame.vue3.Vue3Util;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class Vue3UtilTest {
    @InjectMocks
    private Vue3Util vue3Util;

    /**
     * case 测试语法解析
     * when 代码与注释混杂
     * then 有用的代码被保留
     */
    @Test
    public void testGetVues() {
        boolean flag = true;
        try {
            String path = this.getClass().getResource("").getPath();
            List<String> lines = FileUtil.readFileByLines(path + "1.html");
            List<GetDataAction> getJsonDataActionList = vue3Util.getJsonDataActionMap(lines);
            //能找到5个数据请求动作
            Assert.assertTrue(getJsonDataActionList.size() == 5);
            //notifications 既有排序又有筛选
            GetDataAction notifications = getJsonDataActionList.get(0);
            Assert.assertTrue("n.mobile == $.session.get('mobile')".equals(notifications.getCondition()));
            Assert.assertTrue("time:asc".equals(notifications.getOrder()));
            Assert.assertTrue("notifications".equals(notifications.getJsonName()));
            List<String> replaceCodeList = notifications.getReplaceCodeList();
            Assert.assertTrue("this.notifications = notifications.filter((n)=>{".equals(replaceCodeList.get(0)));
            Assert.assertTrue("return n.mobile == $.session.get('mobile')".equals(replaceCodeList.get(1)));
            Assert.assertTrue("}".equals(replaceCodeList.get(2)));
            Assert.assertTrue(")".equals(replaceCodeList.get(3)));
            Assert.assertTrue(".sort(sortByKeys(\"time:asc\"));".equals(replaceCodeList.get(4)));
            //patients 只有排序
            GetDataAction patients = getJsonDataActionList.get(2);
            Assert.assertTrue(patients.getCondition() == null);
            Assert.assertTrue("createTime:desc".equals(patients.getOrder()));
            Assert.assertTrue("patients".equals(patients.getVueDataName()));
            Assert.assertTrue("this.patients = patients.sort(sortByKeys(\"createTime:desc\"));"
                    .equals(patients.getReplaceCodeList().get(0)));
            //modules 只有筛选
            GetDataAction modules = getJsonDataActionList.get(3);
            Assert.assertTrue("m.role == $.session.get('role')".equals(modules.getCondition()));
            Assert.assertTrue(modules.getOrder() == null);
            Assert.assertTrue("let myModules = modules.filter((m)=>{".equals(modules.getReplaceCodeList().get(0)));
            Assert.assertTrue("return m.role == $.session.get('role')".equals(modules.getReplaceCodeList().get(1)));
            Assert.assertTrue("}".equals(modules.getReplaceCodeList().get(2)));
            Assert.assertTrue(");".equals(modules.getReplaceCodeList().get(3)));
            //companySettings 都没有
            GetDataAction companySettings = getJsonDataActionList.get(4);
            Assert.assertTrue(companySettings.getCondition() == null);
            Assert.assertTrue(companySettings.getOrder() == null);
            Assert.assertTrue(companySettings.getReplaceCodeList().size() == 0);
        } catch (Exception e) {
            flag = false;
        }
        Assert.assertTrue(flag);

    }

}
